<?php
/**
 *Methods for FYS Semesters Tool
 *
 *Contains all methods used by FYS Semesters tool to execute DB queries
 *
 */
 
/**
 *Methods for FYS Semesters Tool
 *
 *Contains all methods used by FYS Semesters Tool to execute DB queries
 *
 *@package FYS
 */
Class SemesterMethods {
    
    /**
     *__construct Constructor for SemesterMethods
     *
     *@param AdminMethods $adm Class object of AdminMethods
     *@return void
     */
    public function __construct($adm) {
        $this->adm = $adm;
    }

    /**
     *getCurrentSemester Retrieves current semester based on current datetime
     *
     *@return int ID of current semester
     */
    public function getCurrentSemester() {
        $query = 'SELECT semesterid
                    FROM FYS_Semester
                    WHERE now() BETWEEN startDateTime And endDateTime';
                    
        $semester = $this->adm->core->executeSQL($query);
        return (count($semester) > 0) ? $semester[0]->semesterid : 0;
    }
    
    /**
     *getSemesters Fetches all semester from table
     *
     *@return object Object of all semesters
     */
    public function getSemesters() {
        $query = 'SELECT * FROM FYS_Semester';
        return $this->adm->core->executeSQL($query);
    }
    
    /**
     *updateSemester Updates semester in DB
     *
     *@param int $semesterID ID of semester
     *@param string $startDate  Updated start date
     *@param string $endDate    Updated end date
     *@return object DB object of result
     */
    public function updateSemester($semesterID, $startDate, $endDate) {
        $i=0; $arr=null;
        $arr[$i]['parameter'] = ':semesterid';
        $arr[$i]['value'] = $semesterID;
        $arr[$i]['data_type'] = PDO::PARAM_INT;
        $i++;
        $arr[$i]['parameter'] = ':startdate';
        $arr[$i]['value'] = $startDate;
        $arr[$i]['data_type'] = PDO::PARAM_STR;
        $i++;
        $arr[$i]['parameter'] = ':enddate';
        $arr[$i]['value'] = $endDate;
        $arr[$i]['data_type'] = PDO::PARAM_STR;
        
        $query = 'UPDATE FYS_Semester SET startDateTime=:startdate, endDateTime=:enddate WHERE semesterid=:semesterid';
        return $this->adm->core->executeSQL($query, $arr);
    }
    
    /**
     *isValidDate Verify the date string
     *
     *@param string $date Date string to check
     *@return bool
     */
    public function isValidDate($date, $format = 'Y-m-d H:i:s') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}
?>