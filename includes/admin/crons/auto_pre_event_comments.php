<?php
function logToFile($message) {
    $file = 'auto_pre_event_comments.log';
    $txt .= date('y:m:d h:i:s', strtotime("now")) . ' | ' . $message;
    $myfile = file_put_contents($file, $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
}

function getEventInfo($core) {        
    $query = 'SELECT events.EventID, events.Name, events.Date, events.StartTime, events.EndTime, events.Location, events.Presenter, events.CoPres1, events.CoPres2, reg.PreEventComments
                FROM FYS_Registration_Table reg
                INNER JOIN FYS_Cocurricular_Events events
                    ON reg.EventID = events.EventID
                ORDER BY reg.EventID, reg.PreEventComments';
    
    $info = $core->executeSQL($query);
    return $info;
}

function separateResultsByID($data) {
    $currentID = "";
    $collection = [];
    $arr = [];
    
    foreach($data as $row) {
        if($row->EventID != $currentID) {
            if($currentID != "") {
                array_push($collection, $arr);
            }
            
            $currentID = $row->EventID;
            $arr = [];
        }
        
        array_push($arr, $row);
    }
    
    return $collection;
}

function sendPreEventComments($info, $tableData, $_settings) {
    $cleanDate = date('F d, Y', strtotime($info->Date));
    $message = '<h3>' . $info->Name . '</h3>
                <h4>' . $cleanDate . ', ' . $info->StartTime . ' - ' . $info->EndTime  . '</h4>
                <h4>' . $info->Location . '</h4>
                <ul>' . $tableData . '</ul>';
           
    if(empty($_GET['nomessage'])) {
        echo $message;
    }
    
    if(empty($_GET['html'])) {
        $subject = 'FYS Pre-Event Comments (' . $info->EventID . ')';
        $email = ($_settings['devMode']) ? $_settings['developerEmail'] : $info->Presenter . "@psu.edu";
        $headers = 'FROM: psh_fys_no_reply@psu.edu' . "\r\n" .
                   'Reply-To: psh_fys_no_reply@psu.edu' . "\r\n" .
                   'X-Mailer: PHP/' . phpversion() . "\r\n" .
                   'MIME-Version: 1.0' . "\r\n" . 
                   'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";

        if(!$_settings['devMode']) { $headers .= 'CC: ' . $info->CoPres1 . '@psu.edu,' . $info->CoPres2 . '@psu.edu' . "\r\n"; }
         
        mail($email, $subject, $message, $headers);
    }
}

// Check to see if event date is DaysAway day(s) away and if so, return true
function checkDaysAway($daysAway, $startDate) {
    $dateTime = strtotime($startDate);
    $nowTime = strtotime(date('Y-m-d 00:00:00', strtotime($daysAway . ' days')));
    return ($dateTime == $nowTime);
}

if(!empty($_GET['days'])) {
    $daysAway = $_GET['days'];
    echo "DAYS AWAY: " . $_GET['days'] . '<br>';
} else {
    $daysAway = 10;
}

include $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/settings.php';
include $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/core.php';

$obj = new Core($_settings);
$data = getEventInfo($obj);

$collection = separateResultsByID($data);

foreach($collection as $arr) {
    if(!checkDaysAway($daysAway, $arr[0]->Date)) {
        continue;
    }
    
    $tableData = "";
    foreach($arr as $row) {
        $preEventComments = ($row->PreEventComments == "") ? "The student did not leave a comment!" : $row->PreEventComments;
        $tableData .= '<li style="padding-bottom:5px;">' . $preEventComments . '</li>';
    }
    
    if(empty($_GET['html'])) {
        $logMsg = '[' . $arr[0]->EventID . ']' . ' sent email to presenter (' . $arr[0]->Presenter . ') with ' . count($arr) . ' pre-event comments';
        logToFile($logMsg);
    }
    
    sendPreEventComments($arr[0], $tableData, $_settings);
}
?>