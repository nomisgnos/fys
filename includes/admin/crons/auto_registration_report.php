<?php
function logToFile($message) {
    $file = 'auto_registration_report.log';
    $txt = '';
    $txt .= date('y:m:d h:i:s', strtotime("now")) . ' | ' . $message;
    $myfile = file_put_contents($file, $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
}

function getEventsStudents($core) {
    $query = 'SELECT events.EventID, events.Name, events.Type, events.Date, events.StartTime, events.EndTime, events.Location, events.Seats, events.Registered, events.Presenter, events.CoPres1, events.CoPres2,
                usr.FName, usr.LName, usr.AccessID, usr.Email
              FROM FYS_Registration_Table reg 
              INNER JOIN FYS_Cocurricular_Events events
                ON reg.EventID = events.EventID
              INNER JOIN FYS_User_Table usr
                ON reg.UserID = usr.UserID
              ORDER BY reg.EventID, usr.LName, usr.FName';
    
    $info = $core->executeSQL($query);
    return $info;
}

function separateResultsByID($data) {
    $currentID = "";
    $collection = [];
    $arr = [];
    
    foreach($data as $row) {
        if($row->EventID != $currentID) {
            if($currentID != "") {
                array_push($collection, $arr);
            }
            
            $currentID = $row->EventID;
            $arr = [];
        }
        
        array_push($arr, $row);
    }
    
    return $collection;
}
    
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

function sendRegReport($info, $tableData, $_settings) {
    $currDateTime = '';
    $logMsg = '';

    logToFile('sendRegReport function entered'); 

    $spreadsheetMessage = '<b>Registration Report</b><br /><br/>' . 
                $info->Name . ' (' . $info->EventID . ') . <br/>' .
                $info->Location . '<br/><br/>' . 
                '<table cellpadding="3" border="1">
                    <tr><td>Present</td>
                        <td>Access ID</td>
                        <td>Last Name</td>
                        <td>First Name</td>
                        <td>Email</td>
                    </tr>' . $tableData . '
                </table>';

    logToFile('About to create html file');
    // Create HTML file to be read by PhpSpreadsheet HTML Reader
    file_put_contents($_SERVER['DOCUMENT_ROOT'] .'/fys/includes/admin/reports/registration/excelEmailAttachment.html', $spreadsheetMessage);
    logToFile('Created html file');

    require $_SERVER['DOCUMENT_ROOT'] . '/fys/resources/phpspreadsheet/vendor/autoload.php';
    
    logToFile('Creating Spreadsheet, and reading file');
    $spreadsheet = new Spreadsheet();
    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
    $spreadsheet = $reader->load($_SERVER['DOCUMENT_ROOT'] .'/fys/includes/admin/reports/registration/excelEmailAttachment.html');

    logToFile('creating new XLsx');
    // Convert HTML file into Excel
    $writer = new Xlsx($spreadsheet);
    logToFile('Created Xlsx');

    // Get formatted date to use in excel file name
    $currDateTime .= date('_y-m-d_h-i-s', strtotime("now"));

    $excel_file_to_attach = $_SERVER['DOCUMENT_ROOT'] .'/fys/includes/admin/reports/registration/' . $info->EventID . $currDateTime . '.xlsx';

    logToFile('About to write excel file');
    // Save Excel file 
    $writer->save($excel_file_to_attach);
    logToFile('Wrote excel file');

    if(empty($_GET['nomessage'])) {
        echo '<br>' . $spreadsheetMessage;
    }

    // Php Mailer
    require_once $_SERVER['DOCUMENT_ROOT'] .'/fys/includes/phpmailer.php'; 

    if(empty($_GET['html'])) {
        logToFile('Entered if statement, if(empty($_GET[html]))');
       
        $destination_email = ($_settings['devMode']) ? $_settings['developerEmail'] : $info->Presenter . "@psu.edu";
        logToFile('Presenter\'s Email is ' . $destination_email);
        
        $mail = new PHPMailer;
        $mail->setFrom('psh_fys_no_reply@psu.edu');
        $mail->isHTML(true);
        if(!empty($destination_email)) {
            $mail->addAddress($destination_email);
        }

        /*if(!$_settings['devMode']) { 
            if(!empty($info->CoPres1)) {
                $mail->AddCC( $info->CoPres1 . '@psu.edu' ); 
                $logMsg .= 'CoPresenter1 Email is ' . $info->CoPres1 . '@psu.edu' . PHP_EOL;
            }
            if(!empty($info->CoPres2)) {
                $mail->AddCC( $info->CoPres2 . '@psu.edu' ); 
                $logMsg .= 'CoPresenter2 Email is ' . $info->CoPres2 . '@psu.edu' . PHP_EOL;
            }
        }*/

        $mail->Subject    = 'FYS Registration Report (' . $info->EventID . ')';
        $mail->Body       = "Dear Presenter,<br />
                             Please complete the attached attendance list for your event at the end of your presentation and then email to Daeon Blessing at dxb625@psu.edu
                             <br /><br/>
                             Please email this Excel sheet at your earliest convenience, but NO LATER than the Friday of the week of your presentation.<br /><br/>";

        $mail->AddAttachment( $excel_file_to_attach , 'registration_output.xlsx' );

        logToFile('Email is ready to be sent.');

        $mail->Send();

        logToFile('Email Sent');
                    
    }

}

// Check to see if event date is DaysAway day(s) away and if so, return true
function checkDaysAway($daysAway, $startDate) {
    $dateTime = strtotime($startDate);
    $nowTime = strtotime(date('Y-m-d 00:00:00', strtotime($daysAway . ' days')));
    return ($dateTime == $nowTime);
}

if(!empty($_GET['days'])) {
    if($_GET['days'] == 'now') {
        $daysAway = 0;
    } else {
        $daysAway = $_GET['days'];
    }
    
    echo "DAYS AWAY: " . $_GET['days'] . '<br>';
} else {
    $daysAway = 1;
}


include $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/settings.php';
include $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/core.php';

$obj = new Core($_settings);
$data = getEventsStudents($obj);

$collection = separateResultsByID($data);

foreach($collection as $arr) {
    if(!checkDaysAway($daysAway, $arr[0]->Date)) {
        continue;
    }
    $tableData = "";
    foreach($arr as $row) {
        $tableData .= '<tr><td>&nbsp;</td>
                        <td>' . $row->AccessID . '</td>
                        <td>' . $row->LName . '</td>
                        <td>' . $row->FName . '</td>
                        <td>' . $row->Email . '</td>
                       </tr>';
    }
    
    if(empty($_GET['html'])) {
        $logMsg = '[' . $arr[0]->EventID . ']' . ' sent email to presenter (' . $arr[0]->Presenter . ') with registration report of ' . count($arr) . ' students.';
        logToFile($logMsg);
    }

    logToFile('sendRegReport starting'); 
    
    sendRegReport($arr[0], $tableData, $_settings);   

    logToFile('sendRegReport done'); 
}
?>