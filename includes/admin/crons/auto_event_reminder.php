<?php
function logToFile($message) {
    $file = 'auto_event_reminder.log';
    $txt .= date('y:m:d h:i:s', strtotime("now")) . ' | ' . $message;
    $myfile = file_put_contents($file, $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
}

function getStudentsRegInfo($core){         
    $query = 'SELECT reg.EventID, reg.PreEventComments, reg.UserID, events.Date, events.Name, events.StartTime, events.EndTime, events.Location, usr.FName, usr.LName, usr.Email
                FROM FYS_Registration_Table reg
                INNER JOIN FYS_Cocurricular_Events events ON reg.EventID = events.EventID
                INNER JOIN FYS_User_Table usr ON reg.UserID = usr.UserID';
    $info = $core->executeSQL($query);
    
    return $info;
}

function sendEventReminder($info, $_settings) {
    $cleanDate = date('F d, Y', strtotime($info->Date));
    $message = 'Hi, ' . $info->FName . ' ' . $info->LName . "<br /><br />\r\n\r\n" .
               '<p>You have registered for the First-Year Seminar Cocurricular, <a href="https://harrisburg2.vmhost.psu.edu/fys/register#' . $info->EventID . '">' . $info->Name . '</a> on ' . $cleanDate . ' at ' . $info->StartTime . ' - ' . $info->EndTime . ' in ' . $info->Location . '.' . "<br /><br />\r\n\r\n" .
               'If you are unable to attend, please delete your reservation at least one business day before the event at  <a href="https://harrisburg2.vmhost.psu.edu/fys/">https://harrisburg2.vmhost.psu.edu/fys</a>  </p>' . "\r\n" .
               '<p>Contact the administrator at FYScocurriculars@psu.edu if you have questions. Do not reply to this email.</p>';

    if(empty($_GET['nomessage'])) {
        echo $message;
    }
    
    if(empty($_GET['html'])) {
        $subject = 'FYS - Event Reminder' . $info->EventID;
        $email = ($_settings['devMode']) ? $_settings['developerMail'] : $info->Email;
        
        $headers = 'FROM: psh_fys_no_reply@psu.edu' . "\r\n" .
                   'Reply-To: psh_fys_no_reply@psu.edu' . "\r\n" .
                   'X-Mailer: PHP/' . phpversion() . "\r\n" .
                   'MIME-Version: 1.0' . "\r\n" . 
                   'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
               
        mail($email, $subject, $message, $headers);
    }
}

// Check to see if event date is DaysAway day(s) away and if so, return true
function checkDaysAway($daysAway, $startDate) {
    $dateTime = strtotime($startDate);
    $nowTime = strtotime(date('Y-m-d 00:00:00', strtotime($daysAway . ' days')));
    return ($dateTime == $nowTime);
}

if(!empty($_GET['days'])) {
    $daysAway = $_GET['days'];
    echo "DAYS AWAY: " . $_GET['days'] . '<br>';
} else {
    $daysAway = 3;
}

include $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/settings.php';
include $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/core.php';

$obj = new Core($_settings);
$regInfo = getStudentsRegInfo($obj);

foreach($regInfo as $info) {
    // Reminder is sent only if event date is x $daysAway
    if(checkDaysAway($daysAway, $info->Date)) {
        if(empty($_GET['html'])) {
            $logMsg = '[' . $info->EventID . ']' . ' sent event reminder email to student (' . $info->Email . ')';
            logToFile($logMsg);
        }
        
        sendEventReminder($info, $_settings);
    }
}
?>
