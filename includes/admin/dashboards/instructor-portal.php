<?php
    $courses = $adm->getMyCourses(trim(strtolower($accessID)));
?>
<style>
.tooltip {
    font-size:12px;
}
table tr th {
    padding:0!important;
}
</style>

<h2>Instructor Dashboard</h2>

<div class="row">
    <div class="col-md-9 col-sm-12">
        <h3>My Courses</h3>
        <table class="table table-hover">
            <thead><tr><th></th><th></th><th></th><th></th></tr></thead>
            <tbody>
            <?php
            foreach($courses as $course) {
            ?>
                <tr><td><label data-toggle="tooltip" title="Course Number" class="label label-primary"><?=$course->ScheduleNumber?></label></td><td><a href="<?= $_settings['current_URL_path'];?>/admin/courses/search/<?=$course->ScheduleNumber?>"><?=$course->Name?> <?=$course->Number?>: Section <?=$course->Section?></a></td><td style="vertical-align:middle;"><span class="label label-default" data-toggle="tooltip" data-html="true" title="<?=$course->Instructor?><br><?=$course->InstructorID?>"><?=$course->Instructor?></span></td><td><a class="btn btn-default btn-xs" href="<?= $_settings['current_URL_path'];?>/admin/courses/<?=$course->CourseID?>"><span class="fa fa-user-o fa-sm"></span> Students</a></td></tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<script>
$('[data-toggle="tooltip"]').tooltip(); 
</script>