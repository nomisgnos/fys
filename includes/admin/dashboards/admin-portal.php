<?php
    $events = $adm->getUpcomingEvents();
    $eventStats = $adm->getEventBreakdown();
    $eventRegStats = $adm->getEventRegistrationStats();
    
    $totalEvents = $eventStats[0]->numEvents;
    $totalReg = $eventRegStats[0]->registrations;

    function dateDiff($dateStart) {
        $now = new DateTime("midnight");
        $dateTimeStart = new DateTime($dateStart);
        $interval = date_diff($now, $dateTimeStart);
        $days = $interval->format('%a');
        echo ($days == "0") ? "today" : ($days == "1") ? "tomorrow" : "in " . $days . " days";
    }
?>

<style>
.tooltip {
    font-size:12px;
}
table tr th {
    padding:0!important;
}
.quick-action {
    margin-bottom:10px;
}
.dataTables_paginate, .dataTables_length, #dataEvents {
    display:none;
}
.align-middle {
    vertical-align:middle;
}
.lead-md {
    font-size:17px;margin-bottom:5px;
}
.lead-sm {
    font-size:15px;margin-bottom:5px;
}
</style>
    
<h2 class="lead">Admin Dashboard</h2>
    <div class="row">
        <div class="col-md-10 col-sm-12">
            <h3>Upcoming Events</h3>
            <hr class="style1 no-margin">
            <table id="dataEvents" class="table table-hover">
                <thead><tr><th></th><th></th><th></th><th></th></tr></thead>
                <tbody id="rowAppend">
                <?php
                foreach($events as $event) {
                ?>
                    <tr>
                        <td class="align-middle">
                            <label data-toggle="tooltip" title="Event ID" class="label label-default label-md"><?=$event->EventID?></label>  
                            <a href="<?= $_settings['current_URL_path'];?>/admin/events/<?=$event->EventID?>"><?=$event->Name?></a>
                        </td>
                        <td class="align-middle"><span class="label label-success label-md" data-toggle="tooltip" title="Registered / Seats"><?=$event->Registered?> / <?=$event->Seats?></span></td>
                        <td class="align-middle">
                            <span class="label label-primary label-md" data-toggle="tooltip" data-html="true" title="<?=$event->Date?><br><?=$event->StartTime?>-<?=$event->EndTime?>"><?=dateDiff($event->Date)?></span>
                        </td>
                        <td>
                            <a class="btn btn-default" href="<?= $_settings['current_URL_path'];?>/admin/events/view-attendance-report?EventID=<?=$event->EventID?>">
                                <span class="fa fa-user-o fa-sm"></span> Registered
                            </a>
                        </td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
            <button style="width:100%;" class="btn btn-default" id="moreEntries">Show More</button>
        </div>

        <div class="col-md-2 col-sm-12">
            <h3>Quick Actions</h3>
            <hr style="margin:0 0 10px 0;" class="style1">
            <div class="col-md-12 col-sm-3 col-xs-6 quick-action"><a class="btn btn-default" href="<?= $_settings['current_URL_path'];?>/admin/calendar"><span class="fa fa-calendar"></span> Calendar View</a></div>
            <div class="col-md-12 col-sm-3 col-xs-6 quick-action"><a class="btn btn-default" href="<?= $_settings['current_URL_path'];?>/admin/import/timeline"><span class="fa fa-history"></span> Import Timeline</a></div>
            <div class="col-md-12 col-sm-3 col-xs-6 quick-action"><a class="btn btn-default" href="<?= $_settings['current_URL_path'];?>/admin/semester"><span class="fa fa-clock-o"></span> Semesters</a></div>
            <div class="col-md-12 col-sm-3 col-xs-6 quick-action"><a class="btn btn-default" href="<?= $_settings['current_URL_path'];?>/admin/purge"><span class="fa fa-trash-o"></span> Purge Data</a></div>
            <form method="post">
                <div class="col-md-12 col-sm-3 col-xs-6 quick-action"><button class="btn btn-default" type="submit" name="BackupDB"><span class="fa fa-database"></span> Backup DB</button></div>
            </form>
            <div class="col-md-12 col-sm-12 col-xs-12 quick-action"><hr class="style1 no-margin"></div>
            <div class="col-md-12 col-sm-3 col-xs-6 quick-action"><a class="btn btn-success" href="<?= $_settings['current_URL_path'];?>/admin/events/add">New Event</a></div>
            <div class="col-md-12 col-sm-3 col-xs-6 quick-action"><a class="btn btn-success" href="<?= $_settings['current_URL_path'];?>/admin/students/add">New Student</a></div>
            <div class="col-md-12 col-sm-3 col-xs-6 quick-action"><a class="btn btn-success" href="<?= $_settings['current_URL_path'];?>/admin/users/add">New User</a></div>
            <div class="col-md-12 col-sm-3 col-xs-6 quick-action"><a class="btn btn-success" href="<?= $_settings['current_URL_path'];?>/admin/courses/add">New Course</a></div>
            <div class="col-md-12 col-sm-12 col-xs-12 quick-action"><hr class="style1 no-margin"></div>
            <div class="col-md-12 col-sm-3 col-xs-6 quick-action"><a class="btn btn-primary" href="<?= $_settings['current_URL_path'];?>/admin/reports/email-all">Email Students</a></div>
            <div class="col-md-12 col-sm-3 col-xs-6 quick-action"><a class="btn btn-primary" href="<?= $_settings['current_URL_path'];?>/admin/reports/email-presenters">Email Presenters</a></div>
            
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col col-md-6">
            <!--EVENT TYPE BREAKDOWN -->
                <h2>Event Type Breakdown<small data-toggle="tooltip" title="Total Number of Events" style="margin-top:.5em;" class="label label-default pull-right text-muted"><?=$totalEvents?></small></h2>
                <?php
                foreach($eventStats as $evntObj) {
                    if($evntObj->Type == "SUM") {
                        continue;
                    }
                    
                    $percentage = round($evntObj->numEvents/$totalEvents*100);
                ?>
                    <div class="lead lead-md"><?=$evntObj->Type?><span class="lead-sm pull-right"><strong><?=$evntObj->numEvents?></strong></span></div>
                    <div class="progress">
                        <div data-toggle="tooltip" title="<?=$evntObj->numEvents?> events" class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="<?=$evntObj->numEvents?>" aria-valuemin="0" aria-valuemax="<?=$totalEvents?>" style="width:<?=$percentage?>%">
                        <span><?=$percentage?>%</span>
                        </div>
                    </div>
                <?php
                }
                ?>
                
            <!--MISC STATS -->
                <?php
                $totalAttendance = 0;
                $totalMissed = 0;
                $totalHold = 0;
                
                foreach($eventRegStats as $regStat) {
                    if($regStat->Type == "SUM") {
                        continue;
                    }
                    
                    $totalAttendance += $regStat->sumAttended;
                    $totalMissed += $regStat->sumMissed;
                    $totalHold += $regStat->sumHold;
                }
                ?>
                
                <h2>Misc. Stats</h2>
                <div style="margin-bottom:10px;" class="lead lead-md" data-toggle="tooltip" data-html="true" data-placement="top" title="<strong><?=$totalAttendance?></strong> registrations attended. <strong><?=$totalMissed?></strong> missed.">
                    Attendance Rate: <span class="pull-right"><strong><?= !empty($totalReg) ? round($totalAttendance/($totalReg-$totalHold)*100) : 0 ?>%</strong></span></div>
                <div class="lead lead-md" data-toggle="tooltip" data-html="true" data-placement="top" title="<strong><?=$totalHold?></strong> unrecorded event registrations">
                    Unrecorded Attendance: <span class="pull-right"><strong><?= !empty($totalReg) ? round($totalHold/$totalReg*100) : 0 ?>%</strong></span></div>
            </div>
            
            
            <div class="col col-md-6">
            <!--EVENT REGISTRATIONS -->
                <h2>Event Registrations <small data-toggle="tooltip" title="Total Number of Registrations" style="margin-top:.5em;" class="label label-default pull-right text-muted"><?=$totalReg?></small></h2>
                
                <?php
                foreach($eventRegStats as $regStat) {
                    if($regStat->Type == "SUM") {
                        continue;
                    }
                    
                    $percentage = round($regStat->registrations / $totalReg * 100);
                ?>
                    <div class="lead lead-md"><?=$regStat->Type?><span class="lead-sm pull-right"><strong><?=$regStat->registrations?></strong></span></div>
                    <div class="progress">
                        <div data-toggle="tooltip" title="<?=$regStat->registrations?> Event Registrations" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?=$regStat->registrations?>" aria-valuemin="0" aria-valuemax="<?=$totalReg?>" style="width:<?=$percentage?>%">
                            <span><?=$percentage?>%</span>
                        </div>
                    </div>
                <?php
                }
                ?>
            <!--ATTENDANCE -->
                <h2>Attendance</h2>
                <?php
                foreach($eventRegStats as $regStat) {
                    if($regStat->Type == "SUM") {
                        continue;
                    }
                    
                    $totalTypeReg = $regStat->registrations;
                    $percentAdded = round($regStat->sumAttended / $totalTypeReg * 100);
                    $percentMissed = round($regStat->sumMissed / $totalTypeReg * 100);
                    $percentHold = round($regStat->sumHold / $totalTypeReg * 100);
                    
                ?>
                    <div class="lead lead-md"><?=$regStat->Type?></div>
                    <div class="progress">
                        <div data-toggle="tooltip" title="<?=$regStat->sumAttended?> Students Attended" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?=$regStat->sumAttended?>" aria-valuemin="0" aria-valuemax="<?=$totalTypeReg?>" style="width:<?=$percentAdded?>%">
                            <span><?=$percentAdded?>%</span>
                        </div>
                        <div data-toggle="tooltip" title="<?=$regStat->sumMissed?> Students Missed" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?=$regStat->sumMissed?>" aria-valuemin="0" aria-valuemax="<?=$totalTypeReg?>" style="width:<?=$percentMissed?>%">
                            <span><?=$percentMissed?>%</span>
                        </div>
                        <div data-toggle="tooltip" title="<?=$regStat->sumHold?> Students Unrecorded" class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?=$regStats[0]->sumHold?>" aria-valuemin="0" aria-valuemax="<?=$eventInfo[0]->Registered?>" style="width: <?=$percentHold?>%;">
                            <span><?=$percentHold?>%</span>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
    
<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/dataTables.bootstrap.min.js"></script>

<script>
    /* Courses DataTable */
    var table = $('#dataEvents').DataTable({
        DisplayLength: 10,
        bDeferRender: true,
        orderClasses: false,
        responsive: true,
        autoWidth: true,
        paging:   true,
        ordering: false,
        info:     false,
        searching: false
    });
    
    $('#dataEvents').show();
    $('[data-toggle="tooltip"]').tooltip(); 
    
    var rows = table.rows().data();
    var pageIndex = 1;
    var pageLength = rows.length / 10 - 1;
    
    var fetchRows = function(start, end) {
        filteredRows = [];
        
        for(var i = start; i < end; i++) {
            if(i > rows.length) {
                break;
            }
            filteredRows.push(rows[i]);
        }
        
        return filteredRows;
    };
    
    $('#moreEntries').on('click', function() {
        if(pageIndex > pageLength) {
            return;
        }
        start = pageIndex * 10;
        end = start + 10;
        newRows = fetchRows(start,end);

        rowHTML = "<tr><td class='no-padding' colspan='3'><hr class='style1 no-margin'></td></tr>";
        for(var i = 0; i < newRows.length; i++) {
            rowHTML += '<tr><td>' +  newRows[i][0] + '</td><td>' + newRows[i][1] + '</td><td>' + newRows[i][2] + '</td><td>' + newRows[i][3] + '</td></tr>';
        }
        $('#rowAppend').append(rowHTML);
        pageIndex++;
        if(pageIndex > pageLength){
            $(this).attr("disabled", 0);
        }
    });
</script>