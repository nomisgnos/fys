<?php
    $events = $adm->getMyEvents(trim(strtolower($accessID)), $semesterID);
    
    function dateDiffSign($dateStart) {
        $now = new DateTime("now");
        $dateTimeStart = new DateTime($dateStart);
        
        $interval = date_diff($now, $dateTimeStart);
        
        if($interval->format('%R') == "+") {
            echo $interval->format('in %a days');
        } else {
            echo $interval->format('%a days ago');
        }
    }
?>
<style>
.tooltip {
    font-size:12px;
}
table tr th {
    padding:0!important;
}
</style>

<h2>Presenter Dashboard</h2>

<div class="row">
    <div class="col-md-9 col-sm-12">
        <h3>My Events</h3>
        <table class="table table-hover">
            <thead><tr><th></th><th></th><th></th></tr></thead>
            <tbody>
            <?php
            foreach($events as $event) {
            ?>
                <tr><td style="vertical-align:middle;"><label data-toggle="tooltip" title="Event ID" class="label label-default"><?=$event->EventID?></label>  <a href="<?= $_settings['current_URL_path'];?>/admin/events/<?=$event->EventID?>"><?=$event->Name?></a></td><td style="vertical-align:middle;"><span class="label label-primary" data-toggle="tooltip" data-html="true" title="<?=$event->Date?><br><?=$event->StartTime?>-<?=$event->EndTime?>"><?=dateDiffSign($event->Date)?></span></td><td><a class="btn btn-default" href="<?= $_settings['current_URL_path'];?>/admin/events/view-attendance-report?EventID=<?=$event->EventID?>"><span class="fa fa-user-o fa-sm"></span> Students</a></td></tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<script>
$('[data-toggle="tooltip"]').tooltip(); 
</script>