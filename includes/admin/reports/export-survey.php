<?php
try {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/settings.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/core.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/export.php';
    
    require_once $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/admin/reports/exports/class.writeexcel_biffwriter.inc.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/admin/reports/exports/functions.writeexcel_utility.inc.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/admin/reports/exports/class.writeexcel_format.inc.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/admin/reports/exports/class.writeexcel_formula.inc.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/admin/reports/exports/class.writeexcel_olewriter.inc.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/admin/reports/exports/class.writeexcel_workbook.inc.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/admin/reports/exports/class.writeexcel_worksheet.inc.php';

    $obj = new Core($_settings);
    
    if ($_SERVER['HTTP_HOST'] != '127.0.0.1' && $_SERVER['HTTP_HOST'] != 'localhost') {
        require_once $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/admin.php';
        
        $accessID = $obj->getAccessID();
        $userID = $obj->getUserID($accessID);
        $roleID = $obj->getRoleID($userID);
        
        if(!$obj->canUserAccess(array(1), $roleID)) {
            echo "403 Access Forbidden!";
            return;
        }
    }
    
    $export = new ExportMethods($obj);
    $data = $export->getSurveyResponses();
    
    // Header
    $keys = array("Event", "EventDate", "EventID", "Attendance", "Last Name", "First Name", "AccessID", "Course", "Number", "Section", "Pre-Event Comments", "Rating", "Paragraph");

    $fname = tempnam(getcwd() . "/tmp", "temp_fys_all_report.xls");
    $workbook = new writeexcel_workbook($fname);
    $worksheet = $workbook->addworksheet();
    
    $worksheet->freeze_panes(1, 0);
    
    // Wrap text and center format
    $wordwrapCenter = $workbook->addformat();
    $wordwrapCenter->set_text_wrap();
    $wordwrapCenter->set_align('center');
    
    // Only wrap text format
    $wordwrap = $workbook->addformat();
    $wordwrap->set_text_wrap();

    // Only center format
    $center = $workbook->addformat();
    $center->set_align('center');

    // Header format
    $header = $workbook->addformat();
    $header->set_color('white');
    $header->set_align('center');
    $header->set_align('vcenter');
    $header->set_pattern();
    $header->set_bold();
    $header->set_text_wrap();
    $header->set_fg_color('green');

    // Set column width and header height
    $worksheet->set_column('A:A', 32);
    $worksheet->set_column('B:B', 10);
    $worksheet->set_column('C:D', 6);
    $worksheet->set_column('E:H', 16);
    $worksheet->set_column('H:J', 8);
    $worksheet->set_column('K:K', 48);
    $worksheet->set_column('L:L', 5);
    $worksheet->set_column('M:M', 48);
    $worksheet->set_row(0, 30);

    // Write header in first row
    foreach($keys as $num => $key) {
        $worksheet->write(0, $num, $key, $header);
    }

    foreach($data as $num => $row) {
        // Skip first row
        if(!$num) continue;
        
        // Write to row
        $worksheet->write($num, 0, $row->EventName, $wordwrapCenter);
        $worksheet->write($num, 1, $row->EventDate, $center);
        $worksheet->write($num, 2, $row->EventID, $center);
        $attended = ($row->IsAttended == "") ? "R" : $row->IsAttended;
        $worksheet->write($num, 3, $attended, $center);
        
        $worksheet->write($num, 4, $row->LName, $center);
        $worksheet->write($num, 5, $row->FName, $center);
        $worksheet->write($num, 6, $row->AccessID, $center);
        
        $worksheet->write($num, 7, $row->Name, $center);
        $worksheet->write($num, 8, $row->Number, $center);
        $worksheet->write($num, 9, $row->Section, $center);
        
        $worksheet->write($num, 10, $row->PreEventComments, $wordwrap);
        $worksheet->write($num, 11, $row->SurveyRating, $center);
        $worksheet->write($num, 12, $row->SurveyParagraph, $wordwrap);
    }

    $workbook->close();

    $date = date('Y-m-d-Hi');
    header("Content-Type: application/x-msexcel; name=\"FYS_.xlsx\"");
    header("Content-Disposition: inline; filename=\"FYS_Event_Feedback_All_$date.xlsx\"");
    $fh=fopen($fname, "rb");
    fpassthru($fh);
    unlink($fname);

} catch (\Throwable $th) {
    var_dump('<pre>' . $th . '</pre>');
}
?>
