<?php
    // Permissions check
    if (!$adm->canUserAccess(array(1), $roleID)) {
        return;
    }
    $allPresenters = $adm->getPresenters(false);
?>

<div class="body-content" id="body-content-padding">
<legend>Send Email to All Presenters</legend>
    <form class="form-horizontal" id="PreEventEmail" action="" method="post">
        <ul class="element-list">
            <li>Subject:</br >
                <input class="form-control" type="text" name="Subject" size="50" maxlength="100" required /></li>
            <li>Message:</br >
                <textarea class="form-control" name="Message" rows="5" cols="52" maxlength="2000" required></textarea></li>
            <li>Reply-to email address: (Do not change this unless you know what you are doing) <br />
                <input class="form-control" type="text" name="Reply" size="50" value="psh_fys_no_reply@psu.edu" required/></li>
        </ul>
        <input type="hidden" name="Redirect" value="redirectEmailAllPresenters" />
        <input class="btn btn-success" type="submit" name="Submit" value="Send Email" />&nbsp;&nbsp;&nbsp;<input class="btn btn-primary" type="reset" name="Reset" value="Reset" /> 
        &nbsp;&nbsp;&nbsp;<input class="btn btn-danger" type="button" value="Cancel" onClick="history.go(-1);return true;" />
    </form>
    
    <p>Your email will be sent to the following students:</p>
    <ul>
    <?php
        foreach($allPresenters as $presenter) {
            echo "<li>" . $presenter->label;
        }
    ?>
    </ul>
</div>