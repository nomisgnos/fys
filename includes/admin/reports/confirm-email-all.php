<?php
if ($adm->canUserAccess(array(1), $roleID)) {
?>
                <h1>Confirmation: Send Email via Global Messaging</h1>
            </div>
        
            <div class="body-content" id="body-content-padding">

                <?php
                if (isset($_GET['results'])) {
                    switch ($_GET['results']) {
                        case 'success':
                            echo '<p>Pre-event email was successfully sent.</p>';
                            break;
                        case 'failure':
                            echo '<p>The pre-event mailer failed to send the email.</p>';
                            break;
                        case 'empty':
                            echo '<p>There are not recipients for this email.</p>';
                            break;
                        default:
                            echo '<p>An unknown error occurred while attempting to prepare the pre-event email. Please try again.</p>';
                            break;
                    }
                }
                
                echo '<p class="no-print"><a class="btn btn-info" href="' . $_settings['current_URL_path'] . '/admin/reports">Return to Reports</a></p>';
} ?>
            </div>
        </div>
    </div>
</div>
