<?php
    // Permissions check
    if (!$adm->canUserAccess(array(1), $roleID)) {
        return;
    }
    
    require_once $_SERVER['DOCUMENT_ROOT'] . '/fys/includes/export.php';
    
    $export = new ExportMethods($obj);
    $results = $export->getSemesterReport();
?>

<div class="body-content" id="body-content-padding">
    <legend>Send End of Semester Report (to Presenters only)</legend>
    <?php
        $send = $_GET['send'];
        if(empty($send)) {
            echo "<blockquote >Preview of the reports can be found below. Please confirm before sending them. <a href='?send=1' class='btn btn-primary' style='font-style: normal;float:right;'>Confirm Send Reports</a></blockquote>";
        } else {
            echo "<h3>Following reports sent out:</h3>";
        }
        
        $collection = $export->separateResultsByID($results);
        foreach($collection as $arr) {
            $tableData = "";
            foreach($arr as $row) {
                $tableData .= "<tr><td>" . $row->IsAttended . "</td>
                                <td>" . $row->PreEventComments . "</td>
                                <td>" . $row->SurveyRating . "</td>
                                <td>" . $row->SurveyParagraph . "</td>";
            }
            
            $export->sendSemesterReport($arr[0], $tableData, $send);
        }
        
        if(!empty($send)) {
            echo "<div class='alert alert-success'>Reports sent out to presenters successfully</div>";
            echo "<a href='". $_settings['current_URL_path'] . "/admin/reports' class='btn btn-default'>Return to Reports</a>";
        }
    ?>
</div>