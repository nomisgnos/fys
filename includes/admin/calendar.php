<?php
    $events = $adm->getEvents();
?>

<h2>Calendar</h2>

<div id="calendar"></div>

<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/js/moment.js"></script>
<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/fullcalendar/fullcalendar.min.js"></script>
<link rel="stylesheet" href="<?=$_settings['current_URL_path'];?>/resources/fullcalendar/fullcalendar.min.css" media="all"/>
<link rel="stylesheet" href="<?=$_settings['current_URL_path'];?>/resources/fullcalendar/fullcalendar.print.min.css" media="print"/>

<style>
thead th {
background: inherit!important;
color: inherit!important;
}
</style>

<script>
    $('#calendar').fullCalendar({
        weekends: false,
        events: [
            <?php
                foreach($events as $event) {
                    $startTime = $event->Date . 'T' . date("G:i", strtotime($event->StartTime));
                    $endTime = $event->Date . 'T' . date("G:i", strtotime($event->EndTime));
                    $eventName = addslashes($event->Name);
                    $eventDescr = filter_var(trim(preg_replace('/\s+/', ' ', addslashes($event->Description))), FILTER_SANITIZE_STRING);
                    $eventColor = ($event->semesterid == 1) ? '#ffa500' : '#0277BD';
                    $eventTextColor = ($event->semesterid == 1) ? '#000' : '#fff';
                    echo "{id : ". $event->EventID . ", url : '/fys/admin/events/edit?EventID=" . $event->EventID. "', title : '" . $eventName .   "', description : '" . $eventDescr 
                            . "', start : '" . $startTime . "', end : '" . $endTime . "', color:'" . $eventColor . "', textColor:'" . $eventTextColor . "'},";
                }
            ?>            
        ],
        header: {
            left:   'prev,next,today,agenda',
            center: 'title',
            right:  'month,agendaWeek,listWeek'
        },
        views: {
            agenda: {
                name: 'test',
                minTime : '7:00:00',
                maxTime : '20:00:00'
            },
            agendaWeek: {
                minTime : '10:00:00',
                maxTime : '20:00:00'
            }
        },
        themeSystem: 'bootstrap3',
        eventRender: function(event, element) {
            $('.fc-today').removeClass('alert');
            $(element).tooltip({title: '<code>ID: ' + event.id + '</code><br>' + event.title, html:true, container:'body'});
        },
        dayClick: function(event, element) {
            $(element).tooltip('hide');
        },
        eventClick: function(calEvent, view) {
            $(calEvent).tooltip('hide');
            if(calEvent.url) {
                window.open(calEvent.url);
                return false;
            }
        }
    });
</script>