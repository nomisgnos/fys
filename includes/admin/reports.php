<?php
    // Permissions check
    if (!$adm->canUserAccess(array(1), $roleID)) {
        return;
    }

    $breadText = '';
    $action = true;
    
    /* Event Circuits */
    switch($circuit) {
            
        case checkCircuit($circuit, 'confirm-email-all'):
            $breadText = 'Confirm Email All Students';
            include 'reports/confirm-email-all.php';
            break;
        case checkCircuit($circuit, 'semester-report'):
            $breadText = 'Semester Report';
            include 'reports/semester-report.php';
            break;
        case checkCircuit($circuit, 'email-all'):
            $breadText = 'Email All Students';
            include 'reports/send-email-all-students.php';
            break;
        case checkCircuit($circuit, 'email-presenters'):
            $breadText = 'Email All Presenters';
            include 'reports/send-email-all-presenters.php';
            break;
        default:
            $action = false;
    }
    if($action) {
        $bread = '<a style="text-decoration: none;" href="' . $_settings['current_URL_path'] . '/admin/reports">Reports</a><li class="breadcrumb-item active">' . $breadText . '</li>';
    } else {
?>
</div>
<div class="body-content" id="body-content-padding">
    <legend>Reports and Global Messaging</legend>
    
    <p style="padding:.5em 0;">
        <a href="<?= $_settings['current_URL_path']; ?>/admin/reports/email-all" class="btn btn-primary btn-add">Email all students</a>
        <a href="<?= $_settings['current_URL_path']; ?>/admin/reports/email-presenters" class="btn btn-primary btn-add">Email all presenters</a></p>
    <p style="padding:.5em 0;">
        <a href="<?= $_settings['current_URL_path']; ?>/includes/admin/reports/export-survey.php" class="btn btn-primary btn-add">Download Excel of all Survey Responses</a> </p>
    <p style="padding:.5em 0;">
        <a href="<?= $_settings['current_URL_path']; ?>/admin/reports/semester-report" class="btn btn-primary btn-add">Send End of Semester Report (to Presenters and Admins only)</a> </p>
</div>

    <?php } ?>