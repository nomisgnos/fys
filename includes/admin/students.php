<?php
    // Permissions check
    if (!$adm->canUserAccess(array(1), $roleID)) {
        return;
    }

    $breadText = '';
    $action = true;
    
    /* Event Circuits */
    switch($circuit) {
        
        /* Event Actions */
        case checkCircuit($circuit, 'post-delete-student-reg'):
            $breadText = 'Post Delete Student Force Registration';
            include 'students/post/post-student-delete-force-reg.php';
            break;
        case checkCircuit($circuit, 'post-student-force-reg'):
            $breadText = 'Post Student Force Registration';
            include 'students/post/post-student-force-reg.php';
            break;
        case checkCircuit($circuit, 'post-student-edit'):
            $breadText = 'Post Edit Student';
            include 'students/post/post-student-edit.php';
            break;
        case checkCircuit($circuit, 'post-student-add'):
            $breadText = 'Post Add Student';
            include 'students/post/post-student-add.php';
            break;
        case checkCircuit($circuit, 'force-reg'):
            $breadText = 'Force Registration';
            include 'students/force-reg.php';
            break;
        case checkCircuit($circuit, 'edit'):
            $breadText = 'Edit Student';
            include 'students/edit-student.php';
            break;
        case checkCircuit($circuit, 'add'):
            $breadText = 'Add Student';
            include 'students/add-student.php';
            break;
        case checkCircuit($circuit, 'delete'):
            $breadText = 'Delete student';
            include 'students/delete-student.php';
            break;
        default:
            $action = false;
    }
    
    if ($action) {
        $bread = '<a style="text-decoration: none;" href="' . $_settings['current_URL_path'] . '/admin/students">Students</a><li class="breadcrumb-item active">' . $breadText . '</li>';
    } else {
        $students = $adm->getStudents();
        
?>
        <div class="body-content" id="body-content-padding">
            <legend>Manage Existing Students <a href="<?= $_settings['current_URL_path']; ?>/admin/students/add" style="float:right!important;" class="btn btn-success btn-add"> New Student</a></legend>
            
            <div id="alert-area"></div>

            <div style="overflow-x: visible;" class="table-responsive">
                <table id="dataStudents" class="table hover" width="100%" style="display:none;">
                    <thead>
                        <tr>
                          <th width="15%">PSU ID</th>
                          <th width="35%">Name</th>
                          <th width="45%">Actions</th>
                          <th width="0%" style="display:none;"></th>
                          <th width="0%" style="display:none;"></th>
                        </tr>
                    </thead>
                    <tbody>
<?php
                    foreach($students as $student) {
                        $constants = $student->UserID . ";" . $student->AccessID . ";" . $student->FName . ";" . $student->LName . ";" . $student->Email . ";" . $student->Notes . ";" . $student->ConfidentialInfo;
                        $encoded = $adm->base64_url_encode($constants);
?>
                        <tr id="body-message">
                            <td id="body-id"><?=$student->AccessID;?></td>
                            <td id="body-name"><?=$student->FName;?> <?=$student->LName?></td>
                            <td>
                                <a class="btn btn-success" style="font-size:12px; margin-right:20px;" href="<?=$_settings['current_URL_path']; ?>/admin/students/force-reg?ID=<?=$encoded?>"><span class="glyphicon glyphicon-plus"></span> Register Event</a>
                                <a class="btn btn-primary" style="font-size:12px; margin-right:20px;" href="<?=$_settings['current_URL_path']; ?>/admin/students/edit?ID=<?=$encoded?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                                <a class="btn btn-danger student-delete" style="font-size:12px;" href="#"><span class="glyphicon glyphicon-trash"></span> Delete</a>
                            </td>
                            <td id="body-email" style="display: none;"><?=$student->Email;?></td>
                            <td id="body-uid" style="display: none;"><?=$student->UserID;?></td>
                        </tr>
<?php
                    }
?>
                  </tbody>
                </table>
            </div>
        </div>

        <script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/dataTables.bootstrap.min.js"></script>
        <script>
            $('#dataStudents').DataTable( {
                columnDefs: [{ "orderable": false, "targets": 2 }, { "searchable": false, "targets": 2 } ],
                order: [[ 1, "asc" ]],
                bDeferRender: true,
                orderClasses: false,
                responsive: true,
                autoWidth: false,
            });
            $('#dataStudents').show();
            $('#dataStudents').wrap('<div class="table-responsive"></div>');
            
            ajaxDeleteStudent();
        </script>
    
<?php } ?>