<?php   
    $urlDecode = $adm->base64_url_decode($_GET['ID']);
    // Format: UserID, AccessID, FName, LName, Email, Notes, ConfidentialInfo
    $user = explode(";", $urlDecode);
    $roles = $adm->getRoles();
    $userRoles = $adm->getUserRoles($user[0]);
    $userid = $user[0];
?>
<style>
.btn {
  display: inline-block;
  margin-bottom: 0;
  font-weight: normal;
  text-align: center;
  vertical-align: middle;
  -ms-touch-action: manipulation;
      touch-action: manipulation;
  cursor: pointer;
  background-image: none;
  border: 1px solid transparent;
  white-space: nowrap;
  padding: 6px 12px;
  height: auto !important;
  font-size: 16px;
  line-height: 1.42857143;
  border-radius: 4px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
textarea { resize:vertical; }
</style>
</div>
<div id="alert-area"></div>
<div class="col-md-8">
<form id="editUser" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend align="left">Edit User: <?=$user[2]?> <?=$user[3]?></legend>


<div class="form-group">
  <label class="col-md-3 control-label" for="accessid">Access ID:</label>  
  <label class="col-md-2 control-label" for="accessid"><?=$user[1]?></label>  
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="firstname">First Name</label>  
  <div class="col-md-8">
    <input id="firstname" name="firstname" type="text" value="<?=$user[2]?>" class="form-control input-md" autofocus>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="lastname">Last Name</label>  
  <div class="col-md-8">
  <input id="lastname" name="lastname" type="text" value="<?=$user[3]?>" class="form-control input-md">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="email">Email</label>  
  <div class="col-md-8">
  <input id="email" name="email" type="text" value="<?=$user[4]?>" class="form-control input-md" required="">
  </div>
</div>

<!-- Dropdown Select -->
<div class="form-group">
    <label class="col-md-3 control-label" for="roles">Roles</label>
    <div class="col-md-9">
        <select id="roles" name="roles[]" class="selectpicker form-control" multiple>
            <?php 
            foreach($roles as $role) {
                foreach($userRoles as $userRole) {
                    $selected = ($role->User_Role_ID == $userRole->User_Role_ID) ? 'selected' : '';
                    if($selected != '') {
                        break;
                    }
                }
            ?>
                <option <?=$selected?> value="<?=$role->User_Role_ID;?>"><?=$role->Role?></option>
            <?php
            }
            ?>
        </select>
    </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-3 control-label" for="notes">Notes</label>
  <div class="col-md-9">
    <textarea class="form-control" id="notes" name="notes"><?=$user[5]?></textarea>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-3 control-label" for="confidential">Confidential Information</label>
  <div class="col-md-9">                     
    <textarea class="form-control" id="confidential" name="confidential"><?=$user[6]?></textarea>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-3 control-label" for="submitbtn"></label>
  <div class="col-md-8">
    <button id="submitbtn" name="submitbtn" class="btn btn-primary">Submit</button>
    <button id="cancelbtn" name="cancelbtn" class="btn btn-danger" type="reset">Reset</button>
  </div>
</div>

</fieldset>
</form>
</div>

<script>
    $("#roles").selectpicker("render");
    ajaxHandleForm('#editUser', 'post-user-edit', 'div#post-id-edit', <?=$userid?>);
</script>