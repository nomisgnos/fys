<?php 
// $match comes from courses.php
$students = $adm->getCourseStudents($match[1]);
$course = $students[0];
?>
<style>
.label-md {
    font-size:13px;
}
.eventlist {
    padding-left: 0;
}

tr:hover {
    background-color:#f7faff;
    
}

.event-name {
    font-family: "Roboto Slab",Georgia,Times,"Times New Roman",serif;
    font-weight: 700;
    font-size: 12px;
    text-transform: uppercase;
}
</style>

<h2><?=$course->Name?> <?=$course->Number?>: Section <?=$course->Section?></h2>
<span class="label label-default label-md">Schedule #: <?=$course->ScheduleNumber?></span>
<span class="label label-default label-md"><?=$course->Instructor?></span>

<table style="margin-top:10px;">
    <tr> <th></th> <th></th> </tr>
<?php

foreach($students as $student) :
?>
    <tr>
        <td>
            <h4 style="margin-top:0px;"><?=$student->FName?> <?=$student->LName?></h4>
            <table>
                <tr> <th></th> <th></th> <th></th> </tr>
                <?php
                    $studentEvents = $adm->getStudentEvents($student->UserID);
                    
                    foreach($studentEvents as $event) :
                        if ($event->isAttended == '') {
                            $attended = "<span style='font-size:16px;color:blue;padding-right:3px;' class='fa fa-registered'></span> Registered";
                        } elseif ($event->isAttended == 'Y') {
                            $attended =  "<span style='font-size:16px;color:green;padding-right:3px;' class='glyphicon glyphicon-ok-circle'></span> Attended";
                        } elseif ($event->isAttended == 'N') {
                            $attended =  "<span style='font-size:16px;color:red;padding-right:3px;' class='glyphicon glyphicon-remove-circle'></span> Missed";
                        }
                ?>
                    <tr>
                        <td style='width:20px;'><a href="<?= $_settings['current_URL_path'];?>/admin/events/<?=$event->EventID?>"><span style='border-radius:.7em; float:left;' class='label label-primary label-event' title='Event ID'>ID:<?=$event->EventID?></span></a></td>
                        <td class='event'><span class='event-name'><a href="<?= $_settings['current_URL_path'];?>/admin/events/<?=$event->EventID?>"><?=$event->Name?></a></span> (<?=$event->Date?>)<br/><?=$attended?></td>
                        <td><span style='float:right;' class='label label-default'><?=$event->Type?></span></td>
                    </tr>
                
                <?php
                    endforeach;
                ?>
            </table>
        </td>
        <td><a href="mailto:<?=$student->AccessID?>@psu.edu"><?=$student->AccessID?></a></td>
    </tr>
<?php
endforeach;
?>

</table>