<?php   
    $urlDecode = !empty($_GET['ID']) ? $adm->base64_url_decode($_GET['ID']) : null;
    $instructors = $adm->getInstructors();
    $assistants = $adm->getAssistants();
?>
</div>
<div id="alert-area"></div>
<div class="col-md-8">
<form id="addCourse" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend align="left">Add a new course</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="ScheduleNumber">Schedule Number</label>  
  <div class="col-md-8">
    <input id="ScheduleNumber" name="ScheduleNumber" type="text" class="form-control input-md" autofocus>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="name">Course Name</label>  
  <div class="col-md-8">
  <input id="Name" name="Name" type="text" class="form-control input-md">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="number">Course Number</label>  
  <div class="col-md-8">
  <input id="Number" name="Number" type="text" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="Section">Section</label>  
  <div class="col-md-8">
  <input id="Section" name="Section" type="text" class="form-control input-md">
  </div>
</div>

<!-- Dropdown Select -->
<div class="form-group">
    <label class="col-md-3 control-label" for="Instructor">Instructor</label>
    <div class="col-md-8">
        <select class="selectpicker show-tick" data-dropup-auto="false" data-width="400px" data-live-search="true" data-live-search-placeholder="Search Instructor" id="Instructor" name="Instructor">
            <?php 
            foreach($instructors as $instructor) {
                $selected = ($instructor->InstructorID == $course[5]);
                if ($selected) {
                ?>
                    <option selected value="<?=$instructor->InstructorID;?>"><?=$instructor->Instructor?> (<?=$instructor->InstructorID?>)</option>
                <?php
                } else {
                ?>
                    <option value="<?=$instructor->InstructorID;?>"><?=$instructor->Instructor?> (<?=$instructor->InstructorID?>)</option>
                <?php
                }
            }
            ?>
        </select>
    </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="InstrEmail">Instructor Email</label>  
  <div class="col-md-8">
  <input id="InstrEmail" name="InstrEmail" type="text" class="form-control input-md" required="">
  </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="InstructorID">Instructor PSU Access ID</label>  
  <div class="col-md-8">
  <input id="InstructorID" name="InstructorID" type="text" class="form-control input-md">
  </div>
</div>

<!-- Dropdown Select -->
<div class="form-group">
    <label class="col-md-3 control-label" for="MentorID">Instructor Assistant</label>
    <div class="col-md-8">
        <select id="MentorID" name="MentorID" class="form-control">
            <option selected value="0"?>Select One</option>
            <?php 
            foreach($assistants as $assistant) {
            ?>
                    <option value="<?=$assistant->AccessID;?>"><?=$assistant->FName?> <?=$assistant->LName?> (<?=$assistant->AccessID?>)</option>
            <?php
            }
            ?>
        </select>
    </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="MentorEmail">Instructor Assistant's Email</label>  
  <div class="col-md-8">
  <input id="MentorEmail" name="MentorEmail" type="text" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="MeetingTimes">Meeting Times:</label>  
  <div class="col-md-8">
  <input id="MeetingTimes" name="MeetingTimes" type="text" class="form-control input-md">
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-3 control-label" for="submitbtn"></label>
  <div class="col-md-8">
    <button id="submitbtn" name="submitbtn" class="btn btn-primary">Submit</button>
    <button id="cancelbtn" name="cancelbtn" class="btn btn-danger">Reset</button>
  </div>
</div>

</fieldset>
</form>
</div>

<script>
    ajaxHandleForm('#addCourse', 'post-add-course', 'div#post-id-add');
</script>