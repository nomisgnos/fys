<?php
    
if ( !isset($_POST['cid']) || !isset($_POST['name']) || empty($_POST['cid']) || empty($_POST['name']) ) {
    $msg = "Error! Could not delete course!";
} else {
    $id = $_POST['cid'];
    $name = $_POST['name'];
    // TODO: DB testing stuff
    $response = $adm->deleteCourse($id);
    $msg = "<strong>Success!</strong> Course: <strong>" . $name . "</strong> (#" . $id . ") deleted!";
}
?>

<div id="post-id-delete">
<?=$msg?>
</div>