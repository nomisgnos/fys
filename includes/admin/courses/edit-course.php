<?php   

    if(!isset($_GET['ID'])) {
        echo "<div class='alert alert-danger'>Error: ID Missing</div>";
        echo "<script>redirectTo('/fys/admin/courses/', 1000);</script>";
        exit();
    }
    $urlDecode = $adm->base64_url_decode($_GET['ID']);
    // Format: ID, Name, Number, Section, ScheduleNumber, InstructorID, InstrEmail, MentorEmail, MeetingTimes
    $course = explode(";", $urlDecode);
    $courseid = $course[0];
    $instructors = $adm->getInstructors();
?>
<style>
textarea { resize:vertical; }
</style>
</div>
<div id="alert-area"></div>
<div class="col-md-8">
<form id="editCourse" class="form-horizontal">
<fieldset>

<legend align="left">Edit Course</legend>

<div class="form-group">
  <label class="col-md-3 control-label" for="ScheduleNumber">Schedule Number</label>  
  <div class="col-md-8">
    <input id="ScheduleNumber" name="ScheduleNumber" type="text" value="<?=$course[4]?>" class="form-control input-md" autofocus>
  </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="name">Name</label>  
  <div class="col-md-8">
  <input id="Name" name="Name" type="text" value="<?=$course[1]?>" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="number">Number</label>  
  <div class="col-md-8">
  <input id="Number" name="Number" type="text" value="<?=$course[2]?>" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="Section">Section</label>  
  <div class="col-md-8">
  <input id="Section" name="Section" type="text" value="<?=$course[3]?>" class="form-control input-md">
  </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="Instructor">Instructor</label>
    <div class="col-md-8">
        <select class="selectpicker show-tick" data-dropup-auto="false" data-width="100%" data-live-search="true" data-live-search-placeholder="Search Instructor" id="Instructor" name="Instructor">
            <?php
            foreach($instructors as $instructor) {
                if (trim(strtolower($instructor->InstructorID)) == $course[5]) {
            ?>
                    <option selected value="<?=$instructor->InstructorID;?>"><?=$instructor->Instructor?> (<?=$instructor->InstructorID?>)</option>
            <?php
                } else {
            ?>
                    <option svalue="<?=$instructor->InstructorID;?>"><?=$instructor->Instructor?> (<?=$instructor->InstructorID?>)</option>
            <?php
                }
            }
            ?>
        </select>
    </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="InstrEmail">Instructor Email</label>  
  <div class="col-md-8">
  <input id="InstrEmail" name="InstrEmail" type="text" value="<?=$course[6]?>" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="InstructorID">Instructor PSU Access ID</label>  
  <div class="col-md-8">
    <input id="InstructorID" name="InstructorID" type="text" value="<?=$course[5]?>" class="form-control input-md">
  </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="MentorID">Instructor Assistant</label>
    <div class="col-md-8">
        <select id="MentorID" name="MentorID" class="form-control">
            <?php 
            foreach($roles as $role) {
                if ($role->User_Role_ID == $course[8]) {
                ?>
                    <option selected value="<?=$role->User_Role_ID;?>"><?=$role->Role?></option>
                <?php
                } else {
                ?>
                    <option value="<?=$role->User_Role_ID;?>"><?=$role->Role?></option>
                <?php
                }
            }
            ?>
        </select>
    </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="MentorEmail">Instructor Assistant's Email</label>  
  <div class="col-md-8">
  <input id="MentorEmail" name="MentorEmail" type="text" value="<?=$course[7]?>" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="MeetingTimes">Meeting Times:</label>  
  <div class="col-md-8">
  <input id="MeetingTimes" name="MeetingTimes" type="text" value="<?=$course[8]?>" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="submitbtn"></label>
  <div class="col-md-8">
    <button id="submitbtn" name="submitbtn" class="btn btn-primary">Submit</button>
    <button id="cancelbtn" name="cancelbtn" class="btn btn-danger" type="reset">Reset</button>
  </div>
</div>

</fieldset>
</form>
</div>

<script>
    ajaxHandleForm('#editCourse', 'post-edit-course', 'div#post-id-edit', <?=$courseid?>);
</script>