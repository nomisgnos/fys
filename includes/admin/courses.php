<?php
    // Permissions check
    if (!$adm->canUserAccess(array(1, 2, 3), $roleID)) {
        return;
    }
    $searchQuery = '';
    $breadText = '';
    $action = true;
    
    /* Event Circuits */
    switch($circuit) {

        case checkCircuit($circuit, 'post-edit-course'):
            $breadText = 'Post Edit Event';
            include 'courses/post/post-edit-course.php';
            break;
        case checkCircuit($circuit, 'post-add-course'):
            $breadText = 'Post Add Event';
            include 'courses/post/post-add-course.php';
            break;
        /* Event Actions */
        case checkCircuit($circuit, 'delete'):
            $breadText = 'Delete course';
            include 'courses/delete-course.php';
            break;
        case checkCircuit($circuit, 'edit'):
            $breadText = 'Edit course';
            include 'courses/edit-course.php';
            break;
        case checkCircuit($circuit, 'add'):
            $breadText = 'Add course';
            include 'courses/add-course.php';
            break;
        default:
            $action = false;
    }
    
    if(!$action) {
        preg_match("/admin\/courses\/(\d+)/", $circuit, $match);
        if(!empty($match)) {
            $breadText = '#' . $match[1];
            include 'courses/course-students.php';
            $action = true;
        } else {
            preg_match("/admin\/courses\/search\/(\d+)/", $circuit, $match);
            if(!empty($match)) {
                $searchQuery = $match[1];
            }
        }

        

    }
    
    if ($action) {
        $bread = '<a style="text-decoration: none;" href="' . $_settings['current_URL_path'] . '/admin/courses">Courses</a><li class="breadcrumb-item active">' . $breadText . '</li>';
    } else  {
        $isAdmin = in_array(1, $roleID);
        $isInstructor = in_array(2, $roleID);
        $isAssistant = in_array(3, $roleID);
        $courses = (($isInstructor || $isAssistant) && !$isAdmin) ? $adm->getMyCourses($accessID) : $adm->getCourses();     
?>

</div>
<style>
.tooltip {
    font-size:12px;
}
</style>

<div class="body-content" id="body-content-padding">
    <legend>
        Manage <?=($isAdmin) ? "Existing" : "My" ?> Courses 
        <?php if($isAdmin) {?>
        <a href="<?= $_settings['current_URL_path'];?>/admin/courses/add" class="btn btn-success btn-add" style="float:right!important;">New Course</a>
        <?php } ?>
    </legend>
    <div id="alert-area"></div>
        <table id="dataCourses" class="table hover" width="100%" style="display:none;">
            <thead>
                <tr><th style="width:80px;">Schedule #</th><th>Course</th><th>Instructor</th><th><?php if($isAdmin) {echo "Actions";} ?></th><th></th></tr>
            </thead>
            <tbody>
        <?php
            foreach($courses as $course) {
                $constants = $course->CourseID . ";" . $course->Name . ";" . $course->Number . ";" . $course->Section . ";" . $course->ScheduleNumber  . ";" . $course->InstructorID  . ";" . $course->InstrEmail  . ";" . $course->MentorEmail  . ";" . $course->MeetingTimes . ";" . $course->Instructor;
                $encoded = $adm->base64_url_encode($constants);
        ?>      <tr class="course-row">
                    <td><span id="body-id" style="display:none;"><?=$course->CourseID?></span><span style="margin-top:5px; font-size:13px; cursor:pointer;" data-delay='{"show":0, "hide":100}' data-placement="auto left" data-toggle="tooltip" title="Copy to clipboard" data-original-title="Copy to clipboard" class="label label-default clipme" data-clipboard-text="<?=$course->ScheduleNumber?>"><?=$course->ScheduleNumber?></span></td>
                    <td id="body-title"><?=$course->Name?> <?=$course->Number?>: Section <?=$course->Section?></td>
                    <td id="body-message"><?=$course->Instructor?>, <a href="mailto:<?=$course->InstrEmail?>"><?=$course->InstructorID?></a></td>
                    <td>
                    <?php if($isAdmin) { ?>
                        <a class="btn btn-primary btn-edit" style="" href="<?=$_settings['current_URL_path']; ?>/admin/courses/edit?ID=<?=$encoded?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                        <a class="btn btn-danger course-delete btn-delete" href="#"><span class="glyphicon glyphicon-trash"></span> Delete</a>
                    <?php } ?>
                    </td>
                    <td><a class="btn btn-default btn-xs btn-delete" href="<?=$_settings['current_URL_path']; ?>/admin/courses/<?=$course->CourseID?>"><span class="fa fa-user"></span> Students</a></td>
                </tr>
        <?php
            }
        ?>
            </tbody>
        </table>
</div>
<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/dataTables.bootstrap.min.js"></script>
<script>

    // Clipboard for schedule number
    $('.clipme').click(function(){
        var e = this;
        copyToClipboard(e);
        $(e).attr('data-original-title', 'Copied!').tooltip('show');
        setTimeout(function() { $(e).attr('data-original-title', 'Copy to clipboard!').tooltip('hide');}, 500);
    });
    
    // Create trigger for delete course event
    ajaxDeleteCourse();
    
    /* To fix extra spaces between course title and course code (issue from DB) */
    $("#body-title").each(function(){
        $(this).text( $(this).text().replace(/ +/g, ' ') );
    });
    
    /* Courses DataTable */
    var t = $('#dataCourses').DataTable({
        bDeferRender: true,
        columnDefs: [{ "orderable": false, "targets": [3,4] }, { "searchable": false, "targets": [3,4] } ],
        order: [[ 2, "asc" ]],
        orderClasses: false,
        responsive: true,
        autoWidth: true,
        /* Disable smart search to match exact phrase */
        search: {smart: false}
    });
    if('<?=$searchQuery?>' != '') {
        t.search('<?=$searchQuery?> ', false, false).draw();
    }
    $('#dataCourses').show();
    
    // Enable tooltips
    $('[data-toggle="tooltip"]').tooltip();
</script>
    
<?php } ?>