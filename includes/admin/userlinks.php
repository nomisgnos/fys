<div class="wrapper">
    <div class="clearfix">
        <div class="column-1">
            <div id="content-header">
                
                <?php
                    if(isset($accessID)) {
                ?>
                        <div style="padding:7.5px;" id="userlinks" class="no-print floatright">
                            <span>Logged in as &nbsp;<strong><?php echo $accessID; ?> ( <?=$adm->printRoleNames($roleID)?>)</strong></span>
                            <ul>
                                <li><a href="<?= $_settings['current_URL_path']; ?>/register"><div class="fa fa-user"></div> Student View</a></li>
                                <?php if(in_array(1, $roleID)) {?>
                                    <li><a href="<?= $_settings['current_URL_path'];?>/admin/settings"><div class="fa fa-cog"></div> Settings</a></li>
                                <?php }?>
                                <li><a href="https://webaccess.psu.edu/cgi-bin/logout"><div class="glyphicon glyphicon-log-out"></div> Logout</a></li>
                            </ul>
                        </div><!--.userlinks-->
                <?php } ?>
                
                <ol style="padding-bottom:7.5px;" class="no-print breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= $_settings['current_URL_path'];?>/admin">FYS Admin</a></li>
                  <li class="breadcrumb-item active">%BREAD%</li>
                </ol><!--.breadcrumbs"-->

                <style>
                    .breadcrumb {
                        margin-bottom:0px;
                    }
                    .btn-nav {
                        text-align: center;
                        background-color: #fffff;
                        border: 0;
                        color: #777;
                        box-shadow:  inset 0px -11px 8px -10px #CCC;
                        -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
                        -moz-box-sizing: border-box;    /* Firefox, other Gecko */
                        box-sizing: border-box; /* Opera/IE 8+ */
                        margin-bottom: 0px;
                    }
                    .btn-nav:hover {
                        color: #445aad;
                        cursor: pointer;
                        -webkit-transition: color 1s; /* For Safari 3.1 to 6.0 */
                        transition: color 1s;
                    }
                    .btn-nav.active {
                        color: #445aad;
                        //padding: 2px;
                        border-top: 2px solid #8EA8C3!important;
                        border-left: 0;
                        border-right: 0;
                        border-bottom: 2px solid #2781de!important;
                        -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
                        -moz-box-sizing: border-box;    /* Firefox, other Gecko */
                        box-sizing: border-box; /* Opera/IE 8+ */
                    }
                    .btn-nav .glyphicon, .btn-nav .fa {
                        color: #465775;
                        padding-top: 16px;
                        font-size: 24px;
                    }
                    .btn-nav .glyphicon:hover, .btn-nav .fa:hover {
                        color: #202C39;
                        transition: color 0.2s;
                    }
                    .btn-nav.active p {
                        margin-bottom: 8px;
                    }
                    
                    .btn-nav h4 {
                        font-family: "Open Sans","HelveticaNeue","Helvetica","Arial",sans-serif!important;
                    }
                    
                    @media (max-width: 680px) {
                        .btn-group-nav  {
                            float: none !important;
                            width: 100% !important;
                            max-width: 100% !important;
                        }
                        .btn-group-nav .btn-nav h4 {
                            display:none;
                        }
                    }
                    @media (max-width: 600px) {
                        .btn-nav .glyphicon, .btn-nav .fa {
                            padding-top: 12px;
                            font-size: 26px;
                        }
                        
                        .label {
                            margin: 0;
                            padding: 2px;
                            border: 0;
                        }
                    }
                </style>

                <div style="margin-bottom:0px;" class="no-print"> 
                    <div class="btn-group btn-group-nav  btn-group-justified">
                        <?php echo $adm->printNewNavigation($roleID); ?>
                    </div>
                    <hr class="style2">
                </div>

