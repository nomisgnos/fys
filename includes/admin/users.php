<?php
    // Permissions check
    if (!$adm->canUserAccess(array(1), $roleID)) {
        return;
    }
    
    function newUserRoleTable($role, $curRole) {
        if ($curRole != 0) {
?>
              </tbody>
            </table>
<?php
        }
?>
            <h2><?=$role;?></h2>
            
                <table id="table<?=$curRole?>" class="table table-hover" style="display:none;">
                    <thead>
                        <tr>
                          <th style="display:none;">UID</th>
                          <th width="15%">PSU ID</th>
                          <th width="30%">Name</th>
                          <th width="30%">Email</th>
                          <th width="25%">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
    }

    $breadText = '';
    $action = true;
    /* Event Circuits */
    switch($circuit) {
        
        /* Event Actions */
        case checkCircuit($circuit, 'confirm-edit-event'):
            $breadText = 'Edit Event';
            include 'users/confirm-edit-event.php';
            break;
        case checkCircuit($circuit, 'post-user-edit'):
            $breadText = 'Post Edit User';
            include 'users/post/post-user-edit.php';
            break;
        case checkCircuit($circuit, 'post-user-add'):
            $breadText = 'Post Add User';
            include 'users/post/post-user-add.php';
            break;
        case checkCircuit($circuit, 'delete'):
            $breadText = 'Delete User';
            include 'users/delete-user.php';
            break;
        case checkCircuit($circuit, 'edit'):
            $breadText = 'Edit User';
            include 'users/edit-user.php';
            break;
        case checkCircuit($circuit, 'add'):
            $breadText = 'Add User';
            include 'users/add-user.php';
            break;
        
        default:
            $action = false;
    }
    
    if ($action) {
        $bread = '<a style="text-decoration: none;" href="' . $_settings['current_URL_path'] . '/admin/users">Users</a><li class="breadcrumb-item active">' . $breadText . '</li>';
    } else  {
        $users = $adm->getUsers();

?>

</div>

        <div class="body-content" id="body-content-padding">
            <legend>Manage Existing Users <a href="<?= $_settings['current_URL_path']; ?>/admin/users/add" class="btn btn-success btn-add" style="float:right!important;">New User</a></legend>
            
            <div id="alert-area"></div>
<?php
                $curRole = 0;
                foreach($users as $user) {
                    $constants = $user->UserID . ";" . $user->AccessID . ";" . $user->FName . ";" . $user->LName . ";" . $user->Email . ";" . $user->Notes . ";" . $user->ConfidentialInfo;
                    if($user->User_Role_ID > $curRole) {
                        newUserRoleTable($user->Role, $curRole);
                        $curRole = $user->User_Role_ID;
                    }
?>
                    <tr id="body-message">
                        <td id="body-uid" style="display:none;"><?=$user->UserID?></td>
                        <td id="body-id"><?=$user->AccessID;?></td>
                        <td id="body-name"><?=$user->FName;?> <?=$user->LName;?></td>
                        <td id="body-email"><a href="mailto:<?=$user->Email;?>"><?=$user->Email;?></a></td>
                        <td>
                            <a class="btn btn-primary" style="font-size:12px; margin-right:15px;" href="<?=$_settings['current_URL_path']; ?>/admin/users/edit?ID=<?=$adm->base64_url_encode($constants);?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                            <a class="btn btn-danger user-delete" href="#" style="font-size:12px;"><span class="glyphicon glyphicon-trash"></span> Delete</a>
                        </td>
                    </tr>
<?php
                }
?>
                    </tbody>
                </table>
        </div>
        <script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/dataTables.bootstrap.min.js"></script>
        <script>
            $('.table').DataTable({
                initComplete: function(settings) {
                    var controls = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate, .dataTables_length, .dataTables_filter, .dataTables_info');
                    controls.toggle(this.api().page.info().pages > 1);
                },
                columnDefs: [{ "orderable": false, "targets": 4 }, { "searchable": false, "targets": 4 } ],
                order: [[ 2, "asc" ]],
                bDeferRender: true,
                responsive: true,
                autoWidth: false,
            });
            $('.table').show();
            $('.table').wrap('<div class="table-responsive"></div>');
            
            ajaxDeleteUser();
        </script>
<?php
    }
?>