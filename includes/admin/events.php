<?php
/* FIXME this needs reworked, discuss with Simon again how semesters will be handled in the future */

    // Permissions check
    if (!$adm->canUserAccess(array(1, 2, 3, 4), $roleID)) {
        return;
    }

    $breadText = '';
    $searchQuery = '';
    $action = true;
    $semester = null;
    
    $isPresenter = in_array(4, $roleID);
    $isAdmin = in_array(1, $roleID);
        
    switch($circuit) {
        /* Event Post */
        case checkCircuit($circuit, 'post-add-event'):
            $breadText = 'Add Event';
            include 'events/post-events/post-add-event.php';
            break;
        case checkCircuit($circuit, 'post-edit-event'):
            $breadText = 'Edit Event';
            include 'events/post-events/post-edit-event.php';
            break;
            
        /* Report Confirmations */
        case checkCircuit($circuit, 'confirm-pre-event-email'):
            $breadText = 'Send Pre-Event Email';
            include 'events/post-actions/confirm-pre-event-email.php';
            break;
        case checkCircuit($circuit, 'confirm-email-attendance-report'):
            $breadText = 'Confirm and Email Attendance Report';
            include 'events/post-actions/confirm-email-attendance-report.php';
            break;
        case checkCircuit($circuit, 'confirm-develop-attendance-report'):
            $breadText = 'Develop Attendance Report';
            include 'events/post-actions/confirm-develop-attendance-report.php';
            break;
            
        /*  Pre-event Email */
        case checkCircuit($circuit, 'pre-event-email'):
            $breadText = 'Send Pre-Event Email';
            include 'events/pre-event-email.php';
            break;

        /* Attendance Report */
        case checkCircuit($circuit, 'develop-attendance-report'):
            $breadText = 'Develop Attendance Report';
            include 'events/develop-attendance-report.php';
            break;
        case checkCircuit($circuit, 'email-attendance-report'):
            $breadText = 'Confirm and Email Attendance Report';
            include 'events/email-attendance-report.php';
            break;

        /* View Reports */
        case checkCircuit($circuit, 'view-attendance-report'):
            $breadText = 'View Attendance Report';
            include 'events/view-reports/view-attendance-report.php';
            break;
        case checkCircuit($circuit, 'view-pre-event-comments'):
            $breadText = 'View Pre-Event Comments';
            include 'events/view-reports/view-pre-event-comments.php';
            break;
        case checkCircuit($circuit, 'view-feedback-report'):
            $breadText = 'View Event Feedback Report';
            include 'events/view-reports/view-feedback-report.php';
            break;
        
        /* Primary Actions */
        case checkCircuit($circuit, 'add'):
            $breadText = 'Add Event';
            include 'events/add-event.php';
            break;
        case checkCircuit($circuit, 'delete'):
            $breadText = 'Delete Event';
            include 'events/delete-event.php';
            break;
        case checkCircuit($circuit, 'edit'):
            $breadText = 'Edit Event';
            include 'events/edit-event.php';
            break;
            
        /* Semester Selection */
        case checkCircuit($circuit, 'spring'):
            $semester = 'spring';
            $action = false;
            $bread = '<a style="text-decoration: none;" href="' . $_settings['current_URL_path'] . '/admin/events">Events</a><li class="breadcrumb-item active">Spring</li>';
            break;
            
        case checkCircuit($circuit, 'fall'):
            $semester = 'fall';
            $action = false;
            $bread = '<a style="text-decoration: none;" href="' . $_settings['current_URL_path'] . '/admin/events">Events</a><li class="breadcrumb-item active">Fall</li>';
            break;
        case checkCircuit($circuit, 'all'):
            $semester = 'all';
            $action = false;
            $bread = '<a style="text-decoration: none;" href="' . $_settings['current_URL_path'] . '/admin/events">Events</a>';
            break; 
        default:
            $action = false;
    }
    
    if(!$action) {
        preg_match("/admin\/events\/(\d+)/", $circuit, $match);
        if(!empty($match)) {
            $semester = 'all';
            $searchQuery = 'ID: ' . $match[1];
        }
    }
    
    if ($action) {
        $bread = '<a style="text-decoration: none;" href="' . $_settings['current_URL_path'] . '/admin/events">Events</a><li class="breadcrumb-item active">' . $breadText . '</li>';
    } else {        
        $semesterID = ($semester == 'spring') ? '2' : (($semester == 'fall') ? '1' : (($semester == 'all' ? '0' : $adm->getCurrentSemester())));
        $events = $adm->getAdminEvents($semesterID);
?>
    
    </div><!-- #content-header -->          
<style>
.panel-heading:hover {
  background-color: #dfdfdf;
}
.list-group-item:hover {
    background-color: #f7faff;
}
.panel-heading.collapsed .show-description:before {
    /* symbol for "collapsed" panels */
    content: "\e080";    /* adjust as needed, taken from bootstrap.css */
}
.show-description:hover {
    cursor: pointer;
    cursor: hand;
}
.show-description:before {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';  
    content: "\e114";    
    float: left;        
    color: grey;         
    margin-right:-10px;
}

.label-event {
    font-size: 12px;
}
.event-description {
    padding:10px;
    margin-left:21px;
}
.panel-btn {
    font-size:12px; 
    padding:4px;
    margin-bottom:5px;
}
.col-md-8 strong, .col-md-4 strong {
    margin-right:10px;
}
.admin-action {
    padding-bottom:5px;
}

.panel-heading.row {
  margin: 0;
  padding-left: 0;
  padding-right: 0;
}

@media (min-width: 992px) {
    .col-md-8 {
        width: 65.666667%;
    }
}

tr.even td, tr.odd td {
    background-color:#fff!important;
}

tr, td {
    padding:0!important;
    margin:0!important;
}

.inline-form-control {
    display:inline-block; 
    width:auto;
    font-size:14px;
}

.label-semester {
    margin-left:10px;
    font-size:14px;
}
</style>
<div id="alert-area"></div>
            <div class="body-content" id="body-content-padding">
                <legend align="left">Manage <?php if ($isPresenter) echo 'My '; ?> Events <?php echo ($semesterID == '1') ? '<label class="label label-warning label-semester">Fall</label>' : (($semesterID == '2')  ? '<label class="label label-info label-semester">Spring</label>' : '');?>  <a href="<?= $_settings['current_URL_path']; ?>/admin/events/add" class="btn btn-success btn-add" style="float:right!important;">New Event</a></legend>
                <div id="semester-selector">
                    <div class="row">
                        <div class="col-md-4">
                            <form style="display:inline-block;" method="get">
                                Filter by <strong>Semester</strong>&nbsp;
                                <select class="form-control inline-form-control" name="semester" onChange="location.href=$(this).val();">
                                    <option value="<?=$_settings['current_URL_path'];?>/admin/events/all" <?php if ($semesterID == '0') echo 'selected';?>>All</option>
                                    <option value="<?=$_settings['current_URL_path'];?>/admin/events/fall" <?php if ($semesterID == '1') echo 'selected';?>>Fall</option>
                                    <option value="<?=$_settings['current_URL_path'];?>/admin/events/spring" <?php if ($semesterID == '2') echo 'selected';?>>Spring</option>
                                </select>
                            </form>   
                        </div>
                    </div>
                </div><br />
                <table id="dataEvents" class="table hover" width="100%" style="display:none;">
                    <thead>
                        <tr style="display:none;">
                            <th>Event</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($events as $event) :
                            $eventSem = ($event->semesterid == 1) ? "class='label label-warning label-event'>Fall" : "class='label label-info label-event'>Spring";
                            $eventTitleClass = ($event->active == '0') ? 'col-md-7' : 'col-md-8';
                            $eventLabelClass = ($event->active == '0') ? 'col-md-5' : 'col-md-4';
                        ?>
                        <tr> <td>
                            <div class="panel panel-default">
                                <div id="<?=$event->EventID;?>" class="panel-heading collapsed row" data-toggle="collapse" data-target="#eventDescription<?= $event->EventID;?>" aria-expanded="false" aria-controls="eventDescription<?=$event->EventID;?>">

                                    <div style="margin-top:5px;" class="<?=$eventTitleClass?> col-sm-12">
                                        <a style="margin-right:15px; margin-left:-15px; float:left; padding-right:5px;" class="show-description" data-placement="top" data-toggle="tooltip" title="Show Description"></a>
                                        <span style="border-radius:.7em;" class="label label-default label-event" data-toggle="tooltip" title="Event ID"><span style="display:none;"><?=$event->Date?></span>ID: <?= $event->EventID;?></span>
                                        <span id="event-title" class="panel-message" data-toggle="tooltip" title="Event Name"><strong><?=$event->Name;?></strong></span>        
                                    </div>
                                    <div style="margin-top:5px;margin-left:-5px;" class="<?=$eventLabelClass?> col-sm-12">
                                        <?php if ($event->active == '0'){ echo '<span data-toggle="tooltip" title="Event Status" class="label label-danger label-event">Awaiting approval</span>';} ?>
                                        <span data-toggle="tooltip" title="Semester"<?=$eventSem;?></span>
                                        <span id="event-label" class="label label-success label-event" data-toggle="tooltip" title="Event Type"><?=$event->Type;?></span>

                                        <?php if($isAdmin) { ?>
                                            <a class="btn btn-danger event-delete panel-btn pull-right" href="#">
                                                <span class="glyphicon glyphicon-trash"></span> Delete </a>
                                            <a class="btn btn-primary panel-btn pull-right" style="margin-right:10px;" href="events/edit?EventID=<?=$event->EventID;?>">
                                                <span class="glyphicon glyphicon-pencil"></span> Edit </a>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <span id="event-id" style="display:none;"><?=$event->EventID;?></span>
                                <div id="eventDescription<?= $event->EventID;?>" class="panel-body collapse" style="padding:0;">
                                    <div class="event-description"><strong>Description:</strong> <?=$event->Description;?></div>
                                </div>
                                
                                <ul class="list-group">
                                    <div id="event-message">
                                        <li class="list-group-item row no-margin"><div class="col-md-8"><strong>Date & Time:</strong><?= date_format(date_create($event->Date), "F d, Y");?>, <?= $event->StartTime;?> - <?= $event->EndTime;?></div><div class="col-md-4"><strong>Registered:</strong><?= $event->Registered;?> / <?=$event->Seats;?></div></li>
                                        <li class="list-group-item row no-margin"><div class="col-md-8"><strong>Location:</strong><?= $event->Location;?></div></li>
                                        <li class="list-group-item row no-margin"><div class="col-md-8"><strong>Primary Presenter:</strong>
                                        <?= $event->Presenter;?> - <?=$event->PresenterLName?>, <?=$event->PresenterFName?></div><div class="col-md-4"><strong>Co-Presenter(s):</strong>
                                            <?php if(!empty($event->CoPres1)){ echo $event->CoPres1 . '-' . $event->CoPres1FName . ', ' . $event->CoPres1LName . '<br>';} ?>
                                            <?php if(!empty($event->CoPres2)){ echo $event->CoPres2 . '-' . $event->CoPres2FName . ', ' . $event->CoPres2LName;} ?>
                                        </div></li>
                                    </div>
                                    <?php if($isAdmin || trim(strtolower($accessID)) == trim(strtolower($event->Presenter))) {?>
                                    <li class="list-group-item row no-margin">
                                        <div class="col-md-8"><em><strong>Administrator Action Required</strong></em></div>
                                        <div class="col-md-4"><em><strong>Optional Reports</em></strong></div>
                                        
                                        <hr class="style2">
                                        <div class="row">
                                            <div class="col-md-8 admin-action"><a class="btn btn-default" href="events/pre-event-email?EventID=<?= $event->EventID;?>"><span style="color:#1f78d1;"class="fa fa-envelope-o"></span> Pre-Event Email to Registered Students</a></div>
                                            <div class="col-md-4 admin-action"><a class="btn btn-default" href="events/view-attendance-report?EventID=<?= $event->EventID;?>">Registration Report</a></div>
                                        </div>
                                        <div class="row">
                                        <?php if($isAdmin || trim(strtolower($accessID)) == trim(strtolower($event->Presenter))) { ?>
                                        <div class="col-md-8 admin-action"><a class="btn btn-default" href="events/develop-attendance-report?EventID=<?= $event->EventID;?>"><span style="color:#1f78d1;"class="fa fa-flag-o"></span> Develop Attendance Report</a></div>
                                        <?php } ?>
                                        <div class="<?=($isAdmin) ? 'col-md-4' : 'col-md-8'?> admin-action"><a class="btn btn-default" href="events/view-pre-event-comments?EventID=<?= $event->EventID;?>">Pre-Event Comments</a></div>
                                        </div>
                                        <div class="row">
                                        <?php if($isAdmin) { ?>
                                        <div class="col-md-8 admin-action"><a class="btn btn-default" href="events/email-attendance-report?EventID=<?= $event->EventID;?>"><span style="color:#1f78d1;"class="fa fa-check-square-o"></span> Confirm attendance and generate automatic email to attendees</a></div>
                                        <?php } ?>
                                        <div class="col-md-4 admin-action"><a class="btn btn-default" href="events/view-feedback-report?EventID=<?= $event->EventID;?>">Feedback Report</a></div>
                                        </div>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </td> </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
<?php 
            }
?> 
        </div><!-- .column-1 -->
    </div><!-- .clearfix -->
</div><!-- .wrapper -->
    
<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/dataTables.bootstrap.min.js"></script>
<script>
    var t = $('#dataEvents').DataTable({
        bDeferRender: true,
        orderClasses: false,
        //responsive: true,
        autoWidth: false,
        // Disable smart search to match exact phrase 
        search: {smart: false},
        stateSave: true,
        lengthMenu: [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]]
    });
    
    if('<?=$searchQuery?>' != '') {
        t.search('<?=$searchQuery?> ', false, false).draw();
    }
    
    $('#dataEvents').show();

    $('[data-toggle="tooltip"]').tooltip(); 
    
    ajaxDeleteEvent();
</script>
