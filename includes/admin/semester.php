<?php
    // Permissions check
    if (!$adm->canUserAccess(array(1), $roleID)) {
        return;
    }
    
    include 'includes/semester.php';
    
    $sem = new SemesterMethods($adm);
    
    if(isset($_POST['SubmitForm'])){
        $semesterID = $_POST['semesterid'];
        $startDate = $_POST['startDate'];
        $endDate = $_POST['endDate'];
        if(!$sem->isValidDate($startDate) || !$sem->isValidDate($endDate)) {
            $message = "Error: Invalid Start or End Date!";
            echo "<div class='alert center-block alert-danger fade in'> ". $message . " <button type='button' class='close' data-dismiss='alert'>  &times;</button></div>";
        } else {
            $sem->updateSemester($semesterID, $startDate, $endDate);
            $message = "Success! Semester has been updated.";
            echo "<div class='alert center-block alert-success fade in'> ". $message . " <button type='button' class='close' data-dismiss='alert'>  &times;</button></div>";
        ?>
            <script>
                redirectToLastPage(1000);
            </script>
        <?php
        }
    }
    
    $currentSemesterID = $sem->getCurrentSemester();
    $semesters = $sem->getSemesters();
?>

<legend>FYS Semesters</legend>

<div class="table-responsive">
    <table id="dataTable" class="table table-hover table-data">
        <thead>
            <tr>
              <th></th>
              <th>Semester</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Actions</th>
            </tr>
        </thead>
        <tbody>
<?php
        foreach($semesters as $semester) {
?>
            <form action="" method="post">
                <input type="hidden" name="semesterid" value="<?=$semester->semesterid?>"/>
                <tr id="body-message">
                    <td><?=($currentSemesterID == $semester->semesterid) ? "<span class='label label-success'>Active</span>" : ""?></td>
                    <td><?=$semester->label?></td>
                    <td><input type="text" class="form-control" name="startDate" value="<?=$semester->startDateTime?>"/></td>
                    <td><input type="text" class="form-control" name="endDate" value="<?=$semester->endDateTime?>"/></td>
                    <td><input type="submit" name="SubmitForm" class="btn btn-xs btn-primary" value="Update"/></td>
                </tr>
            </form>
<?php
        }
?>
        </tbody>
    </table>
</div>
