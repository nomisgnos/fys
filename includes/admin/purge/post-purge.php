<?php
    
if ( !isset($_POST['SubmitFilter'])) {
    $msg = "Error! Could not purge!";
} else {
    $totals = $del->handlePurgeData($_POST);
    $totStr = http_build_query($totals,'',', ');
    
    $msg = "<strong>Success!</strong> Data Purged! <br>Total rows deleted:<br>" . $totStr;
}
?>

<div id="post-id-delete">
<?=$msg?>
</div>