<?php
    include 'includes/import-timeline.php';
    
    $tml = new ImportTimeline($adm);
    $uniqueDates = $tml->getUniqueImportDates();
    $allData = $tml->getAllData();
?>
<style>
.dataTables_filter, .dataTables_length, #dataTable {
    display:none;
}
</style>
<legend>Import Timeline</legend>
<div style="float:left;margin-right:15px;">
<h5>Date</h5>
<select id="dates" class="selectpicker" multiple data-max-options="1" show-tick title="Nothing selected">
    <?php 
    foreach($uniqueDates as $date) {
    ?>
        <option value="<?=$date->importedOn;?>"><?=$date->importedOn?></option>
    <?php
    }
    ?>
</select>
</div>

<div style="float:left;margin-right:15px;">
<h5>User Role</h5>
<select id="roles" class="selectpicker" show-tick>
    <option value="0">Students and Instructors</option>
    <option value="5">Student</option>
    <option value="2">Instructor</option>
</select>
</div>
<div style="float:right;margin-right:15px;">
<h5>Search</h5>
<input id="tableSearch" type="search" class="form-control input-xs"></input>
</div>
<div style="clear:both;"></div>
<h2 id="currentRole">Students and Instructors</h2>
<div class="table-responsive">
    <table id="dataTable" class="table table-hover table-data" style="display:none;">
        <thead>
            <tr>
              <th>Imported On</th>
              <th>UID</th>
              <th>PSU ID</th>
              <th>Name</th>
              <th>Role</th>
              <th>Schedule #</th>
              <th>Course</th>
            </tr>
        </thead>
        <tbody>
<?php
        foreach($allData as $data) {
?>
            <tr id="body-message">
                <td id="body-imported"><?=$data->importedOn?></td>
                <td id="body-uid"><?=$data->UserID?></td>
                <td id="body-id"><a href="mailto:<?=$data->Email;?>"><?=$data->AccessID;?></a></td>
                <td id="body-name"><?=$data->FName;?> <?=$data->LName;?></td>
                <td id="body-role"><?=($data->User_Role_ID == 2) ? "<span class='label label-primary'>Instructor</span>" : "<span class='label label-success'>Student</span>"?></td>
                <td id="body-course"><?="<a href='" . $_settings['current_URL_path'] . "/admin/courses/search/" . $data->ScheduleNumber . "'>" . $data->ScheduleNumber . "</a>"?>
                <td id="body-students"><?="<a href='" . $_settings['current_URL_path'] . "/admin/courses/" . $data->CourseID . "' class='btn btn-xs btn-default'>Students</a>"?></td>
            </tr>
<?php
        }
?>
        </tbody>
    </table>
</div>

<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $("#dates").selectpicker("render");
    var table = $('.table-data').DataTable({
        deferRender: true,
        orderClasses: false,
        responsive: true,
        autoWidth: true,
        paging:   true,
        searching: true,
        order: [[ 0, "desc" ]]
    });
    $('.table-data').show();
    
    $("#dates").on("change", function() {
        var searchDate = $('#dates :selected').text();
        table.columns(0).search(searchDate, false, false).draw();
    });
    
    $("#roles").on("change", function() {
        if($('#roles').val() == 0) {
            table.columns(4).search('').draw();
            $('#currentRole').text($('#roles :selected').text());
            return;
        }
        
        var role = $('#roles :selected').text();
        $('#currentRole').text(role);
        table.columns(4).search(role, false, false).draw();
    });
    
    $('#tableSearch').on( 'keyup', function () {
        table.search( this.value ).draw();
    } );
</script>