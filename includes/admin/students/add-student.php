<?php
    $courses = $adm->getCourses();
?>
<style>
textarea { resize:vertical; }
</style>
</div>
<div id="alert-area"></div>
<div class="col-md-8">
<form id="addStudent" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend align="left">Add Student</legend>


<div class="form-group required">
  <label class="col-md-3 control-label" for="accessid">Access ID:</label>  
  <div class="col-md-8">
    <input id="accessid" name="accessid" type="text" placeholder="abc123" class="form-control input-md" required="" autofocus>
  </div>
</div>
<!-- Text input-->
<div class="form-group required">
  <label class="col-md-3 control-label" for="firstname">First Name</label>  
  <div class="col-md-8">
    <input id="firstname" name="firstname" type="text" class="form-control input-md" required="">
  </div>
</div>

<!-- Text input-->
<div class="form-group required">
  <label class="col-md-3 control-label" for="lastname">Last Name</label>  
  <div class="col-md-8">
  <input id="lastname" name="lastname" type="text" class="form-control input-md" required="">
  </div>
</div>

<!-- Text input-->
<div class="form-group required">
  <label class="col-md-3 control-label" for="email">Email</label>  
  <div class="col-md-8">
  <input id="email" name="email" type="text" placeholder="abc123@psu.edu" class="form-control input-md" required="">
  </div>
</div>

<!-- Select Multiple -->
<div class="form-group required">
  <label class="col-md-3 control-label" for="course">Courses</label>
  <div class="col-md-9">
    <select id="course" name="course[]" class="form-control"  required="" style="width:100%;height:400px;font-size:14px;" multiple>
    <?php foreach($courses as $course) { ?>
            <option value="<?=$course->CourseID?>"><?=$course->Name?> <?=$course->Number?> Sec. <?=$course->Section?> -- <?=$course->Instructor?></option>
    <?php } ?>
    </select>
    <span style="color:#B76B38;font-style: italic;font-size:smaller;">(Hold down the Ctrl (windows) / Command (Mac) button to select multiple options.)</span>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-3 control-label" for="notes">Notes</label>
  <div class="col-md-9">
    <textarea class="form-control" id="notes" name="notes"></textarea>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-3 control-label" for="confidential">Confidential Information</label>
  <div class="col-md-9">                     
    <textarea class="form-control" id="confidential" name="confidential"></textarea>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-3 control-label" for="submitbtn"></label>
  <div class="col-md-8">
    <button id="submitbtn" name="submitbtn" class="btn btn-primary btn-raised">Submit</button>
    <button id="cancelbtn" name="cancelbtn" class="btn btn-danger btn-raised" type="reset">Reset</button>
  </div>
</div>

</fieldset>
</form>
</div>

<script>
$(document).ready(function() {
    ajaxHandleForm('#addStudent', 'post-student-add', 'div#post-id-add');
});
</script>