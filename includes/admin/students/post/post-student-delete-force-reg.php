<div id="post-id-delete">
<?php
    $uid = $_POST['uid'];
    $eventid = $_POST['id'];

    if(empty($uid) || empty($eventid)) {
        $msg = "<strong>Error:</strong> UserID or EventID not set!";
    } else {
        // check student event registration
        if(!$adm->checkStudentEventRegistration($uid, $eventid)) {
            $msg = "<strong>Error:</strong> User is not registered for that event!";
        } else {
        
            $results = $adm->deleteStudentEventRegistration($uid, $eventid);
            
            // todo: db error handling
            
            $msg = "<strong>Success!</strong> Deleted registered event for user!";
        }

    }
    
?>


<?=$msg?>
</div>