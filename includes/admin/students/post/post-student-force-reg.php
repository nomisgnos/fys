<div id="post-id-force">
<?php
    $uid = $_POST['userid'];
    $eventid = $_POST['eventid'];

    if(empty($uid) || empty($eventid)) {
        $msg = "<strong>Error:</strong> UserID or EventID not set!";
    } else {
        // check student event registration
        if($adm->checkStudentEventRegistration($uid, $eventid)) {
            $msg = "<strong>Error:</strong> User is already registered for that event!";
        } else {
            $results = $adm->forceStudentRegistration($uid, $eventid);
            
            // todo: db error handling
            
            $msg = "<strong>Success!</strong> Registered event for user";
        }

    }

?>


<?=$msg?>
</div>