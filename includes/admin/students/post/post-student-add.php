<div id="post-id-add">
<?php
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) { 
        $msg = "<strong>Error:</strong> Invalid email format. Please verify email is correct."; 
    } else {
        // do db stuff
        $results = $adm->addStudent($_POST);
        
        $msg = "<strong>Success!</strong> Added User: " . $_POST['firstname'] . " " . $_POST['lastname'] ;
    }

?>


<?=$msg?>
</div>