<?php
    function in_student_courses($courseID, $studentCourses) {
        foreach($studentCourses as $studentCourse) {
            if ($courseID == $studentCourse->CourseID)
                return true;
        }
        return false;
    }
    
    $urlDecode = $adm->base64_url_decode($_GET['ID']);
    // Format: UserID, AccessID, FName, LName, Email, Notes, ConfidentialInfo
    $student = explode(";", $urlDecode);
    $userid = $student[0];
    
    $courses = $adm->getCourses();
    $studentCourses = $adm->getStudentCourses($userid);
    $events = $adm->getStudentEvents($userid);
?>
</div>
<style>
    .small-btn-delete {
        padding:1px; font-size:13px;
    }
</style>
<div id="alert-area"></div>
<div class="col-md-8">
<form id="editStudent" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend align="left">Edit Student</legend>


<div class="form-group">
  <label class="col-md-3 control-label" for="accessid">Access ID:</label>  
  <label class="col-md-2 control-label" for="accessid"><?=$student[1]?></label>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="firstname">First Name</label>  
  <div class="col-md-8">
    <input id="firstname" name="firstname" type="text" value="<?=$student[2]?>" class="form-control input-md" autofocus>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="lastname">Last Name</label>  
  <div class="col-md-8">
  <input id="lastname" name="lastname" type="text" value="<?=$student[3]?>" class="form-control input-md">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="email">Email</label>  
  <div class="col-md-8">
  <input id="email" name="email" type="text" value="<?=$student[4]?>" class="form-control input-md" required="">
  </div>
</div>

<!-- Select Multiple -->
<div class="form-group">
  <label class="col-md-3 control-label" for="course">Course</label>
  <div class="col-md-9">
    <select id="course" name="course[]" class="form-control" multiple style="width:100%;height:400px;font-size:14px;">
    <?php
        foreach($courses as $course) {
            $selected = (in_student_courses($course->CourseID, $studentCourses)) ? 'selected' : '';
    ?>
            <option <?=$selected?> value="<?=$course->CourseID?>" ><?=$course->Name?> <?=$course->Number?> Sec. <?=$course->Section?> -- <?=$course->Instructor?></option>
    <?php
        }
    ?>
    </select>
    <span style="color:#B76B38;font-style: italic;font-size:smaller;">(Hold down the Ctrl (windows) / Command (Mac) button to select multiple options.)</span>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-3 control-label" for="notes">Notes</label>
  <div class="col-md-9">
    <textarea class="form-control" id="notes" name="notes"><?=$student[5]?></textarea>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-3 control-label" for="confidential">Confidential Information</label>
  <div class="col-md-9">                     
    <textarea class="form-control" id="confidential" name="confidential"><?=$student[6]?></textarea>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-3 control-label" for="submitbtn"></label>
  <div class="col-md-8">
    <button id="submitbtn" name="submitbtn" class="btn btn-primary">Submit</button>
    <button id="cancelbtn" name="cancelbtn" class="btn btn-danger" type="reset">Reset</button>
  </div>
</div>

</fieldset>
</form>
</div>
<div class="col-md-4">
    <legend align="left">Events Registered
        <div style="margin-top:15px;" class="col-md-8"> 
            <a href="<?=$_settings['current_URL_path']; ?>/admin/students/force-reg?ID=<?=$_GET['ID'];?>" style="padding:5px 8px;" class="btn btn-success pull-right">Force Event Registration</a>
        </div>
    </legend> 

    <br>
    <?php 
    foreach($events as $event) {
        $attended = ($event->isAttended == '') ? 'R' : $event->isAttended;
    ?>
        <div style="margin-top:15px;" class="panel panel-primary" id="body-message">
            <a href="#" class="btn btn-danger pull-right small-btn-delete student-delete-reg"><span class="glyphicon glyphicon-trash"></span> Delete</a>

            <div class="panel-heading event-name"><b><?=$event->Name?></b></div>
                <span style="display:none;" class="student-delete-reg-eventid"><?=$event->EventID?></span>
                <span style="display:none;" class="student-delete-reg-userid"><?=$userid?></span>
                <ul class="list-group">
                    <li class="list-group-item">Type: <?=$event->Type?></li>
                    <li class="list-group-item"><?=$event->Date?>, <?=$event->StartTime?> - <?=$event->EndTime?></li>
                    <li class="list-group-item">Attendance: <?=$attended?></li>
                </ul>
                <div class="clearfix"></div>
        </div>
    <?php
    }
    ?>
</div>

<script>
$(document).ready(function() {
    ajaxHandleForm('#editStudent', 'post-student-edit', 'div#post-id-edit', <?=$userid?>);
    ajaxDeleteStudentRegistration();
});
</script>