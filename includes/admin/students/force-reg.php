<?php   function newEventGroup($group) { ?>
            </optgroup>
            <optgroup label="<?=$group?>">
<?php   }
    
    
    $urlDecode = $adm->base64_url_decode($_GET['ID']);
    // Format: UserID, AccessID, FName, LName, Email, Notes, ConfidentialInfo
    $student = explode(";", $urlDecode);
    
    $events = $adm->getEvents();
    //$studentEvents = $adm->getStudentEvents($student[0]);
?>
<style>
@media (min-width: 768px) {
    .form-horizontal .control-label {
        text-align: right;
        margin-bottom: 0;
    }
}
</style>
</div>
<div id="alert-area"></div>
<form id="student-force-reg"class="form-horizontal">
<fieldset>
<!-- Form Name -->
<legend align="left">Force Event Registration for <?=$student[2]?> <?=$student[3]?></legend>

<div class="form-group">
  <label class="col-md-1 control-label" style="margin-bottom:5px;" for="eventid">Event:</label>  
  <select id="eventid" name="eventid" class="selectpicker show-tick" title="Choose an event..." data-live-search="true" data-live-search-placeholder="Search Event" data-width="800px">
    <?php
        $lastType = "";
        foreach($events as $event) {
            if ($lastType != $event->Type) {
                $lastType = $event->Type;
                newEventGroup($event->Type);
            }
            
            $seatsLeft = ( ($event->Seats - $event->Registered) > 0) ? $event->Seats - $event->Registered : 0;
    ?>
            <option value="<?=$event->EventID?>">[<?=$event->EventID?>] <?=$event->Name?>, <?=$event->Date?> <?=$event->StartTime?> - <?=$event->EndTime?> (Seats Left: <?=$seatsLeft?>)</option>
    <?php
        }
    ?>
    </select> 
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-1 control-label" for="submitbtn"></label>
  <div class="col-md-8">
    <button id="submitbtn" name="submitbtn" class="btn btn-other btn-primary">Submit</button>
  </div>
</div>

</fieldset>
</form>

<script>
    $("#eventid").selectpicker("render");
    ajaxHandleForm('#student-force-reg', 'post-student-force-reg', 'div#post-id-force', <?=$student[0]?>);
</script>