<?php
// Permissions check- todo: update user access ids
if (!$adm->canUserAccess(array(1, 2, 3, 4, 6), $roleID)) {
    return;
}
    
if (!isset($_POST['uid']) || !isset($_POST['id']) || empty($_POST['uid']) || empty($_POST['id'])) {
    $msg = "Error! Could not delete user!";
} else {
    $adm->deleteStudent($_POST['uid']);
    
    // TODO: DB error stuff
    $msg = "<strong>Success!</strong> Student Access ID: <strong>" . $_POST['id']  . "</strong> deleted!";
}
?>

<div id="post-id-delete">
<?=$msg?>
</div>
