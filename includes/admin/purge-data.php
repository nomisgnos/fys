<?php
    // Permissions check
    if (!$adm->canUserAccess(array(1), $roleID)) {
        return;
    }
    
    include 'includes/purge-data.php';
    $del = new PurgeDataMethods($adm);
    
    $action = true;
    
    /* Event Circuits */
    switch($circuit) {
        case checkCircuit($circuit, 'delete'):
            $breadText = 'Purge data confirm';
            include 'purge/post-purge.php';
            break;
        default:
            $action = false;
    }
?>
<style>

</style>

<div id="alert-area"></div>
<h1>Purge Data</h3>
<h3>Filter data to purge</h3>
<?php
$arr = ["Events", "Registrations", "Courses", "Instructors", "Students", "Presenters"];
?>
<div class="panel panel-primary">
    <div class="panel-body">
        <form id="submitPurgeData" action="" method="post">
            <div style="padding:20px;" class="bg-warning">
                <strong>Note:</strong> <br />
                (As of 01/10/2020) Before the registration begins, we recommend purging the "cobwebs":
                <ul>
                    <li><strong>For Spring</strong>: Courses, Students, and Presenters.</li>
                    <li><strong>For Fall</strong>: All of it.</li>
                </ul>
                We recommend taking a screenshot of the dashboard page (the stats) and downloading any reports.<br />
                <span style="color:red;">!!  DO NOT PURGE ANY DATA IF REGISTRATION HAS STARTED  !!</span>
            </div>
            <h4>Select Data:</h4>
            <div class="row">
                <?php
                foreach($arr as $ar) {
                    $selected = !empty($_POST['purge' . $ar]) ? 'checked' : '';
                ?>
                <div class="col-md-2">
                    <h5><?=$ar?></h5>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="purge<?=$ar?>" value="1" <?=$selected?>></input>
                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                        </label>
                    </div>
                </div>
                <?php
                }
                ?>
            </div>
            
            <div class="row">
                <div class="col-md-3">
                    <h4>Semester</h4>
                    <select name="purgeSemester" id="fromDate" class="selectpicker" title="Choose a semester" required>
                        <option value="0" <?= !empty($_POST['purgeSemester']) && $_POST['purgeSemester'] == 0 ? 'selected' : ''?>>Fall and Spring</option>
                        <option value="1" <?= !empty($_POST['purgeSemester']) && $_POST['purgeSemester'] == 1 ? 'selected' : ''?>>Fall</option>
                        <option value="2" <?= !empty($_POST['purgeSemester']) && $_POST['purgeSemester'] == 2 ? 'selected' : ''?>>Spring</option>
                    </select>
                </div>
                <div class="col-md-9">
                    <p style="padding:20px;" class="bg-danger">
                        <span>
                        <strong>Note:</strong> Semester is only defined for events and event registrations. For courses, instructors, and students, changing semester will not affect the filtered data.
                        USE CAREFULLY!
                        </span>
                    </p>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" name="SubmitFilter" class="btn btn-primary">Filter</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php

    
    if(!empty($_POST) && isset($_POST['SubmitFilter'])) {
        
        $jsonPost = json_encode($_POST);
        
        // Purge data preview
        $del->handlePurgePreview($_POST);
?>

        <button id="purge-data" class="btn btn-danger">PURGE</button>
        <script>
            ajaxPurgeData();
            function ajaxPurgeData() {
                $(document).on("click", "#purge-data", function(e) {
                    var data = <?=$jsonPost?>;
                    
                    var title = "Purge Data?";
                    var message = "Database will be backed up before purging data but please verify the data purge in case of failure.";
                    ajaxDeleteModal(null, 'purge/delete', data, title, message);
                    e.preventDefault();
                });
            }
        </script>
<?php
    }
?>



