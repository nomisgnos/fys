<?php
if ($adm->canUserAccess(array(1,4), $roleID) && isset($_GET['EventID'])) {
    $eventInfo = $adm->getEventInfo($_GET['EventID']);
    $students = $adm->getEventStudents($_GET['EventID']);
    if(isset($_GET['results']) && $_GET['results'] == "added") {
        echo "<div class='alert alert-success alert-dismissable'>
                <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Registered new students to event, please update attendance below.</div>";
    } 
    elseif (false) {
        echo "<div class='alert alert-success alert-dismissable'>
                <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Saved Attendance for xyz!</div>";
    }
?>
                <h1>Develop Attendance Report</h1>
            </div>
        
            <div class="body-content" id="body-content-padding">

                <ul class="element-list">
                    <li><strong>Name:</strong> <?=$eventInfo[0]->Name;?></li>
                    <li><strong>Type:</strong> <?=$eventInfo[0]->Type;?></li>
                    <li><strong>Date:</strong> <?=date_format(date_create($eventInfo[0]->Date), 'm-d-Y');?></li>
                    <li><strong>Time:</strong> <?=$eventInfo[0]->StartTime . ' - ' . $eventInfo[0]->EndTime;?></li>
                    <li><strong>Location:</strong> <?=$eventInfo[0]->Location;?></li>
                </ul>
                
                <form id="ForceRegisterStudents" action="" method="post">
                    <div class="col-md-8">
                        <select id="ForceStudent" name="ForceStudent[]" title="Register Students to Current Event (click here)" data-width="100%" data-live-search="true" data-live-search-placeholder="Search Student via NAME or PSUID" class="selectpicker form-control show-tick" multiple>
                            <?= $adm->getStudents(true); ?>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <input type="hidden" name="EventID" value="<?=$_GET['EventID']?>" />
                        <input type="hidden" name="Redirect" value="redirectDevelopAttendance" />
                        <input class="btn btn-primary" type="submit" name="Submit" value="Register" />&nbsp;&nbsp;&nbsp;<input class="btn btn-danger" type="reset" name="Reset" value="Reset" />  
                    </div>
                </form>
                
                <p style="margin-bottom:15px;clear:both"></p>
                
                <?php
                if ($students == 0) {
                    echo '<p>No students registered for this event!</p>';
                } else {
                ?>
                    <p>Please update the attendance report below by indicating whether or not a student attended an event.</p>
                    <form id="Attendance" action="" method="post">
                        <table class="table-striped" >
                        <thead>
                            <tr>
                                <th style="text-align:center;">Student Name</th>
                                <th>Yes</th>
                                <th>No</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($students as $student) { ?>
                            <tr class="studentRow <?php if(!isset($student->IsAttended)) { echo "bg-warning-imp"; }?>">
                                <td><label style="text-align:center;" name="label_<?= $student->RegistrationID; ?>"><?= $student->FName . ' ' . $student->LName; ?></label></td>
                                <td><input name="<?= $student->RegistrationID; ?>" type="radio" class="form_rad" value="Y" <?php if (isset($student->IsAttended) && $student->IsAttended == 'Y') echo 'checked'; ?> /></td>
                                <td><input name="<?= $student->RegistrationID; ?>" type="radio" class="form_rad" value="N" <?php if ((isset($student->IsAttended) && $student->IsAttended == 'N') || !isset($student->IsAttended)) echo 'checked'; ?> /></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        </table><br />

                        <input type="hidden" name="EventID" value="<?=$_GET['EventID']?>" />
                        <input type="hidden" name="Redirect" value="redirectDevelopAttendance" />
                        <input class="btn btn-primary" type="submit" name="Submit" value="Update Attendance" />&nbsp;&nbsp;&nbsp;<input class="btn btn-danger" type="reset" name="Reset" value="Reset" />  
                    </form>
                <?php }
} ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("input:reset").click(function() {
            this.form.reset();
            $("#ForceStudent").val('').selectpicker('refresh');
            return false;
        });
    });
</script>