<?php
    if(!$isAdmin && !$isPresenter) {
        return;
    }
    
    $eventInfo = $adm->getEventInfo($_GET['EventID']);
    $presenterOptions = $adm->getPresenters();
?>
<style>
textarea { resize:vertical; }
thead th {
    background: #fff;
    color: #000;
}
</style>
</div>
<div id="alert-area"></div>
<div class="col-md-12">
<form id="editEvent" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend align="left">Edit Event</legend>

<div class="form-group required">
  <label class="col-md-3 control-label" for="Name">Name</label>  
  <div class="col-md-8">
    <input id="Name" name="Name" type="text" class="form-control input-md" value="<?=$eventInfo[0]->Name;?>" required="" autofocus>
  </div>
</div>
<!-- Dropdown Select -->
<div class="form-group required">
    <label class="col-md-3 control-label" for="Type">Type</label>
    <div class="col-md-4">
        <select id="Type" name="Type" class="form-control">
            <option value="Academic" <?php if ($eventInfo[0]->Type == 'Academic') echo 'selected';?>>Academic Event</option>
            <option value="Advising" <?php if ($eventInfo[0]->Type == 'Advising') echo 'selected';?>>Advising Event</option>
            <option value="SCW" <?php if ($eventInfo[0]->Type == 'SCW') echo 'selected';?>>Social, Cultural, or Wellness Event</option>
            <option value="Title IX" <?php if ($eventInfo[0]->Type == 'Title IX') echo 'selected';?>>Title IX</option>
            <option value="Writing Ethics" <?php if ($eventInfo[0]->Type == 'Writing Ethics') echo 'selected';?>>Writing Ethics</option>
            <option value="Unclassified" <?php if ($eventInfo[0]->Type == '') echo 'selected';?>>Unclassified</option>
        </select>
    </div>
</div>
<!-- Text input-->
<div class="form-group required" >
  <label class="col-md-3 control-label" for="Date">Date</label>  
  <div class="col-md-4">
    <input data-date-format="yyyy-mm-dd" id="Date" name="Date" type="text" value="<?=$eventInfo[0]->Date?>" class="form-control input-md datepicker" required="">
  </div>
</div>

<!-- Text input-->
<div class="form-group required">
  <label class="col-md-3 control-label" for="StartTime">Start Time</label>  
  <div class="col-md-2">
  <input id="StartTime" name="StartTime" type="text" class="form-control input-md" value="<?= date_format(date_create($eventInfo[0]->StartTime), 'h:i A');?>" required="">
  </div>
    <label class="col-md-2 control-label" for="EndTime">End Time</label>  
  <div class="col-md-2">
  <input id="EndTime" name="EndTime" type="text" class="form-control input-md" value="<?= date_format(date_create($eventInfo[0]->EndTime), 'h:i A');?>" required="">
  </div>
</div>


<!-- Text input-->
<div class="form-group required">
  <label class="col-md-3 control-label" for="Location">Location</label>  
  <div class="col-md-4">
  <input id="Location" name="Location" type="text" value="<?= $eventInfo[0]->Location;?>" class="form-control input-md" required="">

  </div>
  <small id="roomBuilding" class="text-muted">Room, Building</small>
</div>

<!-- Text input-->
<div class="form-group required">
  <label class="col-md-3 control-label" for="Seats">Seats/Capacity</label>  
  <div class="col-md-2">
    <input id="Seats" name="Seats" type="text" value="<?= $eventInfo[0]->Seats;?>" class="form-control input-md" required="">
  </div>
  
    <label class="col-md-2 control-label optional-field" for="Registered">Registered</label>  
    <div class="col-md-1">
        <input id="Registered" name="Registered" type="text"  class="form-control input-md"value="<?= $eventInfo[0]->Registered;?>" disabled> 
    </div>
</div>

<!-- Textarea -->
<div class="form-group required">
  <label class="col-md-3 control-label" for="Description">Description</label>
  <div class="col-md-8">                     
    <textarea class="form-control" id="Description" rows="5" name="Description"><?= $eventInfo[0]->Description;?></textarea>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="Link">Link</label>  
  <div class="col-md-8">
  <input id="Link" name="Link" type="text" value="<?= $eventInfo[0]->Link;?>" class="form-control input-md">

  </div>
  <small id="eventWebsite" class="text-muted">Event Website</small>
</div>
<!-- Dropdown Select -->
<div class="form-group required">
    <label class="col-md-3 control-label" for="Presenter">Presenter</label>
    <div class="col-md-6">
        <select id="Presenter" name="Presenter" title="Select Presenter" data-live-search="true" data-live-search-placeholder="Search Presenter" class="selectpicker form-control">
            <?= $adm->setSelectedPresenter($presenterOptions, $eventInfo[0]->Presenter); ?>
        </select>
    </div>
</div>
<!-- Dropdown Select -->
<div class="form-group">
    <label class="col-md-3 control-label" for="CoPres1">Co-Presenter(s)</label>
    <div class="col-md-4">
        <select id="CoPres1" name="CoPres1" title="Select Co-Presenter 1" data-live-search="true" data-live-search-placeholder="Search Presenter" class="selectpicker form-control">
            <?= $adm->setSelectedPresenter($presenterOptions, $eventInfo[0]->CoPres1); ?>
        </select>
    </div>
    <div class="col-md-4">
        <select id="CoPres2" name="CoPres2" title="Select Co-Presenter 2" data-live-search="true" data-live-search-placeholder="Search Presenter" class="selectpicker form-control">
            <?= $adm->setSelectedPresenter($presenterOptions, $eventInfo[0]->CoPres2); ?>
        </select>
    </div>
</div>
<!-- Dropdown Select -->
<div class="form-group required">
    <label class="col-md-3 control-label" for="Semester">Semester</label>
    <div class="col-md-3">
        <select id="Semester" name="Semester" class="form-control">
            <option value="" selected disabled>Select Semester</option>
            <option value="1" <?php if($eventInfo[0]->semesterid == 1) echo 'selected';?>>Fall</option>
            <option value="2" <?php if($eventInfo[0]->semesterid == 2) echo 'selected';?>>Spring</option>
        </select>
    </div>
</div>

<?php
if($isAdmin) {
?>
<!-- Dropdown Select -->
<div class="form-group required">
    <label class="col-md-3 control-label" for="Active">Approved</label>
    <div class="col-md-3">
        <select id="Active" name="Active" class="form-control">
            <option value="0" <?php if($eventInfo[0]->active == 0) echo 'selected';?>>No</option>
            <option value="1" <?php if($eventInfo[0]->active == 1) echo 'selected';?>>Yes</option>
        </select>
    </div>
</div>
<?php
}
?>
<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-3 control-label" for="submitbtn"></label>
  <div class="col-md-8">
    <button id="submitbtn" name="submitbtn" class="btn btn-primary btn-raised">Submit</button>
    <button id="cancelbtn" name="cancelbtn" class="btn btn-danger btn-raised" type="reset">Reset</button>
  </div>
</div>

</fieldset>
</form>
</div>

<script>
$(document).ready(function() {
    ajaxHandleForm('#editEvent', 'post-edit-event', 'div#post-id-edit', <?=$_GET['EventID']?>);
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
    $( "#StartTime" ).clockpicker({autoclose: true, twelvehour: true});
    $( "#EndTime" ).clockpicker({autoclose: true, twelvehour: true});
});
</script>