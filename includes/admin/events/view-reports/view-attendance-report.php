<?php
if ($adm->canUserAccess(array(1, 4), $roleID) && isset($_GET['EventID'])) {
    $eventInfo = $adm->getEventInfo($_GET['EventID']);
    $students = $adm->getEventStudents($_GET['EventID']);
    $regStats = $adm->getEventRegStatsByID($_GET['EventID']);
?>

                <h1>View Attendance Report</h1>
            </div>
        
            <div class="body-content" id="body-content-padding">
                <div class="row">
                    <div class="col-md-7">
                        <p style="font-size:20px;" class="lead">Event Info</p> 
                        <p><strong>Name:</strong> <?=$eventInfo[0]->Name;?></p>
                        <p><strong>Type:</strong> <?=$eventInfo[0]->Type;?></p>
                        <p><strong>Date:</strong> <?=date_format(date_create($eventInfo[0]->Date), 'm-d-Y');?></p>
                        <p><strong>Time:</strong> <?=$eventInfo[0]->StartTime . ' - ' . $eventInfo[0]->EndTime;?></p>
                        <p><strong>Location:</strong> <?=$eventInfo[0]->Location;?></p>
                    </div>
                    <div class="col-md-5">
                        <p style="font-size:20px;" class="lead">Attendance</p> 
                        
                        <?php
                        $percentAttended = round($regStats[0]->sumAttended/$eventInfo[0]->Registered*100);
                        $percentMissed = round($regStats[0]->sumMissed/$eventInfo[0]->Registered*100);
                        $percentHold = round($regStats[0]->sumHold/$eventInfo[0]->Registered*100);
                        ?>
                        <div class="progress">
                            <div data-toggle="tooltip" title="<?=$regStats[0]->sumAttended?> attended" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?=$regStats[0]->sumAtended?>" aria-valuemin="0" aria-valuemax="<?=$eventInfo[0]->Registered?>" style="width: <?=$percentAttended?>%;">
                                <span><?=$percentAttended?>%</span>
                            </div>
                            <div data-toggle="tooltip" title="<?=$regStats[0]->sumMissed?> missed" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?=$regStats[0]->sumMissed?>" aria-valuemin="0" aria-valuemax="<?=$eventInfo[0]->Registered?>" style="width: <?=$percentMissed?>%;">
                                <span><?=$percentMissed?>%</span>
                            </div>
                            <div data-toggle="tooltip" title="<?=$regStats[0]->sumHold?> unrecorded" class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?=$regStats[0]->sumHold?>" aria-valuemin="0" aria-valuemax="<?=$eventInfo[0]->Registered?>" style="width: <?=$percentHold?>%;">
                                <span><?=$percentHold?>%</span>
                            </div>
                        </div>
                        
                        <p><strong>Attended:</strong> <?=$regStats[0]->sumAttended?></p>
                        <p><strong>Missed:</strong> <?=$regStats[0]->sumMissed?></p>
                        <p><strong>Total:</strong> <?=$eventInfo[0]->Registered?></p>
                    </div>
                </div>
                
                <?php
                if ($students == 0) {
                    echo '<p>No students registered for this event!</p>';
                } else { ?>
                    <hr class="style2">
                    <div class="table-responsive">
                        <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Attendance Status</th>
                            </tr>
                        </thead>
                    <?php
                    foreach($students as $student) {    
                    ?>
                        <tr>
                            <td><?=$student->FName . ' ' . $student->LName?></td>
                            <td><a href="mailto:<?=$student->Email?>"><?=$student->Email?></a></td>
                            <td>
                    <?php
                                if (empty($student->IsAttended)) {
                                    echo 'No Record';
                                } elseif ($student->IsAttended == 'Y') {
                                    echo '<span style="font-size:16px;color:green;padding-right:3px;" class="glyphicon glyphicon-ok-circle"></span> Attended';
                                } elseif ($student->IsAttended == 'N') {
                                    echo '<span style="font-size:16px;color:red;padding-right:3px;" class="glyphicon glyphicon-remove-circle"></span> Missed';
                                }
                    ?>      </td>
                        </tr>
                    <?php
                    }
                    ?>
                        </table>
                    </div>
                <?php
                }

                echo '<p class="no-print"><a class="btn btn-info" href="' . $_settings['current_URL_path'] . '/admin/events#' . $_GET['EventID'] .'">Return to Events Listing</a></p>';
} ?>
            </div>
        </div>
    </div>
</div>