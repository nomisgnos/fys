<?php
if ($adm->canUserAccess(array(1, 4), $roleID) && isset($_GET['EventID'])) {
    $eventInfo = $adm->getEventInfo($_GET['EventID']);
    $comments = $adm->getPreEventComments($_GET['EventID']);
?>

                <h1>View Pre-Event Comments</h1>
            </div>
        
            <div class="body-content" id="body-content-padding">

                <ul class="element-list">
                    <li><strong>Name:</strong> <?=$eventInfo[0]->Name;?></li>
                    <li><strong>Type:</strong> <?=$eventInfo[0]->Type;?></li>
                    <li><strong>Date:</strong> <?=date_format(date_create($eventInfo[0]->Date), 'm-d-Y');?></li>
                    <li><strong>Time:</strong> <?=$eventInfo[0]->StartTime . ' - ' . $eventInfo[0]->EndTime;?></li>
                    <li><strong>Location:</strong> <?=$eventInfo[0]->Location;?></li>
                </ul>

                <?php
                if ($comments) {
                    echo '<hr class="style2" /><ul>';
                    foreach($comments as $comment) {
                        echo '  <li style="padding-bottom:5px;">' . $comment->PreEventComments . '</li>';
                    }
                    echo '</ul>';
                } else {
                    echo '<p>No students have left pre-event comments for this event!</p>';
                }

                echo '<p class="no-print"><a class="btn btn-info" href="' . $_settings['current_URL_path'] . '/admin/events#' . $_GET['EventID'] .'">Return to Events Listing</a></p>';
} ?>
            </div>
        </div>
    </div>
</div>