<?php
if ($adm->canUserAccess(array(1, 4), $roleID) && isset($_GET['EventID'])) {
    $eventInfo = $adm->getEventInfo($_GET['EventID']);
    $feedback = $adm->getEventFeedBack($_GET['EventID']);
?>

                <h1>View Event Feedback Report</h1>
            </div>
        
            <div class="body-content" id="body-content-padding">

                <ul class="element-list">
                    <li><strong>Name:</strong> <?=$eventInfo[0]->Name;?></li>
                    <li><strong>Type:</strong> <?=$eventInfo[0]->Type;?></li>
                    <li><strong>Date:</strong> <?=date_format(date_create($eventInfo[0]->Date), 'm-d-Y');?></li>
                    <li><strong>Time:</strong> <?=$eventInfo[0]->StartTime . ' - ' . $eventInfo[0]->EndTime;?></li>
                    <li><strong>Location:</strong> <?=$eventInfo[0]->Location;?></li>
                </ul>

                <?php
                if (!$feedback) {
                    echo '<p>No student feedback available for this event!</p>';
                } else { ?>
                    <hr class="style2"/><br />
                    <div class="table-responsive">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <?php 
                                    if ($isAdmin) {
                                        echo '<th>UID</th><th>Name</th><th>Access ID</th>';
                                    } ?>
                                    <th class="center-text">Pre-Event Comments</th>
                                    <th>Rating</th>
                                    <th class="center-text">Survey Paragraph</th>
                                </tr>
                            </thead>
                        <?php
                        foreach($feedback as $item) {
                            // Don't show empty rows for non admins
                            if(!$isAdmin && $item->SurveyParagraph == '' && $item->PreEventComments == '')
                                continue;
                            
                            echo '<tr>';
                            if ($isAdmin) {
                                echo "<td style=\"text-align: center;\">" . $item->UserID . '</td><td style="text-align: center;">'
                                . $item->FName . ' ' . $item->LName . '</td><td style="text-align: center">' . $item->AccessID . '</td>';
                            }
                            echo "<td style=\"text-align: center;\">" . $item->PreEventComments . '</td><td style="text-align: center;">'
                                . $item->SurveyRating . '</td><td style="text-align: center">' . $item->SurveyParagraph . '</td>';
                            echo '</tr>';
                        }
                        echo '</table><br />';
                    ?>
                    </div>
                <?php
                }

                echo '<p class="no-print"><a class="btn btn-info" href="' . $_settings['current_URL_path'] . '/admin/events#' . $_GET['EventID'] .'">Return to Events Listing</a></p>';
} ?>
            </div>
        </div>
    </div>
</div>