<?php
if ($isAdmin) {
?>
				<h1>Confirm and Email Attendance Report</h1>
			</div>
		
			<div class="body-content" id="body-content-padding">
				<?php
				if (isset($_GET['results'])) {
					$event = " the event";

					if (isset($_GET['EventID'])) {
						$event = 'Event #' . $_GET['EventID'];
					}

					switch ($_GET['results']) {
						case 'success':
							echo '<p>The attendance reports for '. $event . ' were successfully sent.</p>';
							break;
						case 'failure':
							echo '<p>There was a problem invoking the mailer. Please try again.</p>';
							break;
						default:
							echo '<p>An unknown error occurred while attempting to email the attendance reports for ' . $event . '. Please try again.</p>';
							break;
					}
				}
				echo '<p class="no-print"><a class="btn btn-info" href="' . $_settings['current_URL_path'] . '/admin/events">Return to Events Listing</a></p>';
} ?>
			</div>
		</div>
	</div>
</div>
