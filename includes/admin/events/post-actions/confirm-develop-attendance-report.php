<?php
if ($isAdmin) {
?>
                <h1>Develop Attendance Report</h1>
            </div>
        
            <div class="body-content" id="body-content-padding">
                <?php
                if (isset($_GET['results'])) {
                    $event = "the event";

                    if (isset($_GET['EventID'])) {
                        $event = 'Event #' . $_GET['EventID'];
                    }

                    switch ($_GET['results']) {
                        case 'success':
                            echo '<p>The attendance report for '. $event . ' was updated.</p>';
                            break;
                        case 'nochange':
                            echo '<p>No changes were made to the attendance report for ' . $event . '. If you have updates to enter, please go back and made them.</p>';
                            break;
                        default:
                            echo '<p>An unknown error occurred while attempting update the attendance report for ' . $event . '. Please try again.</p>';
                            break;
                    }
                }
                
                echo '<p class="no-print"><a class="btn btn-info" href="' . $_settings['current_URL_path'] . '/admin/events">Return to Events Listing</a></p>';
} ?>
            </div>
        </div>
    </div>
</div>
