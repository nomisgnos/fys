<?php       
    $courses = $adm->getCourses();
    $presenterOptions = $adm->getPresenters();
?>
<style>
textarea { resize:vertical; }
thead th {
    background: #fff;
    color: #000;
}
</style>
</div>
<div id="alert-area"></div>
<div class="col-md-12">
<form id="addEvent" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend align="left">Add Event</legend>

<div class="form-group required">
  <label class="col-md-3 control-label" for="Name">Name</label>  
  <div class="col-md-8">
    <input id="Name" name="Name" type="text" class="form-control input-md" required="" autofocus>
  </div>
</div>
<!-- Dropdown Select -->
<div class="form-group required">
    <label class="col-md-3 control-label" for="Type">Type</label>
    <div class="col-md-4">
        <select id="Type" name="Type" class="form-control">
            <option value="" selected disabled>Select Type</option>
            <option value="Academic">Academic</option>
            <option value="Advising">Advising</option>
            <option value="SCW">Social, Cultural, or Wellness</option>
            <option value="Title IX">Title IX</option>
            <option value="Writing Ethics">Writing Ethics</option>
            <option value="Unclassified">Unclassified</option>
        </select>
    </div>
</div>
<!-- Text input-->
<div class="form-group required" >
  <label class="col-md-3 control-label" for="Date">Date</label>  
  <div class="col-md-4">
    <input data-date-format="yyyy-mm-dd" id="Date" name="Date" type="text" class="form-control input-md datepicker" required="">
  </div>
</div>

<!-- Text input-->
<div class="form-group required">
  <label class="col-md-3 control-label" for="StartTime">Start Time</label>  
  <div class="col-md-2">
  <input id="StartTime" name="StartTime" type="text" class="form-control input-md" required="">
  </div>
    <label class="col-md-2 control-label" for="EndTime">End Time</label>  
  <div class="col-md-2">
  <input id="EndTime" name="EndTime" type="text" class="form-control input-md" required="">
  </div>
</div>


<!-- Text input-->
<div class="form-group required">
  <label class="col-md-3 control-label" for="Location">Location</label>  
  <div class="col-md-4">
  <input id="Location" name="Location" type="text" class="form-control input-md" required="">

  </div>
  <small id="roomBuilding" class="text-muted">Room, Building</small>
</div>

<!-- Text input-->
<div class="form-group required">
  <label class="col-md-3 control-label" for="Seats">Seats/Capacity</label>  
  <div class="col-md-2">
  <input id="Seats" name="Seats" type="text" class="form-control input-md" required="">

  </div>
</div>

<!-- Textarea -->
<div class="form-group required">
  <label class="col-md-3 control-label" for="Description">Description</label>
  <div class="col-md-8">                     
    <textarea class="form-control" id="Description" name="Description"></textarea>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="Link">Link</label>  
  <div class="col-md-8">
  <input id="Link" name="Link" type="text" class="form-control input-md">

  </div>
  <small id="eventWebsite" class="text-muted">Event Website</small>
</div>
<!-- Dropdown Select -->
<div class="form-group required">
    <label class="col-md-3 control-label" for="Presenter">Presenter</label>
    <div class="col-md-6">
        <select id="Presenter" name="Presenter" title="Select Presenter" data-live-search="true" data-live-search-placeholder="Search Presenter" class="selectpicker form-control">
            <?=$presenterOptions?>
        </select>
    </div>
</div>
<!-- Dropdown Select -->
<div class="form-group">
    <label class="col-md-3 control-label" for="CoPres1">Co-Presenter(s)</label>
    <div class="col-md-4">
        <select id="CoPres1" name="CoPres1" title="Select Co-Presenter 1" data-live-search="true" data-live-search-placeholder="Search Presenter" class="selectpicker form-control">
            <?=$presenterOptions?>
        </select>
    </div>
    <div class="col-md-4">
        <select id="CoPres2" name="CoPres2" title="Select Co-Presenter 2" data-live-search="true" data-live-search-placeholder="Search Presenter" class="selectpicker form-control">
            <?=$presenterOptions?>
        </select>
    </div>
</div>
<!-- Dropdown Select -->
<div class="form-group required">
    <label class="col-md-3 control-label" for="Semester">Semester</label>
    <div class="col-md-3">
        <select id="Semester" name="Semester" class="form-control">
            <option value="" selected disabled>Select Semester</option>
            <option value="1">Fall</option>
            <option value="2">Spring</option>
        </select>
    </div>
</div>

<?php
if($isAdmin) {
?>
    <!-- Dropdown Select -->
    <div class="form-group required">
        <label class="col-md-3 control-label" for="Active">Approved</label>
        <div class="col-md-3">
            <select id="Active" name="Active" class="form-control">
                <option value="0">No</option>
                <option value="1">Yes</option>
            </select>
        </div>
    </div>  
<?php
}
?>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-3 control-label" for="submitbtn"></label>
  <div class="col-md-8">
    <button id="submitbtn" name="submitbtn" class="btn btn-primary btn-raised">Submit</button>
    <button id="cancelbtn" name="cancelbtn" class="btn btn-danger btn-raised" type="reset">Reset</button>
  </div>
</div>

</fieldset>
</form>
</div>

<script>
$(document).ready(function() {
    ajaxHandleForm('#addEvent', 'post-add-event', 'div#post-id-add');
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
    $( "#StartTime" ).clockpicker({autoclose: true, twelvehour: true});
    $( "#EndTime" ).clockpicker({autoclose: true, twelvehour: true});
});
</script>