<?php
if ($adm->canUserAccess(array(1), $roleID) && isset($_GET['EventID'])) {
    $eventInfo = $adm->getEventInfo($_GET['EventID']);
    $count = $adm->getAttendanceCount($_GET['EventID']);
?>
                <h1>Confirm and Email Attendance Report</h1>
            </div>
        
            <div class="body-content" id="body-content-padding">

                <ul class="element-list">
                    <li><strong>Name:</strong> <?=$eventInfo[0]->Name;?></li>
                    <li><strong>Type:</strong> <?=$eventInfo[0]->Type;?></li>
                    <li><strong>Date:</strong> <?=date_format(date_create($eventInfo[0]->Date), 'm-d-Y');?></li>
                    <li><strong>Time:</strong> <?=$eventInfo[0]->StartTime . ' - ' . $eventInfo[0]->EndTime;?></li>
                    <li><strong>Location:</strong> <?=$eventInfo[0]->Location;?></li>
                </ul>

                <?php if ($count < 1) {
                    echo 'The attendance report for this event has not yet been developed. Please develop the <a href="develop-attendance-report?EventID='
                    . $_GET['EventID'] . '">attendance report</a> before attending to confirm attendence and sending out confirmation emails.';
                }
                else { ?>
                <p>Are you sure that you want to confirm and email the attendance report for the above event?</p>
                <p>A total of <?= $count; ?> emails will be sent out.</p>
                <form id="Attendance" action="" method="post">
                    <input type="hidden" name="EventID" value="<?= $_GET['EventID'] ?>" />
                    <input type="hidden" name="Redirect" value="redirectEmailAttendance" />
                    <input class="btn btn-success" type="submit" name="Submit" value="Email Attendance" />&nbsp;&nbsp;&nbsp;
                    <input class="btn btn-danger" type="button" value="Cancel" onclick="window.location.href='/fys/admin/events'">
                </form>
                <?php } 
}?>
            </div>
        </div>
    </div>
</div>
