<?php
if ($adm->canUserAccess(array(1, 4), $roleID) && isset($_GET['EventID'])) {
    $eventInfo = $adm->getEventInfo($_GET['EventID']);
    $recipients = $adm->getPreEventEmailRecipients($_GET['EventID']);
?>
                <h1>Send Pre-Event Email to Registered Students</h1>
            </div>
        
            <div class="body-content" id="body-content-padding">

                <p class="redtext">Please review this event's information before sending out pre-event emails.</p>

                <ul class="element-list">
                    <li><strong>Name:</strong> <?=$eventInfo[0]->Name;?></li>
                    <li><strong>Type:</strong> <?=$eventInfo[0]->Type;?></li>
                    <li><strong>Date:</strong> <?=date_format(date_create($eventInfo[0]->Date), 'm-d-Y');?></li>
                    <li><strong>Time:</strong> <?=$eventInfo[0]->StartTime . ' - ' . $eventInfo[0]->EndTime;?></li>
                    <li><strong>Location:</strong> <?=$eventInfo[0]->Location;?></li>
                </ul>

                <p>Please enter your message below and click submit to send.</p>

                <form id="PreEventEmail" action="" method="post">
                    <ul class="element-list">
                        <li>Subject:</br >
                            <input class="form-control" type="text" name="Subject" size="50" maxlength="100" required /></li>
                        <li>Message:</br >
                            <textarea class="form-control" name="Message" rows="5" cols="52" maxlength="2000" required></textarea></li>
                        <li>Personalize Greeting:</br>
                            <select class="form-control" name="Greeting" required>
                                <option value="">-- Select --</option>
                                <option value="No">No</option>
                                <option value="Yes">Yes</option>
                            </select></li>
                        <li>Reply-to email address: (Do not change this unless you know what you are doing) <br />
                            <input class="form-control" type="text" name="Reply" size="50" value="psh_fys_no_reply@psu.edu" required/></li>
                    </ul>
                    <input type="hidden" name="EventID" value="<?= $_GET['EventID'] ?>" />
                    <input type="hidden" name="Redirect" value="redirectPreEventEmail" />
                    <input class="btn btn-success" type="submit" name="Submit" value="Send Email" />&nbsp;&nbsp;&nbsp;<input class="btn btn-primary" type="reset" name="Reset" value="Reset" /> 
                </form>
                <br/>
                <p>Your email will be sent to the following students (and admins): </p>
                <ul class="element-list">
                    <?php
                    foreach($recipients as $recipient) {
                        echo '<li>' . $recipient->FName . ' ' . $recipient->LName . ' -- ' . $recipient->Email . '</li>'; 
                    }
                    ?>
                </ul>
<?php } ?>
            </div>
        </div>
    </div>
</div>

