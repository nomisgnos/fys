<?php
    if(!$isAdmin && !$isPresenter) {
        return;
    }
if (!isset($_POST['id']) || empty($_POST['id'])) {
    $msg = "Error! Could not delete event!";
} else {
    $adm->deleteEvent($_POST['id']);
    
    // TODO: DB error stuff
    $msg = "<strong>Success!</strong> Event ID: <strong>" . $_POST['id']  . "</strong> deleted!";
}
?>

<div id="post-id-delete">
<?=$msg?>
</div>
