<div id="post-id-add">
<?php
    if(empty($_POST))
        return;
    // todo: validate form ^
    if(empty($_POST['Active'])) {
        $_POST['Active'] = 0;
    }
    
    // AccessID of user creating new event
    $_POST['Signal'] = $accessID;
    
    // Add event
    $results = $adm->addEvent($_POST);
    
    // If event is added, email admins of new event
    if($results) {
        $adm->notifyAdminEvent($_POST, $results);
    }
    
    // todo:error handling
    $msg = "<strong>Success!</strong> Added Event: " . $_POST['Name'] . " - #" . $results;
?>

<?=$msg?>
</div>