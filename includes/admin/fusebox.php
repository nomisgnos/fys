<?php
    require_once('includes/admin.php');
    
    $adm = new AdminMethods($obj, $_settings, $userID);
    
    // Backup DB if download requested
    if(isset($_POST['BackupDB'])) {
        $adm->backupDB('download', true); 
    }
       
    if (isset($_POST) && isset($_POST['Redirect'])) {
        $reflectedMethod = new ReflectionMethod('AdminMethods', $_POST['Redirect']);
        $reflectedMethod->invoke($adm, $_POST);
    }

    echo $buffer;
    ob_flush();
    // note: testing
    //flush();

    // Show 403 Forbidden error if not admin/instructor/presenter
    if(!$adm->canUserAccess(array(1,2,3,4), $roleID)) {
        include 'includes/templates/403.php';
        return;
    }
    
    $bread = 'Dashboard';
    $action = false;
    require_once('userlinks.php');

    switch ($circuit) {
        case '':
            break;
        case checkCircuit($circuit, 'events'):
            $bread = 'Events';
            include 'events.php';
            break;
        case checkCircuit($circuit, 'students'):
            $bread = 'Students';
            include 'students.php';
            break;
        case checkCircuit($circuit, 'users'):
            $bread = 'Users';
            include 'users.php';
            break;
        case checkCircuit($circuit, 'courses'):
            $bread = 'Courses';
            include 'courses.php';
            break;
        case checkCircuit($circuit, 'reports'):
            $bread = 'Reports';
            include 'reports.php';
            break;
        case checkCircuit($circuit, 'import'):
            $bread = 'Import';
            include 'import.php';
            break;
        case checkCircuit($circuit, 'calendar'):
            $bread = 'Calendar';
            include 'calendar.php';
            break;
        case checkCircuit($circuit, 'semester'):
            $bread = 'Semesters';
            include 'semester.php';
            break;
        case checkCircuit($circuit, 'settings'):
            $bread = 'Settings';
            include 'user-settings.php';
            break;
        case checkCircuit($circuit, 'purge'):
            $bread = 'Purge Data';
            include 'purge-data.php';
        default:
            break;
    }
    
    $buffer = ob_get_contents();
    ob_clean();
    $buffer = str_replace("%BREAD%", $bread, $buffer);
    echo $buffer;

    if($bread == 'Dashboard') {
        foreach($roleID as $role) {
            if($role == 1) {
                include 'dashboards/admin-portal.php';
            } elseif($role == 2 || $role == 3) {
                include 'dashboards/instructor-portal.php';
            } elseif($role == 4) {
                include 'dashboards/presenter-portal.php';
            }
        }
    }

?>
<script>
    if(!<?=($action) ? "true" : "false"?>) {    
        $('#nav-<?=$bread?>').addClass('active');
    }
</script>