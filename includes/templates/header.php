<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
      xmlns:og="http://ogp.me/ns#"
      xmlns:article="http://ogp.me/ns/article#"
      xmlns:book="http://ogp.me/ns/book#"
      xmlns:profile="http://ogp.me/ns/profile#"
      xmlns:video="http://ogp.me/ns/video#"
      xmlns:product="http://ogp.me/ns/product#">

    <head profile="http://www.w3.org/1999/xhtml/vocab">
      <meta content="IE=edge" http-equiv="X-UA-Compatible" />
      
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="cleartype" content="on" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=2, minimum-scale=1, user-scalable=yes" name="viewport" />
    <meta name="description" content="Penn State Harrisburg brings nationally accredited academic programs, award-winning faculty, and the resources of a world-class research university to Pennsylvania&#039;s Capital Region." />
    <link rel="shortcut icon" href="https://harrisburg.psu.edu/profiles/psucampus/themes/psu_campus_base/favicon.ico" type="image/vnd.microsoft.icon" />

    <meta name="generator" content="Drupal 7 (http://drupal.org)" />
    <link rel="canonical" href="https://harrisburg.psu.edu/" />
    <link rel="shortlink" href="https://harrisburg.psu.edu/" />
    <meta property="og:site_name" content="Penn State Harrisburg" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://harrisburg.psu.edu/" />
    <meta property="og:title" content="Penn State Harrisburg" />
    
    <title>%TITLE%</title>
    <link href="<?= $_settings['current_URL_path'];?>/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <style>html{overflow-y: scroll;}</style>
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100%7COpen+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,600,800&amp;subset=latin,latin" media="all"/>
    
    <link type="text/css" rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/harrisburg/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
    <link type="text/css" rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/harrisburg/css_hTLrwzbU9bZhjvzx-j5entbJFEHkjJyd6RgHEla8FhA.css" media="all" />
    <link type="text/css" rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/harrisburg/css_vB5yJ7AUSu6IE1WN8787aPp0JXJZjSRAQB0B133mGLI.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/harrisburg/css_5jCwmu01u2IOoGZhNVF68i4U8EonSx6eYgqt1cqBbj0.css" media="all" />
    <link type="text/css" rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/harrisburg/css_LILne0TIDiHwUQIEg8CSWDxc_8OuJ2IxjDeIZdLpbOU.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/harrisburg/css__urxynZnAX3z9PgVeLZRR4zmcVbQf6sGPGW9S3qMybw.css" media="all" />
    <link type="text/css" rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/harrisburg/css_T_LgoGoUIm1jl4QDkFn-2i-5I7T1Ny-cOYtRyQlGfDU.css" media="all" />
    <link type="text/css" rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/harrisburg/css_s7egAqG3eLW9TAWP_R8Zkmcdfuaylw-tR2O9mUwow_E.css" media="all" />
        
        <!--<link rel="stylesheet" href="/sites/all/themes/penn_state_harrisburg/css/polaris-psh.css">-->

  <!-- New version of jQuery -->
    <script src="<?=$_settings['current_URL_path'];?>/resources/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/js/jquery.scrollUp.min.js"></script>
    
  <!-- jQuery scrollUp -->
    <script>
        $(function () {
          $.scrollUp({
            scrollName: 'scrollUp',
            topDistance: '300',
            topSpeed: 300,
            animation: 'slide',
            animationInSpeed: 200,
            animationOutSpeed: 200,
            scrollText: '<span class="glyphicon glyphicon-circle-arrow-up"></span>',
            activeOverlay: false, 
          });
        });
    </script>

  <!-- Bootstrap -->
    <link id="scrollUpTheme" href="<?= $_settings['current_URL_path'];?>/resources/css/jquery.scrollUp.css" rel="stylesheet">

    <link href="<?= $_settings['current_URL_path'];?>/resources/bootstrap/css/bootstrap-select.min.css" rel="stylesheet">
    <script src="<?= $_settings['current_URL_path'];?>/resources/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>

    <link rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/css/font-awesome.min.css" type="text/css" />

    <link rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/css/inherited_styles.css" type="text/css" />
    <?php
        // Resources for Admin section
        if (strpos(strtolower($circuit), 'admin') !== false) {
            //echo "\t" . '<link type="text/css" rel="stylesheet" href="' . $_settings['current_URL_path'] . '/resources/css/jquery-ui.min.css" type="text/css" />' . PHP_EOL;
            //echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/jquery.ui.core.min.js" charset="utf-8"></script>' . PHP_EOL;
            //echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/jquery.ui.widget.min.js" charset="utf-8"></script>' . PHP_EOL;
            //echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/jquery.validate.min.js" charset="utf-8"></script>' . PHP_EOL;
            
            echo '<link rel="stylesheet" href="' . $_settings['current_URL_path'] . '/resources/css/admin.css" type="text/css" />' . PHP_EOL;

                /* Bootstrap-select */
            echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/bootstrap/js/bootstrap-select.min.js" charset="utf-8"></script>' . PHP_EOL;
            
                /* Handle Forms with AJAX */
            echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/Forms.js" charset="utf-8"></script>' . PHP_EOL;
            
                /* Handle Admin Forms with AJAX */
            echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/AdminForms.js" charset="utf-8"></script>' . PHP_EOL;

                /* Bootstrap Datepicker */
            echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/bootstrap-datepicker.min.js" charset="utf-8"></script>' . PHP_EOL;
            echo "\t" . '<link rel="stylesheet" href="' . $_settings['current_URL_path'] . '/resources/css/bootstrap-datepicker3.min.css" type="text/css" />' . PHP_EOL;
            
                /* Bootstrap Clockpicker */
            echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/bootstrap-clockpicker.min.js" charset="utf-8"></script>' . PHP_EOL;
            echo "\t" . '<link rel="stylesheet" href="' . $_settings['current_URL_path'] . '/resources/css/bootstrap-clockpicker.min.css" type="text/css" />' . PHP_EOL;
        }
        
            /* Bootbox- Simplified handling for confirmation modals */
        echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/bootstrap/js/bootbox.min.js" charset="utf-8"></script>' . PHP_EOL;
        /* JS Utils */
        echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/Utils.js" charset="utf-8"></script>' . PHP_EOL;
    ?>
    
  <!-- Begin Google fonts 
    <script type="text/javascript">
      WebFontConfig = {
        google: { families: [ 'Roboto+Slab:400,700,300,100:latin', 'Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,600,800:latin' ] }
      };
      (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
          '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
      })();
    </script>
    -->

    <!-- Custom Stylesheets -->
    <link rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/css/styles.css" type="text/css" />
    <link rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/css/print.css" type="text/css" media="print" />
    <link rel="stylesheet" href="<?=$_settings['current_URL_path'];?>/resources/css/bootstrap-style.css" type="text/css" />

    <!-- dataTables -->
    <link rel="stylesheet" type="text/css" href="<?=$_settings['current_URL_path'];?>/resources/datatables/dataTables.bootstrap.min.css"/>
    
    <!-- Dynamically load body content -->
    <?php include("rendered/body.php"); ?>
    
    <!-- Prevent some of harrisburg CSS to impact FYS CSS -->
    <script>document.getElementById('header-outer-wrapper').id = 'header-wrapper-fys';</script>
    
    <!-- Mobile Nav JS -->
    <script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/harrisburg/harrisburg.mobile.nav.js"></script>

    <!-- CLOSING DIV IS LOCATED IN FOOTER FILE -->
    <section id="content-outer-wrapper" class="outer-wrapper clearfix">
    
        <div id="main-layout" class="main-layout inner-wrapper clearfix with--content without--sidebar-first without--sidebar-second">
            <main id="content" class="region--content column" role="main">
            