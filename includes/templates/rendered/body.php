<?php
    $file_contents = "http://harrisburg2.vmhost.psu.edu/sites/all/themes/penn_state_harrisburg/includes/rendered/header.php";
    $ci = curl_init();
    if (defined('CURLOPT_IPRESOLVE') && defined('CURL_IPRESOLVE_V4')){
       curl_setopt($ci, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    }
    curl_setopt($ci, CURLOPT_URL, $file_contents); 
    curl_setopt($ci, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.73 Safari/537.36"); 
    curl_setopt($ci, CURLOPT_FRESH_CONNECT, TRUE);
    curl_setopt($ci, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($ci, CURLOPT_TIMEOUT, 10); 
    curl_setopt($ci, CURLOPT_CONNECTTIMEOUT, 10);

    $content = curl_exec($ci);
    curl_close($ci);

    preg_match( '/(<body .*?>[\s\S]{1,})<link/', $content, $match );

    $content = $match[1];
    echo $content;
?>