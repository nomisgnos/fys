

  
  <div id="footer-outer-wrapper" class="outer-wrapper clearfix">
    <div id="footer-first-outer-wrapper" class="outer-wrapper clearfix without--footer with--footer-second without--footer-third">
      <div id="footer-layout-main" class="footer-layout inner-wrapper clearfix">
        <div id="footer" class="region--footer">
          
          <a id="bottom" name="bottom"></a>
          <footer id="footer-main" class="footer" role="contentinfo" class="clearfix">
              <div class="footer-campus-data">

                <div class="site-address">
                <address class="vcard">
                   <div class="adr">
                      <h4 class="org">Penn State Harrisburg</h4>
                      <div class="street-address">777 West Harrisburg Pike</div>
                      <span class="locality">Middletown, </span>
                      <span class="region">PA</span>
                      <span class="postal-code">17057</span>
                      <div class="tel">717-948-6000</div>
                   </div>
                </address>
                <div class="campus-map"><a href="https://harrisburg.psu.edu/" id="footer-campus-map" class="footer-campus-map active"><img src="https://harrisburg.psu.edu/sites/default/files/logos/map_hb.png" alt="Pennsylvania map showing Penn State campuses with Penn State Harrisburg highlighted" title="Penn State&#039;s campuses" /></a></div>
                </div>

                <div class="site-slogan"><p>A comprehensive college in southcentral Pennsylvania offering more than 65 undergraduate and graduate degree programs.</p>

<div> </div>
</div>
              </div>

              <div class="footer-menu">
                <div class="footer-information-nav">
                                    <div class="information-nav">
                    <h4 class="nav-label">Information for:</h4>
                    <ul id="information-menu" class="links clearfix"><li class="menu-319 first"><a href="https://harrisburg.psu.edu/admission">Future Students</a></li>
<li class="menu-324"><a href="https://harrisburg.psu.edu/parents-and-families">Parents &amp; Families</a></li>
<li class="menu-321"><a href="https://harrisburg.psu.edu/alumni-relations">Alumni</a></li>
<li class="menu-320"><a href="https://harrisburg.psu.edu/current-students">Current Students</a></li>
<li class="menu-322"><a href="https://harrisburg.psu.edu/faculty-and-staff">Faculty &amp; Staff</a></li>
<li class="menu-1254 last"><a href="https://harrisburg.psu.edu/continuing-education">Workforce Professionals</a></li>
</ul>                   </div>
                              </div>
                                  <div class="footer-main-nav">
                    <ul class="menu"><li class="first expanded"><a href="https://harrisburg.psu.edu/this-is-penn-state-harrisburg">This is Penn State</a><ul class="menu"><li class="first leaf"><a href="https://harrisburg.psu.edu/faculty-staff/administration">Administration</a></li>
<li class="leaf"><a href="https://harrisburg.psu.edu/about-us/vision-mission-and-values">Vision, Mission and Values</a></li>
<li class="leaf"><a href="https://harrisburg.psu.edu/board-of-advisers">Board of Advisors</a></li>
<li class="last leaf"><a href="https://harrisburg.psu.edu/about-us/outreach-and-economic-development-services">Outreach and Economic Development Services</a></li>
</ul></li>
<li class="expanded"><a href="https://harrisburg.psu.edu/academics">Academics</a><ul class="menu"><li class="first leaf"><a href="https://harrisburg.psu.edu/academics/undergraduate-programs">Undergraduate Degrees</a></li>
<li class="leaf"><a href="https://harrisburg.psu.edu/academics/graduate-programs">Graduate Degrees</a></li>
<li class="last leaf"><a href="https://harrisburg.psu.edu/academic-calendar">Academic Calendar</a></li>
</ul></li>
<li class="expanded"><a href="https://harrisburg.psu.edu/admission">Admission</a><ul class="menu"><li class="first leaf"><a href="https://harrisburg.psu.edu/office-of-admissions">Office of Admissions</a></li>
<li class="leaf"><a href="https://harrisburg.psu.edu/housing">Housing Options</a></li>
<li class="last leaf"><a href="https://harrisburg.psu.edu/veterans">Veterans</a></li>
</ul></li>
<li class="expanded"><a href="https://harrisburg.psu.edu/tuition-and-financial-aid">Tuition &amp; Financial Aid</a><ul class="menu"><li class="first leaf"><a href="https://harrisburg.psu.edu/financial-aid-office">Office of Student Aid</a></li>
<li class="leaf"><a href="https://harrisburg.psu.edu/tuition-and-financial-aid">Tuition &amp; Fee Calculators</a></li>
<li class="last leaf"><a href="https://harrisburg.psu.edu/bursar">Bursar&#039;s Office</a></li>
</ul></li>
<li class="expanded"><a href="https://harrisburg.psu.edu/research">Research</a><ul class="menu"><li class="first leaf"><a href="https://harrisburg.psu.edu/research-and-outreach">Office of Research and Outreach</a></li>
<li class="leaf"><a href="https://harrisburg.psu.edu/research/grant-project-life-cycle">Grant/Project Life Cycle</a></li>
<li class="last leaf"><a href="https://harrisburg.psu.edu/invent">Center for Innovation and Entrepreneurship</a></li>
</ul></li>
<li class="expanded"><a href="https://harrisburg.psu.edu/student-life">Student Life</a><ul class="menu"><li class="first leaf"><a href="https://harrisburg.psu.edu/office-of-student-life">Office of Student Life</a></li>
<li class="leaf"><a href="https://harrisburg.psu.edu/student-affairs">Student Affairs</a></li>
<li class="leaf"><a href="https://harrisburg.psu.edu/office-of-student-life/student-organizations">Clubs and Organizations</a></li>
<li class="last leaf"><a href="https://harrisburg.psu.edu/student-affairs/student-activity-fee">Student Activity Fee (SAF)</a></li>
</ul></li>
<li class="last expanded"><a href="https://harrisburg.psu.edu/community-safety-and-health">Community Safety and Health</a><ul class="menu"><li class="first leaf"><a href="https://harrisburg.psu.edu/safety-police-services">Safety and Police Services</a></li>
<li class="leaf"><a href="https://harrisburg.psu.edu/psualert">PSUAlert</a></li>
<li class="leaf"><a href="https://harrisburg.psu.edu/safety-police-services/emergency-response-plan">Emergency Response Plan</a></li>
<li class="last leaf"><a href="https://harrisburg.psu.edu/policy/c-5-inclement-weather">Weather Policy</a></li>
</ul></li>
</ul>                </div><!--end .nav-->
                          </div>
          </footer>
        </div>
      </div> <!-- /#footer -->
    </div>

    <div id="footer-second-outer-wrapper" class="outer-wrapper clearfix">
      <div id="footer-layout-social" class="footer-layout inner-wrapper clearfix">
        <div id="footer-second" class="region--footer-second">
                        <div class="region region-footer-second">
    <div id="block-psucampus-core-psu-social-media-services" class="block block-psucampus-core">

    <h2>Connect with us</h2>
  
  <div class="content social-icon-wrapper">
    <div  class="item-list"><ul class="social-share-list"><li class="social-share-list__item first"><a href="https://www.facebook.com/pennstateharrisburg?v=wall&amp;filter=1" class="prototype-icon prototype-icon-social-facebook" title="facebook"><span class="social-icon-text">facebook</span></a></li>
<li class="social-share-list__item"><a href="https://twitter.com/PSUHarrisburg" class="prototype-icon prototype-icon-social-twitter" title="twitter"><span class="social-icon-text">twitter</span></a></li>
<li class="social-share-list__item"><a href="http://www.flickr.com/photos/penn-state-harrisburg" class="prototype-icon prototype-icon-social-flickr" title="flickr"><span class="social-icon-text">flickr</span></a></li>
<li class="social-share-list__item"><a href="https://www.youtube.com/user/PennStateHarrisburg" class="prototype-icon prototype-icon-social-youtube" title="youtube"><span class="social-icon-text">youtube</span></a></li>
<li class="social-share-list__item last"><a href="https://instagram.com/pennstateharrisburg" class="prototype-icon prototype-icon-social-instagram" title="instagram"><span class="social-icon-text">instagram</span></a></li>
</ul></div>  </div>
</div>
  </div>
                  </div>
      </div> <!-- /#footer -->
    </div>

    <div id="footer-third-outer-wrapper" class="outer-wrapper clearfix">
      <div id="footer-layout-copy" class="footer-layout inner-wrapper clearfix">
        <div id="footer-third" class="region--footer-third">
          
          <div id="copyright-bar" class="clearfix">
                          <div class="footer-logo"><map name="footerlogo"><area shape="rect" coords="3,5,41,51" href="http://www.psu.edu"><area shape="rect" coords="42,0,116,51" href="https://harrisburg.psu.edu/"></map><img usemap="#footerlogo" src="https://harrisburg.psu.edu/profiles/psucampus/themes/psu_campus_base/images/footer_logo_new.png" alt="" title="" /></div>
                                      <div class="mobile-footer-logo"><map name="mobilefooterlogo"><area shape="rect" coords="4,9,53,61" href="http://www.psu.edu"><area shape="rect" coords="54,0,150,61" href="https://harrisburg.psu.edu/"></map><img usemap="#mobilefooterlogo" src="https://harrisburg.psu.edu/profiles/psucampus/themes/psu_campus_base/images/footer_logo_new.png" alt="" title="" /></div>
                        <div class="copy-left clearfix">

                              <nav class="nav-footer-info nav-footer">
                    <ul id="footer-info-menu" class="links clearfix inline"><li class="menu-341 first"><a href="https://psu.jobs/harrisburg/jobs">Employment</a></li>
<li class="menu-1280"><a href="https://harrisburg.psu.edu/campus-map">Map</a></li>
<li class="menu-1269"><a href="http://www.psu.edu/hotlines">Hotlines</a></li>
<li class="menu-1279"><a href="https://harrisburg2.vmhost.psu.edu/calendar">Calendar</a></li>
<li class="menu-1281"><a href="https://harrisburg.psu.edu/campus-directory">Directory</a></li>
<li class="menu-338 last"><a href="https://harrisburg.psu.edu/search/gss">Search</a></li>
</ul>                </nav><!--end .nav-->
                            <div class="address-copy">
                <address class="vcard">
                   <div class="adr">
                      <span class="street-address">777 West Harrisburg Pike, </span><span class="locality">Middletown</span>, <span class="region">PA</span> <span class="postal-code">17057</span> | <span class="tel">717-948-6000</span>
                   </div>
                </address>
              </div>
            </div>
            <div class="copy-right clearfix">
                              <nav class="nav-footer-legal nav-footer">
                    <ul id="footer-legal-menu" class="links clearfix inline"><li class="menu-327 first"><a href="http://www.psu.edu/web-privacy-statement">Privacy</a></li>
<li class="menu-317"><a href="http://guru.psu.edu/policies/AD85.html">Non-discrimination</a></li>
<li class="menu-316"><a href="http://guru.psu.edu/policies/OHR/hr11.html">Equal Opportunity</a></li>
<li class="menu-329"><a href="https://harrisburg.psu.edu/accessibility-statement">Accessibility</a></li>
<li class="menu-326 last"><a href="http://www.psu.edu/copyright-information">Copyright</a></li>
</ul>                </nav><!--end .nav-->
              
              <div class="copyright-copy">The Pennsylvania State University &copy; 2017</div>
            </div>

          </div>
        </div>
      </div> <!-- /#footer -->
    </div>
  </div> <!-- /#footer-outer-wrapper -->
  </div> <!-- /#page -->
</div> <!-- /#page-wrapper -->
        
        <!--<script type="text/javascript" src="/sites/all/themes/penn_state_harrisburg/js/polaris-psh.js" defer="defer"></script>-->
    
</html>
