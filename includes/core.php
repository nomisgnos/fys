<?php
    /**
    *FYS DB Core methods
    *
    *Contains core functionality for FYS pages.
    *
    *@package FYS
    */
    
    /**
    *FYS DB Core methods 
    *
    *Contains core functionality for FYS pages.
    *
    */
    class Core {
        // Properties
        
        /**
         *@var int $total_row Number of rows
         */
        public  $total_row = 0;

        /**
         *@var int $limit   Number of results to fetch from query
         */
        public $limit = 20;
        
        /**
         *@var int $page    Page number
         */
        public $page = 1;

        /**
         *__construct Constructor for Core
         *
         *@param string[] $settings Array of settings
         *@return void
         */
        public function __construct($settings = '') {
            $this->settings = $settings;
        }

        /**
        * Provides safe framework for running SQL queries.
        *
        * @param  string $sqlQuery         provided query string to be run
        * @param  bool $bindValuesArray  array of values to be bound to sql stmt
        * @param  string $limit            number of results to return from query e.g. LIMIT 0, $limit
        * @param  int $page             provides an offset if pagination is used on results
        * @param  bool $debugMode        flag that enables debugging information to be displayed
        * @param  bool $returnRowCount   flag that returns row count of executed query if enabled
        * @return object                  fetched sql object from executed query
        */
        public function executeSQL($sqlQuery = '', $bindValuesArray = false, $limit = 'all', $page = 1, $debugMode = false, $removeNulls = false, $returnRowCount = false) {
            $this->limit    = $limit;
            $this->page     = $page;
            $executed       = null;

            try {
                $pdo = new PDO('mysql:host=' . $this->settings['dbHost'] . ';charset=utf8;dbname=' . $this->settings['dbName'], $this->settings['dbUsername'], $this->settings['dbPassword'], array(PDO::MYSQL_ATTR_LOCAL_INFILE => true,));
                $pdo->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('PDOStatement_Remove_Nulls', array($pdo)));

                if ($debugMode || !empty($_REQUEST['debug'])) {
                    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
                    ini_set('display_errors', 'On');
                }
            }
            catch (PDOException $e) {
                echo 'Connection failed: ' . $e->getMessage();
                exit;
            }
            
            if ($limit != 'all' ) {
                try {
                    if (strpos(strtolower(trim($sqlQuery)),'select ') === 0) { //most likely a select statement...
                        $sql                = $pdo->prepare($sqlQuery);
                        $sql                = $this->bindValues($bindValuesArray,$sql,$sqlQuery);
                        $sql->execute();
                        $executed           = $sql->fetchAll(PDO::FETCH_OBJ);
                        $this->total_row    = count($executed);
                    }
                    if (strpos(strtolower(trim($sqlQuery)),'limit ') !== false) {
                        throw new Exception(' The reserved MySQL word "LIMIT" found in your query. Did you know you can pass in the limit and page values in "executeSQL(...)"? ');
                    }
                }
                catch (Exception $e) {
                    echo '<div class="error"><h1><span>Database Error</span></h1> <strong>Reason:</strong> '.$e->getMessage();
                    var_dump($value); 
                    echo '<p class="contact">'.$this->settings['errorMsgFooter'].'</p></div>';
                    exit;
                }
                
                $query  = $sqlQuery . " LIMIT " . ( ( $page - 1 ) * $limit ) . ", $limit";
            }
            else {
                $query  = $sqlQuery;
            }
            
            $sql = $pdo->prepare($query);

            if ($removeNulls == true) {
                $sql->removeNulls = true;
            }

            $sql = $this->bindValues($bindValuesArray,$sql,$sqlQuery);

            try {
                $sql->execute();
                
                if($returnRowCount) {
                    return $sql->rowCount();
                }
                
                if (strpos(strtolower(trim($sqlQuery)),'insert ') === 0) {
                    return (int)$pdo->lastInsertId(); 
                }
                elseif(strpos(strtolower(trim($sqlQuery)), 'delete') === 0 ||
                    strpos(strtolower(trim($sqlQuery)), 'update') === 0) {
                    return $sql->rowCount();
                }
            }
            catch(PDOException $e) {
                echo '<div class="error"><h1><span>Database Error</span></h1> <strong>Reason:</strong> '.$e->getMessage().'. Could be a result of the same parameter names.<hr><strong>Bind Values:</strong> ';
                echo '<pre>';var_dump($bindValuesArray);echo '</pre>';
                echo '<hr /><strong>Query:</strong>';
                echo '<pre>';print_r($sql->queryString);echo '</pre>';
                echo '<p class="contact">'.$this->settings['errorMsgFooter'].'</p></div>';
                exit;
            }

            try {
                $executed = $sql->fetchAll(PDO::FETCH_OBJ);
            }
            catch (PDOException $e) {
                return null;
            }
            
            return $executed;
        }       

        /**
         *bindValues Binds values to SQL query from array of values
         *
         *@param bool|mixed[] $bindValuesArray Array of values to bind
         *@param object $sql             SQL object
         *@param string $sqlQuery        SQL query to modify
         *@return object SQL object after binding values to query
         */
        public function bindValues($bindValuesArray=false,$sql,$sqlQuery='')
        {
            if ($bindValuesArray != false) {
                try {                   
                    $count=0;
                    foreach ($bindValuesArray as &$value) {
                        if ($bindValuesArray[$count] === null) {
                            throw new Exception('Array #' . $count. ' is null.  Please make sure you have the correct numeric value for the first dimension.');
                        }
                        if (array_key_exists('parameter',$value) && strpos($sqlQuery, $value['parameter']) === false) {
                            throw new Exception('Bind parameter not found for ' . $value['parameter'] . ' within the SQL statement for:  ' . $sqlQuery);
                        }
                        if (array_key_exists('parameter',$value) && 
                            array_key_exists('value',$value) && 
                            array_key_exists('data_type',$value)) {
                                $sql->bindValue($value['parameter'],  !empty($value['value'])   ?   $value['value'] : NULL, $value['data_type']);
                        }
                        else {
                            throw new Exception('Bind values does not contain the following: parameter, value, data_type');
                        }
                        $count++;
                    }
                }
                catch (Exception $e) {
                    echo '<div class="error"><h1><span>Database Error</span></h1> <strong>Reason:</strong> '.$e->getMessage().'<hr><strong>Bind Values:</strong> ';
                    var_dump($value); 
                    echo '<p class="contact">'.$this->settings['errorMsgFooter'].'</p></div>';
                    exit;
                }
            }
            return $sql;
        }
        
        /**
        * canUserAccess     Checks if the role id of the user is appropriate for the current page
        *  
        *@param $permittedIDs   Array of permitted role ids
        *@param $roleIDs      Array of role ids of current user, default value of -1 used to trigger roleID lookup
        *@return  bool response indicating if user should be able to view page content
        */
        public function canUserAccess($permittedIDs, $roleIDs = -1) {
            if ($roleIDs == -1) {
                $roleIDs = $this->core->getRoleID();
            }

            if(!$roleIDs) {
                return false;
            }

            foreach($roleIDs as $roleID) {
                if (in_array($roleID, $permittedIDs)) {
                    return true;
                }
            }

            return false;
        }
        
        /**
        * Gets REMOTE_USER from $_SERVER global array. In this case, it
        * retrieving the WebAccess user id if they are signed in. In PROD this
        * should always return a value even if getUserID doesn't.
        * ================================================
        *@return string returns WebAccess ID from $_SERVER array
        */
        public function getAccessID() {
            $rm = 'REMOTE_USER';
            if (array_key_exists($rm , $_SERVER)) {
                return $_SERVER[$rm];
            }
            return null;
        }
        
        /**
        * Retrieves from FYS_User_Table the user id associated access id 
        *
        *@param  string $accessID  webaccess ID retreived from $_SERVER['REMOTE_USER']
        *@return int        returns user id from user table if access id is matched
        */
        public function getUserID($accessID) {
            if (isset($accessID)) {
                $i=0;$arr=null;
                $arr[$i]['parameter']=':remote_user';
                $arr[$i]['value']=$accessID;
                $arr[$i]['data_type']=PDO::PARAM_STR;

                $query = 'SELECT UserID
                          FROM FYS_User_Table
                          WHERE AccessID = :remote_user';

                $userID = $this->executeSQL($query, $arr);

                /* Since AccessID in FYS_User_Table contains only unique values,
                Only 1 match will be returned by the query. isset returns
                a true if it just looks at an empty array of $userID, so we
                look at the first and only index in the array and return appropriately */

                if (isset($userID[0])) {
                    return $userID[0]->UserID; //pointed to UserID
                }
            }
            return null;
        }

        /**
        *getRoleID  Queries for role id associated with user
        *
        *@return  int[] Array of roleIDs for user
        */
        public function getRoleID($userID) {
            if(empty($userID)) {
                return 0;
            }
            
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':userID';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_STR;

            $query = 'SELECT User_Role_ID
                     FROM FYS_User_Roles_XREF
                     WHERE UserID = :userID';

            $roles = $this->executeSQL($query, $arr);
            if (!empty($roles)) {
                $role_ids = [];
                foreach($roles as $role) {
                    array_push($role_ids, (int) $role->User_Role_ID);
                }
                return $role_ids;
            }

            return 0;
        }

        /**
         *getUserInfo Retrieves user's name and email based on AccessID
         *
         *@param int $accessID AccessID of user to fetch
         *@return object Object of user
         */
        public function getUserInfo($accessID) {

            if (isset($accessID)) {
                $i=0; $arr=null;
                $arr[$i]['parameter']=':accessID';
                $arr[$i]['value']=$accessID;
                $arr[$i]['data_type']=PDO::PARAM_STR;

                $query = 'SELECT FName, LName, Email
                          FROM FYS_User_Table
                          WHERE AccessID = :accessID';

                $results = $this->executeSQL($query, $arr);

                if (isset($results[0])) {
                    return $results[0];
                }
            }
            return null;
        }
        
        /**
         *match_in_arrays Checks 2 arrays and returns true if any value intersect
         *
         *@param mixed[] $arr  Array 1
         *@param mixedp[ $arr1 Array 2
         *@return bool 
         */
        public function match_in_arrays($arr, $arr1) {
            foreach($arr as $val) {
                if(in_array($val, $arr1)) {
                    return true;
                }
            }
            return false;
        }
    }

    // http://stackoverflow.com/questions/34311789/how-to-insert-empty-string-instead-of-null-in-php-pdo
    // http://php.net/manual/en/pdo.setattribute.php
    // IMPORTANT! This corrects behavior in the Core.bindValues method. This is not a workaround for default
    // PDO functionality. Consider modifying bindValues with flag parameter to disable the check for null/empty values.

    class PDOStatement_Remove_Nulls extends PDOStatement {
        public $dbh;
        public $removeNulls = false;
        
        protected function __construct($dbh) {
            $this->dbh = $dbh;
        }

        public function execute($input_parameters = null) {
            if ($this->removeNulls == true && is_array($input_parameters)) {
                   foreach ($input_parameters as $key => $value) {
                    if (is_null($value)) {
                        $input_parameters[$key] = '';
                    }
                }
            }
            return parent::execute($input_parameters);
        }

        public function bindValue($parameter, $value, $data_type = PDO::PARAM_STR) {
            if ($this->removeNulls === true && is_null($value)) {
                $value = '';
            }

            return parent::bindValue($parameter, $value, $data_type);
        }

        public function bindParam($parameter, &$variable, $data_type = PDO::PARAM_STR, $length = null, $driver_options = null) {
            if ($this->removeNulls === true && is_null($variable)) {
                $variable = '';
            }

            parent::bindParam($parameter, $variable, $data_type, $length, $driver_options);
        }
    }
?>
