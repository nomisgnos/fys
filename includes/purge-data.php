<?php
/**
 *Methods for FYS Purge Data Tool
 *
 *Contains all methods used by FYS Purge Data tool to execute DB queries
 *
 */
 
/**
 *Methods for FYS Purge Data Tool
 *
 *Contains all methods used by FYS Purge Data Tool to execute DB queries
 *
 *@package FYS
 */
Class PurgeDataMethods {
    
    /**
     *__construct Constructor for SemesterMethods
     *
     *@param AdminMethods $this->adm Class object of AdminMethods
     *@return void
     */
    public function __construct($adm) {
        $this->adm = $adm;
    }

    /**
     *handlePurgePreview Handle preview of data to purge
     *
     *@param void $post Post array of data to preview
     *@return null
     */
    public function handlePurgePreview($post) {
        echo "<h2>Filtered Data</h2>";
        // Purge Events preview
        if(!empty($post['purgeEvents'])) {
            $purgeSem = empty($post['purgeSemester']) ? 0 : $post['purgeSemester'];

            if($purgeSem == 1) {
                $sem = "Fall";
            } else if($purgeSem == 2) {
                $sem = "Spring";
            } else if($purgeSem == 0) {
                $sem = "Fall and Spring";
            }

            echo "<h3>Events <small class='label label-info'>" . $sem ."</small></h3>";
            $events = $this->adm->getEvents($purgeSem);
            $this->pretty_print_arr($events);
        }
        

        // Purge Registrations preview
        if(!empty($post['purgeRegistrations'])) {
            $purgeSem = empty($post['purgeSemester']) ? 0 : $post['purgeSemester'];

            if($purgeSem == 1) {
                $sem = "Fall";
            } else if($purgeSem == 2) {
                $sem = "Spring";
            } else if($purgeSem == 0) {
                $sem = "Fall and Spring";
            }
            
            echo "<h3>Registrations <small class='label label-info'>" . $sem . "</small></h3>";
            $registrations = $this->adm->getRegistrations($purgeSem);
            $this->pretty_print_arr($registrations);
        }
        
        // Purge Courses preview
        if(!empty($post['purgeCourses'])) {
            echo "<h3>Courses</h3>";
            $courses = $this->adm->getCourses();
            $this->pretty_print_arr($courses);
        }
        
        // Purge Instructors preview
        if(!empty($post['purgeInstructors'])) {
            echo "<h3>Instructors</h3>";
            $instructors = $this->adm->getInstructorsUsers();
            $this->pretty_print_arr($instructors);
        }
        
        // Purge Students preview
        if(!empty($post['purgeStudents'])) {
            echo "<h3>Students</h3>";
            $students = $this->adm->getStudents();
            $this->pretty_print_arr($students);
        }
        
        if(!empty($post['purgePresenters'])) {
            echo "<h3>Presenters</h3>";
            $presenters = $this->adm->getPresenters(false);
            $this->pretty_print_arr($presenters);
        }
    }
    
    /**
     *handlePurgeData Handles all data to purge
     *
     *@param void $post post array of data to purge
     *@return int[] total rows deleted
     */
    public function handlePurgeData($post) {
        // Backup DB
        $this->adm->backupDB('purge');
        
        $totals = [];
        
        // Purge Registrations
        if(!empty($post['purgeRegistrations'])) {
            $purgeSem = empty($post['purgeSemester']) ? 0 : $post['purgeSemester'];
            
            $registrations = $this->clearRegistrations($purgeSem);
            $totals["Registrations"] = $registrations;
        }
        
        // Purge Courses
        if(!empty($post['purgeCourses'])) {
            $courses = $this->clearCourses();
            $totals["Courses"] = $courses;
        }
        
        // Purge Instructors
        if(!empty($post['purgeInstructors'])) {
            $instructors = $this->clearInstructors();
            $totals["Instructors"] = $instructors;
        }
        
        // Purge Students
        if(!empty($post['purgeStudents'])) {
            $students = $this->clearStudents();
            $totals["Students"] = $students;
        }
        
        // Purge Presenters
        if(!empty($post['purgePresenters'])) {
            $presenters = $this->clearPresenters();
            $totals["Presenters"] = $presenters;
        }
        
        // Purge Events
        if(!empty($post['purgeEvents'])) {
            $purgeSem = empty($post['purgeSemester']) ? 0 : $post['purgeSemester'];

            $events = $this->clearEvents($purgeSem);
            $totals["Events"] = $events;
        }
        
        return $totals;
    }
    
    /**
     *clearCourses Purge courses
     *
     *@return int Rows deleted
     */
    private function clearCourses() {
        $query = 'DELETE FROM FYS_Course_Table';

        return $this->adm->core->executeSQL($query);
    }
    
    /**
     *clearStudents Purge students, user roles xref, user course xref
     *
     *@return int Rows deleted
     */
    private function clearStudents() {
        $query = 'DELETE usr.*, role.*, crs.*  
                    FROM FYS_User_Roles_XREF role
                    LEFT JOIN FYS_User_Table usr ON role.UserID = usr.UserID 
                    LEFT JOIN FYS_User_Courses_XREF crs ON role.UserID = crs.UserID 
                    WHERE role.User_Role_ID = 5';
                    
        return $this->adm->core->executeSQL($query);
    }
    
    /**
     *clearInstructors Purge instructors, user roles xref
     *
     *@return int Rows deleted
     */
    private function clearInstructors() {
        $query = 'DELETE usr.*, role.*
                    FROM FYS_User_Roles_XREF role
                    LEFT JOIN FYS_User_Table usr  ON role.UserID = usr.UserID 
                    WHERE role.User_Role_ID IN(2,3)';
                    
        return $this->adm->core->executeSQL($query);
    }
    
    /**
     *clearPresenters Purge Presenters, user roles xref
     *
     *@return int Rows deleted
     */
    private function clearPresenters() {
        $query = 'DELETE usr.*, role.*
                    FROM FYS_User_Roles_XREF role 
                    LEFT JOIN FYS_User_Table usr ON role.UserID = usr.UserID 
                    WHERE role.User_Role_ID = 4';
                    
        return $this->adm->core->executeSQL($query);
    }
    
    /**
     *clearEvents Purge events
     *
     *@param void $sem Specified semester, 0 = both
     *@return int Rows deleted
     */
    private function clearEvents($sem=0) {
        if(!$sem) {
            $query = 'DELETE FROM FYS_Cocurricular_Events';
            return $this->adm->core->executeSQL($query);
        }
        
        $i = 0; $arr = null;
        $arr[$i]['parameter'] = ':semid';
        $arr[$i]['value'] = $sem;
        $arr[$i]['data_type'] = PDO::PARAM_INT;

        $query = 'DELETE FROM FYS_Cocurricular_Events
                    WHERE semesterid = :semid';

        return $this->adm->core->executeSQL($query, $arr);
    }
    
    /**
     *clearRegistrations Purge registrations
     *
     *@param void $sem Specified semester, 0 = both
     *@return int Rows deleted
     */
    private function clearRegistrations($sem=0) {
        if(!$sem) {
            $query = 'DELETE FROM FYS_Registration_Table';
            return $this->adm->core->executeSQL($query);
        }
        
        $i = 0; $arr = null;
        $arr[$i]['parameter'] = ':semid';
        $arr[$i]['value'] = $sem;
        $arr[$i]['data_type'] = PDO::PARAM_INT;

        $query = 'DELETE FROM FYS_Registration_Table 
                    INNER JOIN FYS_Cocurricular_Events 
                    USING(EventID)
                    WHERE semesterid = :semid';

        return $this->adm->core->executeSQL($query, $arr);
    }
    
    /**
     *pretty_print_arr Print array string JSON
     *
     *@param void $arr object to display
     *@return null
     */
    public function pretty_print_arr($arr) {
        $json = json_encode($arr, JSON_PRETTY_PRINT);
        
        echo "<h5>Total count: " . count($arr) ."</h5><br>";
        
        echo "<pre class='pre-scrollable'>";
        var_dump($json);
        echo "</pre>";
    }
}
?>