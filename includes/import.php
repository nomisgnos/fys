<?php
/**
 *Methods for FYS Import tool
 *
 *Contains all methods used by Import tool to execute DB queries
 *
 */
 
/**
 *Methods for FYS Import tool
 *
 *Contains all methods used by Import tool to execute DB queries
 *
 *@package FYS
 */
Class ImportMethods {
    
    /**
     *@var string $absoluteFileName File name for export data to csv
     */
    private $absoluteFileName = '';
    
    /**
     *@var string $baseDir Directory to export CSV files
     */
    private $baseDir = '/fys/includes/admin/import/exports/';
    
    /**
     *@var object $adm AdminMethods class object
     */
    private $adm;
    
    /**
     *@var string[] $keys Expected header columns in array format
     */
    private $keys = array("Campus", "SEMESTER", "FIRST NAME", "LAST NAME", "STUDENT_ID", "ACCESS_ID", "EMAIL", "SCHEDULE_NUMBER", "Catalog", "Subject", "Section", "INSTR FIRST NAME", "INSTR LAST NAME", "INSTR ACCESS ID", "LionPATH_FYS_REPORT_DATE");

    /**
     *@var string BASE_FILE_NAME Primary file name for CSV file export, appended at end
     */
    const BASE_FILE_NAME = 'fys_import.csv';
    
    /**
     *__construct Constructor for ImportMethods
     *
     *@param AdminMethods $adm Class object of AdminMethods
     *@return void
     */
    public function __construct($adm) {
        $this->adm = $adm;
    }
    
    /**
     *excelParseFile Reads excel file data and parses data by combining keys with rows/columns data
     *
     *@param string $name           File name of excel file to read
     *@param int    $headerNumber   Line number of header
     *@return string[][] Parsed data 
     */
    public function excelParseFile($name, $headerNumber = 1) {
        $excel = new Spreadsheet_Excel_Reader;        
        $excel->read($name);
        $sheet = $excel->sheets[0];
        $cells = $sheet['cells'];
        $header = $cells[$headerNumber];

        // Validate header based on expected keys and columns found
        if(!$this->verifyDataHeader($header)) {
            echo '<h3 style="color:red">Please verify that the columns found matches the expected columns. Make sure the Header Row Number is set correctly!</h3><h4>Expected Columns</h4><pre>';
            var_export($this->keys);
            echo '</pre><h4>Columns Found</h4><pre>';
            var_export($header);
            echo '</pre>';
            return false;
        }
        
        for($x = $headerNumber+1; $x <= $sheet['numRows']; $x++) {
            // Combine header array as KEYS and row array as VALUES
            $a = array_combine($header, $cells[$x]);
            $newCells[] = $a;
        }

        return $newCells;
    }

    /**
     *verifyDataHeader Verify keys/columns of the row to make sure they are defined 
     *
     *@param string[] $header   Array of header keys found in file
     *@return bool
     */
    private function verifyDataHeader($header) {
        // Probably wrong header row number
        if(empty($header))
            return false;
        
        return !array_diff_key(array_flip($this->keys), array_flip($header));
    }
    
    /**
     *pushData Writes data to CSV and imports that into DB
     *
     *@param void $data Import data to insert into DB
     *@return void
     */
    public function pushData($data) {
        // Export data to CSV file
        $this->writeToCSV($data);
        
        // Import data to import table with generated CSV file
        $rowsImported = $this->loadDataInfile($this->absoluteFileName);
        
        $this->displayNumRows("Imported Rows: " . $rowsImported);
    }
    
    /**
     *importData Merge temp data from import table into FYS tables
     *
     *@param string[] $row Array of row values
     *@return void
     */
    public function importData() {
        // Backup DB
        $this->adm->backupDB('import');
        
        // Import students, instructors, courses
        $studentsImported = $this->importStudents();
        $instrImported = $this->importInstructors();
        $crsImported = $this->importCourses();
        
        // Link user courses and user roles
        $userCrsLinked = $this->linkUserCourses();
        $usrRolesLinked = $this->linkUserRoles();
        
        // Truncate table
        $this->clearImportTable();
        
        // Display import information
        $this->displayNumRows("Students Added: " . $studentsImported);
        $this->displayNumRows("Instructors Added: " . $instrImported);
        $this->displayNumRows("Courses Added/Updated: " . $crsImported);
        $this->displayNumRows("User Courses Linked: " . $userCrsLinked);
        $this->displayNumRows("User Roles Linked: " . $usrRolesLinked);
    }
    
    /**
     *importStudents Inserts missing students in user table
     *
     *@return int Number of rows imported
     */
    private function importStudents() {
         $query = "INSERT IGNORE INTO FYS_User_Table (FName, LName, AccessID, Email, UserRole, importedOn)  
                    SELECT FName, LName, AccessID, Email, 5, now() 
                    FROM FYS_Import_Table import";

        $response = $this->adm->core->executeSQL($query, false, 'all', 1, false, false, true);
        return $response;
    }

    /**
     *importInstructors Inserts missing instructors in user table
     *
     *@return int Number of rows imported
     */
    private function importInstructors() {
        $query = "INSERT IGNORE INTO FYS_User_Table (FName, LName, AccessID, Email, UserRole, importedOn)  
                    SELECT DISTINCT Instr_FName, Instr_LName, Instr_AccessID, CONCAT(Instr_AccessID, '@psu.edu'), 2, now() 
                    FROM FYS_Import_Table import";
        
        $response = $this->adm->core->executeSQL($query, false, 'all', 1, false, false, true);
        return $response;
    }
    
    /**
     *importCourses Inserts missing courses into courses table or updates existing ones
     *
     *@return int Number of rows imported
     */
    private function importCourses() {
        $query = "INSERT INTO FYS_Course_Table (Name, Number, Section, Instructor, InstrEmail, InstructorID, ScheduleNumber, importedOn)
                    SELECT Subject, Catalog, Section, CONCAT(Instr_FName, ' ', Instr_LName), CONCAT(Instr_AccessID, '@psu.edu'), Instr_AccessID, ScheduleNumber, now()
                    FROM FYS_Import_Table import
                    ON DUPLICATE KEY UPDATE Name=import.Subject, Number=import.Catalog, Section=import.Section, Instructor=CONCAT(import.Instr_FName, ' ', Instr_LName), 
                    InstrEmail=CONCAT(import.Instr_AccessID, '@psu.edu'), InstructorID=import.Instr_AccessID, importedOn=now()";
        
        $response = $this->adm->core->executeSQL($query, false, 'all', 1, false, false, true);
        return $response;
    }
    
    /**
     *linkUserCourses Connects students and their courses based on the import data
     *
     *@return int Number of rows imported
     */
    private function linkUserCourses() {
        $query = "INSERT IGNORE INTO FYS_User_Courses_XREF (UserID, CourseID, importedOn) 
                    SELECT usr.UserID, crs.CourseID, now() FROM FYS_Course_Table crs
                    INNER JOIN FYS_Import_Table import ON crs.ScheduleNumber = import.ScheduleNumber
                    INNER JOIN FYS_User_Table usr ON usr.AccessID = import.AccessID";
        
        $response = $this->adm->core->executeSQL($query, false, 'all', 1, false, false, true);
        return $response;
    }
    
    /**
     *linkUserRoles Adds missing user roles to FYS_User_Roles_XREF based on UserID
     *
     *@param string[] $row Array of row values
     *@return int UserID of new student
     */
    private function linkUserRoles() {
        $query = "INSERT INTO FYS_User_Roles_XREF (UserID, User_Role_ID) 
                    SELECT UserID, UserRole FROM `FYS_User_Table` usr 
                        WHERE UserID 
                        NOT IN ( SELECT UserID From `FYS_User_Table` JOIN `FYS_User_Roles_XREF` USING(UserID) )";
                        
        $response = $this->adm->core->executeSQL($query, false, 'all', 1, false, false, true);
        return $response;
    }
    
    /**
     *clearImportTable Removes all data from fys import table
     *
     *@return object Response of query execute
     */
    private function clearImportTable() {
        $query = "TRUNCATE FYS_Import_Table";
        $response = $this->adm->core->executeSQL($query, false, 'all', 1, false, false, true);
        return $response;
    }
    
    /**
     *loadDataInfile Imports data to FYS_Import_Table for merging
     *
     *@param string $file Import file name
     *@return int Number of rows imported
     */
    private function loadDataInfile($file) {
        $query = "LOAD DATA LOCAL INFILE '" . $file . "' INTO TABLE FYS_Import_Table
                    FIELDS TERMINATED BY ','
                    LINES TERMINATED BY '\n'";
        
        $response = $this->adm->core->executeSQL($query, false, 'all', 1, false, false, true);
        return $response;
    }
    
    /**
     *writeToCSV Exports import data to baseFileName
     *
     *@param string $file Base file name for output file
     *@param object[] $list Array of import rows and data
     *@return void
     */
    private function writeToCSV($list) {
        // Make sure list is not empty
        $total = count($list);
        if(!$total) {
            return 0;
        }
        
        // Fetch time now and append to file name
        $now = date('y_m_d_h_i_', strtotime("now"));
        $this->absoluteFileName = $_SERVER['DOCUMENT_ROOT'] . $this->baseDir . $now . self::BASE_FILE_NAME;
        
        // Create or rewrite new file
        $fo = fopen($this->absoluteFileName, 'w+');
        
        // Write to file
        foreach($list as $fields) {
            $trimmed = array_map('trim', $fields);
            fputs($fo, implode($trimmed, ',')."\n");
        }
    }
    
    /**
     *displayNumRows Prints out readable message for num rows added/updated
     *
     *@param string $msg Message to display
     *@return void
     */
    private function displayNumRows($msg) {
        echo "<h5>" . $msg . "</h5>";
    }
}
?>