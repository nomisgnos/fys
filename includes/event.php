<?php
    /**
     *FYS EventMethods - Methods used for FYS registration
     *
     *Contains all methods used by /fys/register to execute DB queries and display events
     *
     */
    
    /**
     *FYS EventMethods - Methods used for FYS registration
     *
     *Contains all methods used by /fys/register to execute DB queries and display events
     *
     *@package FYS
     *
     */
    Class EventMethods {
        /**
         *__construct Constructor for Event Methods
         *
         *@param Core       $core       Core class object for DB
         *@param string[]   $settings   Array of settings
         *@param int        $userID     UserID of current user
         *@param int        $semesterID Current semesterID
         *@return void
         */
        public function __construct($core, $settings, $userID, $semesterID = null) {
            $this->core = $core;
            $this->settings = $settings;
            $this->userID = $userID;
            if(!$semesterID) {
                $this->semesterID = $this->getCurrentSemester();
            } else {
                $this->semesterID = $semesterID;
            }
        }

        /**
         *getCurrentSemester Retrieves current semester based on current datetime
         *
         *@return int ID of current semester
         */
        public function getCurrentSemester() {
            $query = 'SELECT semesterid
                        FROM FYS_Semester
                        WHERE now() BETWEEN startDateTime And endDateTime';
                        
            $semester = $this->core->executeSQL($query);
            return (count($semester) > 0) ? $semester[0]->semesterid : 0;
        }
        
        /**
         *printAlertBox Prints alert box if student/user does not exist in DB
         *
         *@return void
         */
        public function printAlertBox() {
            echo '<div class="alertbox"><h2>Unlisted Account</h2>' .
            '<p><strong>IMPORTANT!</strong> Your account is not listed in our database, which means you are unable to register for any event listed below.' .
            ' Please contact FYS Administrator at <a href="mailto:FYScocurriculars@psu.edu">FYScocurriculars@psu.edu</a> for more information.</p></div><!--alertbox-->';
        }

        /**
        *getEventInfo  Used to retrieve event information for specific events
        *
        *@param int[] $eventIDs   array of event IDs   
        *@return object event information from matched event IDs
        */
        public function getEventInfo($eventIDs) {
            $query = 'SELECT *
                      FROM FYS_Cocurricular_Events
                      WHERE EventID IN (' . $eventIDs . ')
                      ORDER BY Date';

            return $this->core->executeSQL($query);
        }

        /**
        *Retrieves all events of requested type from FYS_Cocurricular_Events table
        *
        *@param string $type Event type to match in table
        *@param string $orderBy Column name to order by
        *@return object Returns object contain all fields of matched events
        */
        public function getEvents($type, $orderBy = null) {
            
            $i=0; $arr=null;
            $arr[$i]['parameter'] = ':semesterid';
            $arr[$i]['value'] = $this->semesterID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':type';
            $arr[$i]['value'] = $type;
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            
            if(!isset($orderBy)){
                $i++;
                $arr[$i]['parameter'] = ':orderBy';
                $arr[$i]['value'] = 'Date';
                $arr[$i]['data_type'] = PDO::PARAM_STR;
            } else{
                $i++;
                $arr[$i]['parameter'] = ':orderBy';
                $arr[$i]['value'] = $orderBy;
                $arr[$i]['data_type'] = PDO::PARAM_STR;
            }

            $query = 'SELECT event.*, user.FName, user.LName
                      FROM FYS_Cocurricular_Events event
                      INNER JOIN FYS_User_Table user on event.Presenter = user.AccessID
                      WHERE semesterid = :semesterid
                      AND Type = :type 
                      AND active = 1
                      ORDER BY :orderBy';
                          
            $events = $this->core->executeSQL($query, $arr);
            return $events;
        }

        /**
        *Retrieves other events of type SCW and Academic from FYS_Cocurricular_Events table
        *
        *@return object Returns object containing all fields of matched events
        */
        public function getOtherEvents() {
            $i=0; $arr=null;
            $arr[$i]['parameter'] = ':semesterid';
            $arr[$i]['value'] = $this->semesterID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':type1';
            $arr[$i]['value'] = 'SCW';
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':type2';
            $arr[$i]['value'] = 'Academic';
            $arr[$i]['data_type'] = PDO::PARAM_STR;

            $query = 'SELECT event.*, user.FName, user.LName
                      FROM FYS_Cocurricular_Events event
                      INNER JOIN FYS_User_Table user on event.Presenter = user.AccessID
                      WHERE semesterid = :semesterid
                      AND (Type = :type1
                      OR Type = :type2)
                      AND active = 1
                      ORDER BY Date';
                      
            $events = $this->core->executeSQL($query, $arr);
            
            return $events;
        }

        /**
         *printEvents Displays cocurricular events
         *
         *@param object[] $events Array of event objects
         *@param int $userID UserID of user
         *@return void
         */
        public function printEvents($events, $userID) {
            $regEvents = null;
            $numOfEvents = count($events);

            foreach($events as $ty) {
                $arrEventTypeListing[] = $ty->Type;
            }
            $arrEventTypeListing = array_unique($arrEventTypeListing);

            # Get ids of all registered events at once, and gets extracted array of ids
            if (isset($userID)) {
                $regEvents = $this->getRegisteredEvents($userID, $arrEventTypeListing, true);
            }

            if (!empty($regEvents) && count(array_intersect($arrEventTypeListing, ['Title IX', 'Advising', 'Writing Ethics'])) ===1 ) { 
                echo '<caption><strong><div style="color: #007113; padding:5px; background: #f7de84;">Note: You can only register for one event per category (except for "Other Events").  If you want to select another event within that category, simply unregister the event that you registered.</div></strong></caption>';
            } elseif (!empty($regEvents) && count($regEvents) >= 2 && count(array_intersect($arrEventTypeListing, ['Title IX', 'Advising', 'Writing Ethics'])) === 0) {
                echo '<caption><strong><div style="color: #007113; padding:5px; background: #f7de84;">Note: You can only register for two events for "Other Events".  If you want to select another event within that category, simply unregister the event that you registered.</div></strong></caption>';
            }
            echo '<thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Location</th>
                        <th>Registered</th>
                        <th>Open</th>
                    </tr>
                </thead>
                <tbody>';

            foreach ($events as $event) {
                echo '<tr id="'. $event->EventID . '">';
                    echo '<td style="text-align:center;">';
                        if (isset($regEvents)) { //if userid is registered for any events
                            if (in_array((string) $event->EventID, $regEvents)) {
                                echo '<span style="padding: 6px 3px;font-size: 12px;" class="label label-primary">Registered</span>';
                            }
                            elseif ($this->isBeforeDeadline($event->Date, 1)) {
                                if ($this->isEventFull($event)) {
                                    echo '<span style="padding: 6px 10px;font-size: 12px;" class="label label-default">Full</span>';
                                } else {
                                    if ((empty($regEvents) && in_array($event->Type, ['Title IX', 'Advising', 'Writing Ethics'], true)) || (!in_array($event->Type, ['Title IX', 'Advising', 'Writing Ethics']) && count($regEvents) < 2) ) {
                                        echo '<a style="font-weight:bold;font-size: 12px;" class="btn btn-success" href="' . $this->core->settings['current_URL_path'] . '/register/confirm-register?EventID=' . $event->EventID . '">Register</a>';
                                    } 
                                    else {
                                        echo '<span class="btn btn-success" style="font-size:11px;">&nbsp;&nbsp;&#128274;&nbsp;&nbsp;</span>';
                                    }
                                }
                            } else {
                                echo '<span style="padding: 6px 10px;font-size: 12px;" class="label label-danger">Closed</span>';
                            }
                        }
                    echo '</td>';
                    echo '<td> <a href="javascript:void(0);" class="tooltip-text info-modal" data-toggle="tooltip" data-presenter="' . $event->FName . ' ' . $event->LName . ' (' . $event->Presenter . '@psu.edu)" title="' . $event->Description . '">' . $event->Name . '</a></td>';
                    echo '<td>' . date_format(date_create($event->Date), "m/d/Y") . '</td>';
                    echo '<td>' . ltrim($event->StartTime, "0") . ' - ' . ltrim($event->EndTime, "0") . '</td>';
                    echo '<td>' . ( (strpos($event->Location, "http") !== false && (strpos($event->Location, "password") === false) ) ? '<a href="'.$event->Location.'">Zoom Link</a>' : $event->Location) .'</td>';
                    echo '<td>' . $event->Registered . ' / ' . $event->Seats . '</td>';
                    $availableSeats = $event->Seats - $event->Registered;
                    echo '<td>' . (($availableSeats > 0) ? $availableSeats : '0') . '</td>';
                echo '</tr>';
            }
            echo '</tbody>';
        }

        
        /**
        *getRegisteredInfo  gather event information of registered events in a single query. Used
        *in pages like drop.php to only display registered events.
        *
        *@param int $userID         Unique ID associated with the Access ID, from FYS_Users_Table   
        *@return object $eventsInfo    Results from query contained event info from only registered events
        */
        public function getRegisteredInfo($userID) {
            $i = 0; $arr = null;

            $arr[$i]['parameter'] = ':userID';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'SELECT *
                     FROM (SELECT EventID
                           FROM FYS_Registration_Table
                           WHERE UserID = :userID) AS R
                     INNER JOIN FYS_Cocurricular_Events
                     USING (EventID)
                     ORDER BY Date';

            $eventsInfo = $this->core->executeSQL($query, $arr);

            return $eventsInfo;
        }

        /**
         *checkIfRegistered Verifies if user is registered for an event
         *
         *@param int $eventID EventID of event to check
         *@param int $userID  UserID of user to check
         *@return object Matching row from DB
         */
        public function checkIfRegistered($eventID, $userID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':eventID';
            $arr[$i]['value'] = $eventID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':userID';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'SELECT EventID
                      FROM FYS_Registration_Table
                      WHERE EventID = :eventID
                      AND UserID = :userID';

            $matchingRow = $this->core->executeSQL($query, $arr);

            return $matchingRow;
        }

        /**
         *matchRegisteredEvent Retrieves registeration for specified RegistrationID
         *
         *@param int $regID RegistrationID 
         *@return object Matching row from DB
         */
        public function matchRegisteredEvent($regID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':regID';
            $arr[$i]['value'] = $regID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'SELECT *
                      FROM FYS_Registration_Table
                      WHERE RegistrationID = :regID';

            $matchingRow = $this->core->executeSQL($query, $arr);

            return $matchingRow;
        }

        /**
        *getRegisteredEvents   Used to retrieve all IDs of events a user is registered for
        *
        *@param int $userID             user ID associated with access ID
        *@param int $type             type of event
        *@param bool $strip             flag to return array with no key value pairs
        *@return int[] $registeredevents  Array of matched event IDs
        */
        public function getRegisteredEvents($userID, $type, $strip=false){
            $i=0; $arr=null;
            $arr[$i]['parameter']=':userID';
            $arr[$i]['value']=$userID;
            $arr[$i]['data_type']=PDO::PARAM_INT;
            $i++;

            if(count($type) === 1) {
                $arr[$i]['parameter']=':eventType';
                $arr[$i]['value']=$type[0];
                $arr[$i]['data_type']=PDO::PARAM_STR;

                $query = 'SELECT reg.EventID
                    FROM FYS_Registration_Table reg
                    JOIN FYS_Cocurricular_Events eve ON eve.EventID = reg.EventID
                    WHERE reg.UserID = :userID
                    AND eve.Type = :eventType
                    ORDER BY reg.EventID'; 
            } else {
                $query = 'SELECT reg.EventID
                    FROM FYS_Registration_Table reg
                    JOIN FYS_Cocurricular_Events eve ON eve.EventID = reg.EventID
                    WHERE reg.UserID = :userID
                    AND eve.Type IN (\'' . implode ( "', '", $type ) . '\') 
                    ORDER BY reg.EventID'; 
            }

            $registeredEvents = $this->core->executeSQL($query, $arr);
            
            if ($strip) {
                $extractedRegisteredEvents = [];

                foreach ($registeredEvents as $event) {
                    array_push($extractedRegisteredEvents, $event->EventID);
                }
                return $extractedRegisteredEvents;
            }
            return $registeredEvents;
        }

        /**
         *printRegisteredEvents Displays registered event of user
         *
         *@param object[] $eventsInfo       Array of event objects that user is registered for
         *@param bool     $checkboxesDrop   Flag to check if checkboxes should be displayed
         *@param bool     $showsurveylink   Flag to show survey link
         *@return void
         */
        public function printRegisteredEvents($eventsInfo, $checkboxesDrop = false, $showsurveylink = false) {
            $tabsOuter = "\t\t\t\t\t";

            foreach ($eventsInfo as $event) {

                echo PHP_EOL;
                echo $tabsOuter . '<ul style="list-style-type: none;">';
                echo PHP_EOL . '<li><strong>Name: </strong>' . $event->Name . '</li>';
                echo PHP_EOL . '<li><strong>Type: </strong>' . $event->Type . '</li>';
                echo PHP_EOL . '<li><strong>Date: </strong>' . date_format(date_create($event->Date), "m/d/Y") . '</li>';
                echo PHP_EOL . '<li><strong>Time: </strong>' . ltrim($event->StartTime, "0") . ' - ' . ltrim($event->EndTime, "0") . '</li>';
                echo PHP_EOL . '<li><strong>Location: </strong>' . $event->Location . '</li>';

                if ($checkboxesDrop) {
                    if ($this->isBeforeDeadline($event->Date, 1)) {
                        echo PHP_EOL . '<li style="display: inline;"><input type="checkbox" name="SelectedEvent[]" value="' . $event->EventID . '">' .
                        '<span style="color:red;"> Drop this event?</span></li></br>';
                    } else {
                        echo PHP_EOL . '<li style="font-weight: bold">[This event cannot be dropped]</li>';
                    }
                }
                
                if ($showsurveylink) {
                    echo PHP_EOL . '<li style="display: inline;"><a style="padding:3px 6px;" class="btn btn-primary btn-xs" href="' . $this->core->settings['current_URL_path'] . '/register/survey?RegistrationID='. $event->RegistrationID . '">Take this survey</a></li>';
                }
                
                echo PHP_EOL . $tabsOuter . '</ul>';
            }
            
        }

        /**
         *registerEvent Registers user for an event
         *
         *@param int $eventID  EventID of event to register
         *@param int $userID   UserID of user to register
         *@param string $comments Pre-event comments 
         *@return bool
         */
        public function registerEvent($eventID, $userID, $comments='') {
            $i=0; $arr=null;
            $arr[$i]['parameter']=':userID';
            $arr[$i]['value']=$userID;
            $arr[$i]['data_type']=PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter']=':eventID';
            $arr[$i]['value']=$eventID;
            $arr[$i]['data_type']=PDO::PARAM_INT;
        
            if ($comments === '') {
                $query = 'INSERT INTO FYS_Registration_Table
                      (UserID, EventID)
                      VALUES (:userID, :eventID)';
            }
            else {
                $i++;
                $arr[$i]['parameter']=':comments';
                $arr[$i]['value']=$comments;
                $arr[$i]['data_type']=PDO::PARAM_STR;

                $query = 'INSERT INTO FYS_Registration_Table
                      (UserID, EventID, PreEventComments)
                      VALUES (:userID, :eventID, :comments)';
            }

            if ($this->core->executeSQL($query, $arr) > 0) {
                if ($this->updateRegistered($eventID)) {
                    return true;
                }
            }
            return false;
        }

        /**
        *dropEvent  Drops provided event from registration table for provided user ID
        *
        *@param int $eventIDs   event ID to be dropped
        *@param int $userID    user ID associated with access ID
        *@return bool
        */
        public function dropEvents($eventIDs, $userID) {
            $i=0;$arr=null;
            $arr[$i]['parameter']=':userID';
            $arr[$i]['value']=$userID;
            $arr[$i]['data_type']=PDO::PARAM_INT;

            if (!empty($eventIDs)) {
                $query = 'DELETE
                          FROM FYS_Registration_Table
                          WHERE UserID = :userID
                          AND EventID IN (' . $eventIDs . ')';
                
                if ($this->core->executeSQL($query, $arr) > 0) {
                    if ($this->updateRegistered($eventIDs, false)) {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         *updateRegistered Updates registered status for given eventIDs
         *
         *@param int[]  $eventIDs   Array of eventIDs
         *@param bool   $add        Add or drop the registration?
         *@return bool
         */
        public function updateRegistered($eventIDs, $add = true) {
            if (!empty($eventIDs)) {
                if ($add==true) {
                    $query = 'UPDATE
                              FYS_Cocurricular_Events
                              SET Registered = (Registered + 1)
                              WHERE EventID IN (' . $eventIDs . ')';
                } elseif ($add==false) {
                    $query = 'UPDATE
                              FYS_Cocurricular_Events
                              SET Registered = (Registered - 1)
                              WHERE EventID IN (' . $eventIDs . ')';
                }
                
                if ($this->core->executeSQL($query) > 0) {
                    return true;
                }
            }
            return false;
        }

        /**
         *getAvailableSurveys Fetches availble surveys for given user
         *
         *@param int $userID UserID of user
         *@return object[] Availble surveys for events
         */
        public function getAvailableSurveys($userID){
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':userID';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            
            $query = "SELECT *
                      FROM (SELECT EventID, RegistrationID
                            FROM FYS_Registration_Table
                            WHERE UserID = :userID
                            AND IsAttended = 'Y'
                            AND SurveyRating IS NULL
                            ) AS R
                      INNER JOIN FYS_Cocurricular_Events
                      USING (EventID)
                      ORDER BY Date";
            $eventsInfo = $this->core->executeSQL($query, $arr);
            
            return $eventsInfo;
        }

        /**
         *checkSurvey Verifies if survey is already completed
         *
         *@param int $regID RegistrationID of event and user
         *@return bool
         */
        public function checkSurvey($regID) {
            $i=0; $arr=null;
            $arr[$i]['parameter']=':regID';
            $arr[$i]['value']=$regID;
            $arr[$i]['data_type']=PDO::PARAM_INT;

            $query = 'SELECT IF (EXISTS (SELECT * FROM FYS_Registration_Table WHERE RegistrationID = :regID AND SurveyRating IS NOT NULL), 1, 0) AS query';
            if (intval($this->core->executeSQL($query, $arr)[0]->query)) {
                return true;
            }
            return false;
        }

        /**
         *updateSurvey Updates survey
         *
         *@param int $regID     RegistrationID of registration
         *@param int $rating    Rating of event
         *@param string $paragraph Comments regarding event
         *@return bool
         */
        public function updateSurvey($regID, $rating, $paragraph) {
            $i=0;$arr=null;
            $arr[$i]['parameter']=':regID';
            $arr[$i]['value']=$regID;
            $arr[$i]['data_type']=PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter']=':rating';
            $arr[$i]['value']=$rating;
            $arr[$i]['data_type']=PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter']=':paragraph';
            $arr[$i]['value']=$paragraph;
            $arr[$i]['data_type']=PDO::PARAM_STR;

            $query = 'UPDATE FYS_Registration_Table
                      SET SurveyRating = :rating,
                      SurveyParagraph = :paragraph
                      WHERE RegistrationID = :regID';

            if ($this->core->executeSQL($query, $arr) > 0) {
                return true;
            }
            return false;
        }

        /**
         *isBeforeDeadline Checks if date is away from x days
         *
         *@param string  $eventDate Date of event
         *@param int     $daysAway  Number of days away to check
         *@return bool
         */
        public function isBeforeDeadline($eventDate, $daysAway) {
            $now = new DateTime(date('Y-m-d'));

            $diff = (int) date_diff(date_create($now->format('Y-m-d H:i:s') . ' + ' . $daysAway . ' weekdays'), date_create($eventDate . ' 00:00:00.000000'))->format('%r%a');

            //echo $diff;
            if (isset($diff) && $diff >= $daysAway) {
                return true;
            }
            return false;
        }
        
        /**
        *isEventActive Check if event is active
        *
        *@param object $eventInfo Event object
        *@return bool
        */
        public function isEventActive($eventInfo) {
            return ($eventInfo->active);
        }
        
        /**
         *isEventFull Check if event is full
         *
         *@param object $eventInfo Event object
         *@return bool
         */
        public function isEventFull($eventInfo) {
            if (($eventInfo->Seats - $eventInfo->Registered) <= 0) {
                return true;
            }
            return false;
        }

        /**
         *Handles post data from submitted drop forms. Will redirect back to confirm-register
         *with a query string corresponding to outcome 
         *========================================
         *@param   string[] $post  $_POST, passed from the local fusebox file within /includes/register
         *@return void
         */
        public function redirectDrop($post) {
            if (isset($post['DropIDs']) && isset($post['Submit'])) {
                $ids = explode(",", $post['DropIDs']);

                foreach ($ids as $id) {
                    if (!$this->checkIfRegistered($id, $this->userID)) {
                        header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-drop?results=invalid");
                        exit;
                    }
                }
                if ($this->dropEvents($post['DropIDs'], $this->userID)) {
                    header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-drop?results=success"
                    . '&EventIDs=' . $post['DropIDs']);
                    exit;
                }
                exit;
                header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-drop?results=failure");
                exit;
            }
            header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-drop?results=error");
            exit;
        }
        
        /**
         *Handles post data from submitted confirm-register forms. Will redirect back to confirm-register
         *with a query string corresponding to outcome of the numerous status and condition checking methods
         *contained in this methods logic.
         *
         *@param   string[] $post  $_POST, passed from the local fusebox file within /includes/register
         *@return void
         */
        public function redirectRegister($post) {
            if (isset($post['EventID'])) {
                $eventInfo = $this->getEventInfo($post['EventID']);
                // Clean up these URL, remove Event and UserID
                if (isset($post['Submit']) || isset($post['SubmitComments'])) {
                    if (count($this->checkIfRegistered($post['EventID'], $this->userID)) != 0) {
                        header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-register?EventID="
                        . $post['EventID'] . "&results=duplicate");
                        exit;
                    }
                    if ($this->isEventFull($eventInfo[0])) {
                        header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-register?EventID="
                        . $post['EventID'] . "&results=full");
                        exit;   
                    }

                    $beforeDeadline = $this->isBeforeDeadline($eventInfo[0]->Date, 1);

                    if ($beforeDeadline) {
                        if (isset($post['Submit'])) {
                            if ($this->registerEvent($post['EventID'], $this->userID)) {
                                header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-register?EventID="
                                . $post['EventID'] . "&results=success");
                                exit;
                            }
                            header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-register?EventID="
                            . $post['EventID'] . "&results=failure");
                            exit;
                        }
                        if (isset($post['Comments'])) {
                            if (strlen($post['Comments']) > 1000 && $this->registerEvent($post['EventID'], $this->userID,
                                substr($post['Comments'], 0, 1000))) {
                                header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-register?EventID="
                                . $post['EventID'] . "&results=success");
                                exit;       
                            }
                            elseif ($this->registerEvent($post['EventID'], $this->userID, $post['Comments'])) {
                                header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-register?EventID="
                                . $post['EventID'] . "&results=success");
                                exit;
                            }
                            header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-register?EventID="
                            . $post['EventID'] . "&results=failure");
                            exit;
                        }
                    }
                    elseif (!$beforeDeadline) {
                        header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-register?EventID="
                        . $post['EventID'] . "&results=deadline");
                        exit;
                    }
                }
            }
            header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-register?EventID="
            . $post['EventID'] . "&results=error");
            exit;
        }

        /**
         *Handles post data from submitted survey forms. Will redirect to confirm-survey with a query string
         *corresponding to the result of updateSurvey method;
         *
         *@param string[] $post  $_POST, passed from the local fusebox file within /includes/register
         *@return void
         */
        public function redirectSurvey($post) {
            if (isset($post['Submit']) && isset($post['RegistrationID']) && isset($post['SurveyRating'])) {
                $regID = $post['RegistrationID'];
                $surveyRating = intval($post['SurveyRating']);
                if (isset($post['SurveyParagraph'])) {
                    $surveyParagraph = $post['SurveyParagraph'];
                }   
                else {
                    $surveyParagraph = '';
                }

                if ($this->updateSurvey($regID, $surveyRating, $surveyParagraph)) {
                    header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-survey?results=success");
                    exit;   
                }
            }
            header("Location: " . $this->settings['current_URL_path'] . "/register/confirm-survey?results=failure");
            exit;
        }
    }

?>