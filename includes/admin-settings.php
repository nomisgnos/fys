<?php
/**
 *Methods for Admin Settings
 *
 *Contains all methods used to update admin settings
 *
 */
 
/**
 *Methods for Admin Settings
 *
 *Contains all methods used to update admin settings
 *
 *@package FYS
 */
Class AdminSettings {
    
    /**
     *__construct Constructor for AdminSettings
     *
     *@param AdminMethods $adm Class object of AdminMethods
     *@return void
     */
    public function __construct($adm) {
        $this->adm = $adm;
    }
    
}
?>