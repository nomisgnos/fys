<?php
    /**
     *FYS Admin Methods
     *
     *Contains all the admin methods used by /fys/admin
     *to execute DB query, print navigation, and other
     *utility functions
     *
     *@package FYS
     */
     
    /**
     *FYS Admin Methods
     *
     *Contains all the admin methods used by /fys/admin
     *to execute DB query, print navigation, and other
     *utility functions
     *
     */
    Class AdminMethods {
        /**
         *  __construct AdminMethods constructor
         *  
         *@param Core $core     Database class
         *@param string[] $settings Settings array
         *@param integer $userID   UserID of current user
         *@return void
         *  
         */
        public function __construct($core, $settings, $userID) {
            $this->core = $core;
            $this->settings = $settings;
            $this->userID = $userID;
        }

        /**
        * canUserAccess     Checks if the role id of the user is appropriate for the current page
        *  
        *@param $permittedIDs   Array of permitted role ids
        *@param $roleIDs      Array of role ids of current user, default value of -1 used to trigger roleID lookup
        *@return  bool response indicating if user should be able to view page content
        */
        public function canUserAccess($permittedIDs, $roleIDs = -1) {
            return $this->core->canUserAccess($permittedIDs, $roleIDs);
        }
        
        /**
         *getCurrentSemester Calls SemesterMethods getCurrentSemester
         *
         *@return int ID of semester
         */
        public function getCurrentSemester() {
            include_once 'semester.php';
            $sem = new SemesterMethods($this);
            return $sem->getCurrentSemester();
        }
        
        /**
         *  printRoleNames Prints role names of user in navbar
         *  
         *@param int[] $roleID Array of user roleIDs
         *@return void
         */
        public function printRoleNames($roleID) {
            $roleNames = '';
            foreach($roleID as $role) {
                switch($role) {
                    case 1:
                        $roleNames .= 'Admin ';
                        break;
                    case 2:
                        $roleNames .= 'Instructor ';
                        break;
                    case 3:
                        $roleNames .= 'Instructor Assistant ';
                        break;
                    case 4:
                        $roleNames .= 'Presenter ';
                        break;
                    case 5:
                        $roleNames .= 'Student ';
                        break;
                }
            }
            echo $roleNames;
        }
        /**
         *  newNavItem Prints a new nav item to the navbar
         *  
         *@param string $title Title of the navbar icon
         *@param string $icon  Font-awesome icon classname
         *@param string $url   URL path of the nav item
         *@return void
         */
        private function newNavItem($title, $icon, $url) {
        ?>
            <div class="btn-group btn-group-nav">
                <a id="nav-<?=$title?>" href="<?=$this->settings['current_URL_path']?><?=$url?>" class="btn btn-nav">
                    <span class='<?=$icon?>'></span>
                    <h4><?=$title?></h4>
                </a>
            </div>
        <?php
        }
        
        /**
         *  printNewNavigation Prints navigation 
         *  
         *@param int[] $roleID Array of user roleIDs
         *@return void
         */
        public function printNewNavigation($roleID) {
            $this->newNavItem("Dashboard", "fa fa-dashboard", "/admin");
            $this->newNavItem("Events", "fa fa-calendar", "/admin/events");
            if (in_array(1, $roleID)) {
                # 1 2 3 4       Events
                # 1             Students
                # 1             Users
                # 1 2 3         Courses
                # 1             Reports
                # 1             Imports
                
                $this->newNavItem("Students", "fa fa-user-circle", "/admin/students");
                $this->newNavItem("Users", "fa fa-users", "/admin/users");
                $this->newNavItem("Courses", "fa fa-list", "/admin/courses");
                $this->newNavItem("Reports", "fa fa-file-text-o", "/admin/reports");
                $this->newNavItem("Import", "fa fa-cloud-upload", "/admin/import");
            } elseif (array_intersect(array(2,3), $roleID)) {
                $this->newNavItem("Courses", "fa fa-list", "/admin/courses");
            }
        }
        
        /**
        * getAdminEvents        Used to grab event information depending on the given semester
        * 
        *@param  $semester      When not defaulting to null, used by sql query to filter events
        *@param  $order         Order by given column
        *@return object[]        Event information returned by sql query
        */
        public function getAdminEvents($semester = '0', $order = "EventID"){
            $i = 0; $arr = null;
            $arr[$i]['parameter']=':semesterID';
            $arr[$i]['value']= $semester;
            $arr[$i]['data_type']=PDO::PARAM_INT;
            
            if ($semester != '0'){
                $query = 'SELECT events.*, presenterT.FName as PresenterFName, presenterT.LName as PresenterLName,
                            copresT1.FName as CoPres1FName, copresT2.LName as CoPres1LName,
                            copresT2.FName as CoPres2FName, copresT2.LName as CoPres2LName
                          FROM FYS_Cocurricular_Events events
                          LEFT JOIN FYS_User_Table presenterT ON presenterT.AccessID = events.Presenter AND presenterT.AccessID <> ""
                          LEFT JOIN FYS_User_Table copresT1 ON copresT1.AccessID = events.CoPres1 AND copresT1.AccessID <> ""
                          LEFT JOIN FYS_User_Table copresT2 ON copresT2.AccessID = events.CoPres2 AND copresT2.AccessID <> ""
                          WHERE semesterid = :semesterID
                          ORDER BY ' . $order;
                $events = $this->core->executeSQL($query, $arr);
            } else {
                $query = 'SELECT events.*, presenterT.FName as PresenterFName, presenterT.LName as PresenterLName,
                            copresT1.FName as CoPres1FName, copresT2.LName as CoPres1LName,
                            copresT2.FName as CoPres2FName, copresT2.LName as CoPres2LName
                          FROM FYS_Cocurricular_Events events 
                          LEFT JOIN FYS_User_Table presenterT ON presenterT.AccessID = events.Presenter AND presenterT.AccessID <> ""
                          LEFT JOIN FYS_User_Table copresT1 ON copresT1.AccessID = events.CoPres1 AND copresT1.AccessID <> ""
                          LEFT JOIN FYS_User_Table copresT2 ON copresT2.AccessID = events.CoPres2 AND copresT2.AccessID <> ""
                          ORDER BY ' . $order;
                $events = $this->core->executeSQL($query);
            }
            return $events;
        }

        /**
         *  getMyEvents Fetches presenter's events based on accessID
         *  
         *@param string $accessID AccessID of the presenter
         *@return object[] Database object containing list of presenter's events
         */
        public function getMyEvents($accessID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter']=':accessID';
            $arr[$i]['value']= $accessID;
            $arr[$i]['data_type']=PDO::PARAM_STR;
            $query = 'SELECT *
                FROM FYS_Cocurricular_Events 
                WHERE LOWER(Presenter) = :accessID OR LOWER(CoPres1) = :accessID OR LOWER(CoPres2) = :accessID
                ORDER BY Date asc';
            $events = $this->core->executeSQL($query, $arr);
            return $events;
        }
        
        /**
         *  getUpcomingEvents Fetches upcoming cocurricular events
         *  
         *@param int $amount Number of upcoming events to fetch
         *@return object[] Database object containing list of upcoming events
         */
        public function getUpcomingEvents($amount=0) {
            $query = 'SELECT *
                      FROM FYS_Cocurricular_Events
                      WHERE Date >= CURDATE()
                      ORDER BY Date asc';
                      
            $query .= (!$amount) ? '' : ' LIMIT ' . $amount;
            
            $events = $this->core->executeSQL($query);
            return $events;
        }
        
        public function getEventRegStatsByID($eventID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter']=':eventid';
            $arr[$i]['value']= $eventID;
            $arr[$i]['data_type']=PDO::PARAM_INT;
            
            $query = 'SELECT Type, Count(*) as registrations, SUM(CASE WHEN IsAttended="Y" THEN 1 ELSE 0 END) as sumAttended, SUM(CASE WHEN IsAttended="N" THEN 1 ELSE 0 END) as sumMissed, SUM(CASE WHEN IsAttended IS NULL THEN 1 ELSE 0 END) as sumHold
                            FROM FYS_Cocurricular_Events
                            INNER JOIN FYS_Registration_Table USING(EventID) WHERE EventID=:eventid';
            
            $stats = $this->core->executeSQL($query, $arr);
            return $stats;
        }
        
        public function getEventRegistrationStats() {
            $query = 'SELECT "SUM" as Type, COUNT(*) as registrations, NULL as sumAttended, NULL as sumMissed, NULL as sumHold 
                        FROM FYS_Cocurricular_Events  
                        INNER JOIN FYS_Registration_Table USING(EventID)
                        WHERE semesterid = ' . $this->getCurrentSemester() . '
                        UNION ALL
                        SELECT Type, Count(*) as registrations, SUM(CASE WHEN IsAttended="Y" THEN 1 ELSE 0 END) as sumAttended, SUM(CASE WHEN IsAttended="N" THEN 1 ELSE 0 END) as sumMissed, SUM(CASE WHEN IsAttended IS NULL THEN 1 ELSE 0 END) as sumHold
                            FROM FYS_Cocurricular_Events
                            INNER JOIN FYS_Registration_Table USING(EventID) WHERE semesterid = ' . $this->getCurrentSemester() .'  GROUP BY Type';

            $results = $this->core->executeSQL($query);
            return $results;
        }
        
        
        public function getEventBreakdown() {
            $query = 'SELECT "SUM" Type, Count(*) as numEvents FROM FYS_Cocurricular_Events WHERE semesterid = ' . $this->getCurrentSemester() .' UNION ALL SELECT Type, COUNT(*) FROM `FYS_Cocurricular_Events` WHERE semesterid = ' . $this->getCurrentSemester() . ' GROUP BY Type';
            $totalEvents = $this->core->executeSQL($query);
            return $totalEvents;
        }
        
        /**
         *  getStudents Fetches all students in order by last name, first name
         *  
         *@return null|object[] Database object containing list of students
         */
        public function getStudents($build = false) {         
            $query = 'SELECT * 
                      FROM FYS_User_Table usr
                      INNER JOIN FYS_User_Roles_XREF rxref USING(UserID)
                      WHERE rxref.User_Role_ID = 5
                      ORDER by usr.LName, usr.FName';
                      
            $students = $this->core->executeSQL($query);
            
            if ($build) {
                return $this->buildStudents($students);
            }
            
            return (count($students) > 0) ? $students : null;
        }

        /**
        * buildStudents       Used to build html from provided list of students
        * 
        *@param $students     Array of students containing UserID and custom label attributes
        *@return string        Contains select tags constructed from contents of $students
        */
        public function buildStudents($students) {
            $output = '';

            if (isset($students)) {
                foreach ($students as $student) {
                    $output = $output . '<option value="' . $student->UserID . '">'
                    . $student->FName . ' ' . $student->LName . ' (' . $student->AccessID . ')</option>' . PHP_EOL;
                }
            }
            return $output;
        }
        
        /**
         *  addStudentCourse Inserts student courses into database
         *  
         *@param int $userID   UserID of student
         *@param int $course   CourseID of course
         *@param bool $imported Called from import tool or not
         *@return void
         */
        public function addStudentCourse($userID, $course, $imported = false) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':userid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':course';
            $arr[$i]['value'] = $course;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            
            if($imported) {
                $query = "INSERT INTO FYS_User_Courses_XREF (UserID, CourseID, importedOn) VALUES (:userid, :course, now())";
            } else {
                $query = "INSERT INTO FYS_User_Courses_XREF (UserID, CourseID) VALUES (:userid, :course)";
            }
            
            $this->core->executeSQL($query, $arr, 'all', 1, true, true);
        }
        
        /**
         *  getStudentCourses Fetches student courses
         *  
         *@param int $studentID UserID of student
         *@return int|object Database object containing student's courses
         */
        public function getStudentCourses($studentID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':studentID';
            $arr[$i]['value'] = $studentID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'SELECT DISTINCT CourseID
                        FROM FYS_User_Courses_XREF 
                        WHERE UserID = :studentID';

            $results = $this->core->executeSQL($query, $arr);

            return (count($results) > 0) ? $results : 0;
        }
        
        /**
         *  getStudentEvents Fetches student's registered events and those event's info
         *  
         *@param int $studentID UserID of student
         *@return int|object DB object containing list of events students signed up for
         */
        public function getStudentEvents($studentID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':studentID';
            $arr[$i]['value'] = $studentID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'SELECT usr.UserID, reg.EventID, reg.isAttended, event.Name, event.Type, event.Date, event.StartTime, event.EndTime
                        FROM FYS_User_Table usr
                        INNER JOIN FYS_Registration_Table reg
                        USING (UserID)
                        INNER JOIN FYS_Cocurricular_Events event
                            on reg.EventID = event.EventID
                        WHERE usr.UserID = :studentID
                        ORDER BY event.Date ASC';

            $results = $this->core->executeSQL($query, $arr);
            
            return (count($results) > 0) ? $results : 0;
        }
        /**
         *  getStudentFromID Fetches student from UserID
         *  
         *@param int $id UserID of student
         *@return null|object DB object of student (*)
         */
        public function getStudentFromID($id) { 
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':uid';
            $arr[$i]['value'] = $id;
            $arr[$i]['data_type'] = PDO::PARAM_INT;     
            $query = 'SELECT * 
                      FROM FYS_User_Table usr
                      INNER JOIN FYS_User_Roles_XREF rxref USING(UserID)
                      WHERE rxref.User_Role_ID = 5 AND usr.UserID = :uid
                      ORDER by usr.LName, usr.FName';
            
            $students = $this->core->executeSQL($query, $arr);
            return (count($students) > 0) ? $students : null;
        }
        
        /**
         *  getAllUsers Fetches all users from DB
         *  
         *@return null|object[] DB objects of all users (AccessID, Email, FName, LName)
         */
        public function getAllUsers() {
            $query = 'SELECT  AccessID, Email, FName, LName
                      FROM FYS_User_Table';

            $users = $this->core->executeSQL($query);
            return (count($users) > 0) ? $users : null;
        }
        
        /**
         *  getUsers Fetches all users who are not student
         *  
         *@return null|object[] DB objects of all users (*)
         */
        public function getUsers() {
            $query = 'SELECT * 
                      FROM FYS_User_Table
                      INNER JOIN FYS_User_Roles_XREF rxref USING(UserID)
                      INNER JOIN FYS_User_Roles USING(User_Role_ID)
                      WHERE rxref.User_Role_ID != 5
                      ORDER by rxref.User_Role_ID';

            $users = $this->core->executeSQL($query);
            return (count($users) > 0) ? $users : null;
        }
        
        /**
         *  addCourse Add a new course to DB
         *  
         *@param string[] $post         Post form request array of all parameters
         *@param bool $imported         Called from import tool or not
         *@return null|object DB result of new course
         */
        public function addCourse($post, $imported = false) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':name';
            $arr[$i]['value'] = $post['Name'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':number';
            $arr[$i]['value'] = $post['Number'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':section';
            $arr[$i]['value'] = $post['Section'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':instructor';
            $arr[$i]['value'] = $post['Instructor'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':instremail';
            $arr[$i]['value'] = $post['InstrEmail'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':mentoremail';
            $arr[$i]['value'] = $post['MentorEmail'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':meetingtimes';
            $arr[$i]['value'] = $post['MeetingTimes'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':instructorid';
            $arr[$i]['value'] = $post['InstructorID'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':mentorid';
            $arr[$i]['value'] = $post['MentorID'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':schedulenumber';
            $arr[$i]['value'] = $post['ScheduleNumber'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;

            if($imported) {
                $query = 'INSERT INTO FYS_Course_Table
                  (Name, Number, Section, Instructor, InstrEmail, MentorEmail, MeetingTimes,
                  InstructorID, MentorID, ScheduleNumber, importedOn)
                  VALUES (:name, :number, :section, :instructor, :instremail, :mentoremail, :meetingtimes,
                  :instructorid, :mentorid, :schedulenumber, now())';
            } else {
                $query = 'INSERT INTO FYS_Course_Table
                          (Name, Number, Section, Instructor, InstrEmail, MentorEmail, MeetingTimes,
                          InstructorID, MentorID, ScheduleNumber)
                          VALUES (:name, :number, :section, :instructor, :instremail, :mentoremail, :meetingtimes,
                          :instructorid, :mentorid, :schedulenumber)';
            }
            
            return $this->core->executeSQL($query, $arr, 'all', 1, false, true);
        }
        
        
        /**
         *  editCourse Edit a course in DB
         *  
         *@param mixed[]     $post Post form request array of all parameters
         *@param bool $imported Called from import tool or not
         *@return bool
         *  
         */
        public function editCourse($post, $imported = false) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':courseid';
            $arr[$i]['value'] = $post['CourseID'];
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':name';
            $arr[$i]['value'] = $post['Name'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':number';
            $arr[$i]['value'] = $post['Number'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':section';
            $arr[$i]['value'] = $post['Section'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':instructor';
            $arr[$i]['value'] = $post['Instructor'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':instremail';
            $arr[$i]['value'] = $post['InstrEmail'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':mentoremail';
            $arr[$i]['value'] = $post['MentorEmail'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':meetingtimes';
            $arr[$i]['value'] = $post['MeetingTimes'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':instructorid';
            $arr[$i]['value'] = $post['InstructorID'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':mentorid';
            $arr[$i]['value'] = $post['MentorID'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':schedulenumber';
            $arr[$i]['value'] = $post['ScheduleNumber'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;

            if($imported) {
                $query = 'UPDATE FYS_Course_Table
                          SET Name = :name, Number = :number, Section = :section, Instructor = :instructor, InstrEmail = :instremail, MentorEmail = :mentoremail, MeetingTimes = :meetingtimes,
                          InstructorID = :instructorid, MentorID = :mentorid, ScheduleNumber = :schedulenumber, importedOn = now()
                          WHERE CourseID = :courseid';
            } else {
                $query = 'UPDATE FYS_Course_Table
                          SET Name = :name, Number = :number, Section = :section, Instructor = :instructor, InstrEmail = :instremail, MentorEmail = :mentoremail, MeetingTimes = :meetingtimes,
                          InstructorID = :instructorid, MentorID = :mentorid, ScheduleNumber = :schedulenumber
                          WHERE CourseID = :courseid';
            }

            $results = $this->core->executeSQL($query, $arr, 'all', 1, true, true);

            if ($results != 0) {
                return true;
            }

            return false;
        }
        
        /**
         *  deleteCourse Delete course from DB
         *  
         *@param int $courseID CourseID
         *@return int Result of delete
         */
        public function deleteCourse($courseID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':courseid';
            $arr[$i]['value'] = $courseID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'DELETE FROM FYS_Course_Table
                      WHERE CourseID = :courseid';

            return $this->core->executeSQL($query, $arr);
        }
        
        /**
         *  getMyCourses Fetches courses of instructor
         *  
         *@param string $accessID Instructor's AccessID
         *@return object DB object of instructor's courses (*)
         */
        public function getMyCourses($accessID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter']=':accessID';
            $arr[$i]['value']= $accessID;
            $arr[$i]['data_type']=PDO::PARAM_STR;
            $query = 'SELECT *
                FROM FYS_Course_Table 
                WHERE LOWER(InstructorID) = :accessID OR LOWER(MentorID) = :accessID
                ORDER BY Name, Number, cast(Section AS UNSIGNED)';
            $events = $this->core->executeSQL($query, $arr);
            return $events;
        }
        
        /**
         *  getCourses Fetches all courses
         *  
         *@return null|object DB object of all courses (*)
         */
        public function getCourses() {
            $query = 'SELECT *
                      FROM FYS_Course_Table
                      ORDER BY Name, Number, cast(Section AS UNSIGNED)';
            $courses = $this->core->executeSQL($query);
            return (count($courses) > 0) ? $courses : null;
        }
        
        /**
         *  getUserRoles Fetches user roles of user from FYS_User_Roles_XREF
         *  
         *@param int $userID User's UserID
         *@return null|object DB object containing user roleIDs
         */
        public function getUserRoles($userID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':uid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;     
            $query = 'SELECT User_Role_ID
                      FROM FYS_User_Roles_XREF
                      WHERE UserID = :uid
                      ORDER BY User_Role_ID';
            $results = $this->core->executeSQL($query, $arr, 'all', 1, true, true);
            return (count($results) > 0) ? $results : null;
        }
        
        /**
         *  getRoles Fetches all possible roles
         *  
         *@return null|object DB object containing all possible roles (*)
         */
        public function getRoles() {
            $query = 'SELECT *
                      FROM FYS_User_Roles
                      ORDER BY User_Role_ID';
            $results = $this->core->executeSQL($query);
            return (count($results) > 0) ? $results : null;
        }
        
        public function getRegistrations($sem=0) {
            $query = 'SELECT reg.*, evnt.semesterid
                      FROM FYS_Registration_Table reg INNER JOIN FYS_Cocurricular_Events evnt USING(EventID)';
            
            if($sem) {
                $i = 0; $arr = null;
                $arr[$i]['parameter'] = ':semid';
                $arr[$i]['value'] = $sem;
                $arr[$i]['data_type'] = PDO::PARAM_INT;  
                $query .= " WHERE semesterid=:semid";
                
                $results = $this->core->executeSQL($query, $arr);
                return (count($results) > 0) ? $results : null;
            }
            
            $results = $this->core->executeSQL($query);
            return (count($results) > 0) ? $results : null;
        }
        
        /**
         *  getEvents Fetches all cocurricular events
         *  
         *@return null|object DB object containing all cocurricular events
         */
        public function getEvents($sem=0) {
            $query = 'SELECT *
                      FROM FYS_Cocurricular_Events';
            
            if($sem) {
                $i = 0; $arr = null;
                $arr[$i]['parameter'] = ':semid';
                $arr[$i]['value'] = $sem;
                $arr[$i]['data_type'] = PDO::PARAM_INT;  
                $query .= " WHERE semesterid=:semid";
                $results = $this->core->executeSQL($query, $arr);
                return (count($results) > 0) ? $results : null;
            }
            
            $query .= " ORDER BY Type, Date DESC";
            
            $results = $this->core->executeSQL($query);
            return (count($results) > 0) ? $results : null;
        }
        
        /**
         *  getInstructors Fetches all instructors from course table grouped by InstructorID
         *  
         *@return null|object DB object containing all instructors (InstructorID, Instructor)
         */
        public function getInstructors() {
            $query = 'SELECT InstructorID, Instructor
                      FROM FYS_Course_Table
                      GROUP BY InstructorID
                      ORDER BY Instructor';
            $results = $this->core->executeSQL($query);
            return (count($results) > 0) ? $results : null;
        }
        
        /**
         *getInstructorsUsers Get instructors defined in the DB as users
         *
         *@return null|object DB object containing all instructors and assistants
         */
        public function getInstructorsUsers() {
            $query = 'SELECT FName, LName, UserID, AccessID
                      FROM FYS_User_Table
                      INNER JOIN FYS_User_Roles_XREF rxref USING(UserID)
                      WHERE rxref.User_Role_ID = 2 OR rxref.User_Role_ID = 3
                      ORDER BY LName';
                      
            $results = $this->core->executeSQL($query);
            return (count($results) > 0) ? $results : null;
        }
        
        /**
         *  getAssistants Fetches all instructor's assistants from FYS_User_Table
         *  
         *@return null|object DB object containing all instructor's assistants
         */
        public function getAssistants() {
            $query = 'SELECT FName, LName, UserID, AccessID
                      FROM FYS_User_Table
                      INNER JOIN FYS_User_Roles_XREF rxref USING(UserID)
                      WHERE rxref.User_Role_ID = 3
                      ORDER BY LName';
            $results = $this->core->executeSQL($query);
            return (count($results) > 0) ? $results : null;
        }
        
        /**
        * getPresenters         Retrieves array of presenters (users with UserRole 4 or 6)
        * 
        *@param  bool  $build           Flag used to trigger call to buildPresenters over returning raw array
        *@return null|string|object[]   Output depends on $build, returns string output of buildPresenters(),
        *                               directly returns array, or returns null if array is empty, so isset can
        *                               called to check results.
        */
        public function getPresenters($build = true) {
            $query = 'SELECT AccessID, Email, CONCAT(LName, \', \', FName, \' (\', AccessID, \')\') as label
                      FROM FYS_User_Table
                      INNER JOIN FYS_User_Roles_XREF rxref USING(UserID)
                      WHERE rxref.User_Role_ID = 4 OR rxref.User_Role_ID = 6
                      ORDER BY label';

            $presenters = $this->core->executeSQL($query);

            if ($build) {
                return $this->buildPresenters($presenters);
            }
            else {
                if (count($presenters) > 0) {
                    return $presenters;
                }   
            }

            return null;
        }

        /**
        * buildPresenters       Used to build html from provided list of presenters
        * 
        *@param $presenters     Array of presenters containing AccessID and custom label attributes
        *@return string        Contains select tags constructed from contents of $presenters
        */
        public function buildPresenters($presenters) {
            $output = '<option value=""> &ltNO PRESENTER&gt </option>'; 
            if (isset($presenters)) {
                foreach ($presenters as $presenter) {
                    $output = $output . '<option value="' . $presenter->AccessID . '">'
                    . $presenter->label . '</option>' . PHP_EOL;
                }
            }
            return $output;
        }

        /**
         *  setSelectedPresenter Used to set selected presenter for edit event
         *  
         *@param object $presenters               Object of all presenters
         *@param object $selectedPresenter        Object of current presenter
         *@return string Built HTML code for options with selected or without selected
         */
        public function setSelectedPresenter($presenters, $selectedPresenter) {
            $search = '<option value="' . $selectedPresenter . '">';
            $replace = '<option value="' . $selectedPresenter . '" selected>';

            return str_replace($search, $replace, $presenters);
        }

        /**
         *  getCourseStudents Fetches course students based on $courseID
         *  
         *@param int $courseID ID of course in DB
         *@return int|object[] DB object containing course info and all students in the course
         */
        public function getCourseStudents($courseID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':courseID';
            $arr[$i]['value'] = $courseID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            
            $query = 'SELECT DISTINCT usr.FName, usr.LName, usr.UserID, usr.AccessID, course.*
                      FROM FYS_Course_Table course
                      INNER JOIN FYS_User_Courses_XREF uxref
                      ON uxref.CourseID = course.CourseID
                      INNER JOIN FYS_User_Table usr
                      USING (UserID)
                      WHERE course.CourseID = :courseID
                      ORDER BY usr.LName ASC, usr.FName ASC';

            $results = $this->core->executeSQL($query, $arr);

            if (count($results) > 0) {
                return $results;
            }

            return 0;
        }
        
        /**
         *  getEventStudents Fetches all students registered for an event
         *  
         *@param int $eventID ID of event stored in DB
         *@return int|object[] DB object containing user and registration information for the event
         */
        public function getEventStudents($eventID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':eventID';
            $arr[$i]['value'] = $eventID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'SELECT usr.UserID, usr.FName, usr.LName, usr.Email, reg.EventID, reg.RegistrationID, reg.IsAttended
                      FROM FYS_User_Table usr
                      INNER JOIN FYS_Registration_Table reg
                      USING (UserID)
                      WHERE reg.EventID = :eventID
                      ORDER BY usr.LName ASC, usr.FName ASC';

            $results = $this->core->executeSQL($query, $arr);

            if (count($results) > 0) {
                return $results;
            }

            return 0;
        }
        
        /**
         *  getUserID Fetches UserID from accessID
         *  
         *@param string $accessID AccessID of user
         *@return null|int UserID of user based on their accessID
         */
        public function getUserID($accessID) {
            if (isset($accessID)) {
                $i=0;$arr=null;
                $arr[$i]['parameter'] = ':remote_user';
                $arr[$i]['value'] = $accessID;
                $arr[$i]['data_type'] = PDO::PARAM_STR;

                $query = 'SELECT UserID
                          FROM FYS_User_Table
                          WHERE LOWER(AccessID) = LOWER(:remote_user)';
                          
                $userID = $this->core->executeSQL($query, $arr);
                if (isset($userID[0])) {
                    return $userID[0]->UserID;
                }
            }
            
            return null;
        }
        
        /**
         *  checkStudentEventRegistration Checks if a student is registered for an event
         *  
         *@param int $userID   UserID of student
         *@param int $eventID EventID of event
         *@return bool Returns true if student is found registered in the event or false
         */
        public function checkStudentEventRegistration($userID, $eventID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':userid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':eventid';
            $arr[$i]['value'] = $eventID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            
            $query = 'SELECT RegistrationID FROM FYS_Registration_Table 
                        WHERE UserID = :userid AND EventID = :eventid';

            $results = $this->core->executeSQL($query, $arr);
            return (count($results) > 0) ? true : false;
        }
        
        /**
         *  decreaseEventBookedSeats Subtracts registered seats by 1 for a given event
         *  
         *@param int $eventID ID of event
         *@return int Result of decreasing registered seats for event
         */
        private function decreaseEventBookedSeats($eventID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':eventid';
            $arr[$i]['value'] = $eventID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'UPDATE FYS_Cocurricular_Events
                        SET Registered = Registered-1
                        WHERE EventID = :eventid';

            return $this->core->executeSQL($query, $arr);
        }
    
        /**
         *  deleteStudentEventRegistration Deletes users's registration for a given event
         *  
         *@param int $userID   UserID of user
         *@param int $eventID EventID of event
         *@return int Result of deleting event registration for user
         */
        public function deleteStudentEventRegistration($userID, $eventID) {
            $this->decreaseEventBookedSeats($eventID);
            
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':userid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':eventid';
            $arr[$i]['value'] = $eventID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            
            $query = 'DELETE FROM FYS_Registration_Table 
                        WHERE UserID = :userid AND EventID = :eventid';

            return $this->core->executeSQL($query, $arr);
        }
    
        /**
         *  increaseEventBookedSeats Increments registred seats for event by 1
         *  
         *@param int $eventID EventID of event
         *@return int Result of updating event's booked seats by 1
         */
        private function increaseEventBookedSeats($eventID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':eventid';
            $arr[$i]['value'] = $eventID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'UPDATE FYS_Cocurricular_Events
                        SET Registered = Registered+1
                        WHERE EventID = :eventid';

            return $this->core->executeSQL($query, $arr);
        }
        
        /**
         *  forceStudentRegistration Registers student to an event by adding user and event id into FYS_Registration_Table
         *  
         *@param int $userID   UserID of user
         *@param int $eventID EventID of event
         *@return int Result of registering a student to the specified event
         */
        public function forceStudentRegistration($userID, $eventID) {
            
            $this->increaseEventBookedSeats($eventID);
            
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':userid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':eventid';
            $arr[$i]['value'] = $eventID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $query = 'INSERT INTO FYS_Registration_Table
                      (UserID, EventID)
                      VALUES (:userid, :eventid)';

            $response = $this->core->executeSQL($query, $arr, 'all', 1, true, true);
            return $response;
        }
        
        /**
         *  addUserRole Inserts one user role ID for user in user roles table
         *  
         *@param int $userID UserID of user
         *@param int $roleID roleID of user
         *@return int Result of inserting one user role ID into user roles table
         */
        public function addUserRole($userID, $roleID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':uid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':roleid';
            $arr[$i]['value'] = $roleID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            
            $query = 'INSERT INTO FYS_User_Roles_XREF
                      (UserID, User_Role_ID)
                      VALUES (:uid, :roleid)';
                      
            $response = $this->core->executeSQL($query, $arr, 'all', 1, true, true);
            return $response;
        }
        
        /**
         *  addStudent Add student to users table, inserts student userrole id into user roles table, adds student to a course
         *  
         *@param string[] $studentInfo Post request array of student's values
         *@return int Response of DB query
         */
        public function addStudent($studentInfo) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':fname';
            $arr[$i]['value'] = $studentInfo['firstname'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':lname';
            $arr[$i]['value'] = $studentInfo['lastname'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':accessid';
            $arr[$i]['value'] = $studentInfo['accessid'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':email';
            $arr[$i]['value'] = $studentInfo['email'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':notes';
            $arr[$i]['value'] = $studentInfo['notes'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':confidential';
            $arr[$i]['value'] = $studentInfo['confidential'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            // TODO: UserRole column is not in use anymore- remove or what?
            $query = 'INSERT INTO FYS_User_Table
                      (FName, LName, AccessID, Email,
                      Notes, ConfidentialInfo, UserRole)
                      VALUES (:fname, :lname, :accessid, :email, :notes, :confidential,
                      5)';

            $response = $this->core->executeSQL($query, $arr, 'all', 1, true, true);
            
            $id = $this->getUserID($studentInfo['accessid']);
            
            $this->addUserRole($id, 5);
            foreach($studentInfo['course'] as $courseID) {
                $this->addStudentCourse($id, $courseID);
            }
            
            return $response;
        }
        
        /**
         *  addUser Adds user to users table, adds user roles to user roles table
         *  
         *@param string[] $post         Post request array of values to use for insert
         *@param bool     $imported     Boolean to determine whether the method is called by import tool
         *@return int Response of DB query
         */
        public function addUser($post, $imported = false) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':fname';
            $arr[$i]['value'] = $post['firstname'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':lname';
            $arr[$i]['value'] = $post['lastname'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':accessid';
            $arr[$i]['value'] = $post['accessid'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':email';
            $arr[$i]['value'] = $post['email'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            
            
            if($imported) {
                $query = 'INSERT INTO FYS_User_Table
                      (FName, LName, AccessID, Email, importedOn)
                      VALUES (:fname, :lname, :accessid, :email, now())';
            } else {
                $i++;
                $arr[$i]['parameter'] = ':notes';
                $arr[$i]['value'] = $post['notes'];
                $arr[$i]['data_type'] = PDO::PARAM_STR;
                $i++;
                $arr[$i]['parameter'] = ':confidential';
                $arr[$i]['value'] = $post['confidential'];
                $arr[$i]['data_type'] = PDO::PARAM_STR;
                
                // TODO: UserRole column is not in use anymore- remove or no?
                $query = 'INSERT INTO FYS_User_Table
                          (FName, LName, AccessID, Email,
                          Notes, ConfidentialInfo, UserRole)
                          VALUES (:fname, :lname, :accessid, :email, :notes, :confidential,
                          5)';
            }

            $response = $this->core->executeSQL($query, $arr, 'all', 1, true, true);
            
            $id = $this->getUserID($post['accessid']);
            $this->addUserRoles($id, $post['roles']);
            return $response;
        }
        
        /**
         *  editUser Edits user based on submitted values
         *  
         *@param string[] $post Post request array of submitted values
         *@return int Response of DB query
         */
        public function editUser($post) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':fname';
            $arr[$i]['value'] = $post['firstname'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':lname';
            $arr[$i]['value'] = $post['lastname'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':userid';
            $arr[$i]['value'] = $post['userid'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':email';
            $arr[$i]['value'] = $post['email'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':notes';
            $arr[$i]['value'] = $post['notes'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':confidential';
            $arr[$i]['value'] = $post['confidential'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $query = 'UPDATE FYS_User_Table
                      SET FName=:fname, LName=:lname, Email=:email,
                      Notes=:notes, ConfidentialInfo=:confidential WHERE UserID=:userid';

            $response = $this->core->executeSQL($query, $arr);
            
            $this->deleteUserRoles($post['userid']);
            $this->addUserRoles($post['userid'], $post['roles']);
            
            return $response;
        }
        
        /**
         *  addUserRoles Adds multiple user roles to user roles table for user
         *  
         *@param int $userID UserID of user
         *@param int[] $roles   Array of roleIDs
         *@return int  Response of DB query
         */
        private function addUserRoles($userID, $roles) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':uid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            
            $query = 'INSERT INTO FYS_User_Roles_XREF
                      (UserID, User_Role_ID)
                      VALUES ';
            
            for($x=0; $x < count($roles); $x++) {
                $queryRoleID = ':role' . $x . 'id';
                $arr[$i]['parameter'] = $queryRoleID;
                $arr[$i]['value'] = $roles[$x];
                $arr[$i]['data_type'] = PDO::PARAM_INT;
                $i++;
                $comma = ($x == count($roles)-1) ? "" : ",";
                $query .= "(:uid, " . $queryRoleID . ") " . $comma;
            }

            $response = $this->core->executeSQL($query, $arr);
            return $response;
        }
        
        /**
         *  editStudent Edit student based on submitted form values
         *  
         *@param string[] $studentInfo Post request array of submitted values
         *@return int Response of DB query
         */
        public function editStudent($studentInfo) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':fname';
            $arr[$i]['value'] = $studentInfo['firstname'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':lname';
            $arr[$i]['value'] = $studentInfo['lastname'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':userid';
            $arr[$i]['value'] = $studentInfo['userid'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':email';
            $arr[$i]['value'] = $studentInfo['email'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':notes';
            $arr[$i]['value'] = $studentInfo['notes'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':confidential';
            $arr[$i]['value'] = $studentInfo['confidential'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $query = 'UPDATE FYS_User_Table
                      SET FName=:fname, LName=:lname, Email=:email,
                      Notes=:notes, ConfidentialInfo=:confidential WHERE UserID=:userid';

            $response = $this->core->executeSQL($query, $arr);
            
            // Clear linked student courses
            $this->deleteStudentCourses($studentInfo['userid']);
            
            // Link student courses
            foreach($studentInfo['course'] as $courseID) {
                $this->addStudentCourse($studentInfo['userid'], $courseID);
            }
            
            return $response;
        }
        
        /**
         *  incrementStudentSeats Increases available seats by 1 for a user's registered event
         *  
         *@param int $userID UserID of user
         *@return int Response of DB query
         */
        private function incrementStudentSeats($userID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':userid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'UPDATE FYS_Cocurricular_Events AS event
                        INNER JOIN FYS_Registration_Table reg ON event.EventID = reg.EventID
                        SET event.Registered = event.Registered-1
                        WHERE reg.UserID = :userid';

            return $this->core->executeSQL($query, $arr);
        }
        
        /**
         *  deleteStudentRegistration Removes all registrations of event for user
         *  
         *@param int $userID UserID of user
         *@return int Response of DB query
         */
        private function deleteStudentRegistration($userID) {

            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':userid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'DELETE FROM FYS_Registration_Table 
                        WHERE UserID = :userid';

            return $this->core->executeSQL($query, $arr);
        }
        
        /**
         *  deleteStudentCourses Removes all student courses for a student from courses table
         *  
         *@param int $userID UserID of user
         *@return int Response of DB query
         */
        private function deleteStudentCourses($userID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':userid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'DELETE FROM FYS_User_Courses_XREF 
                        WHERE UserID = :userid';

            return $this->core->executeSQL($query, $arr);
        }
        
        /**
         *  deleteOneRole Removes one specific role from user
         *  
         *@param int $userID UserID of user
         *@param int $roleID RoleID of role
         *@return int Response of DB query    
         */
        private function deleteOneRole($userID, $roleID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':uid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':roleid';
            $arr[$i]['value'] = $roleID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            
            $query = 'DELETE FROM FYS_User_Roles_XREF
                        WHERE UserID = :uid AND User_Role_ID = :roleid';

            return $this->core->executeSQL($query, $arr);
        }

        /**
         *  deleteUserRoles Removes all user roles for a user
         *  
         *@param int $userID UserID
         *@return int Response of DB query
         */
        private function deleteUserRoles($userID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':uid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            
            $query = 'DELETE FROM FYS_User_Roles_XREF
                        WHERE UserID = :uid';

            return $this->core->executeSQL($query, $arr);
        }
        
        /**
         *  deleteStudent Calls subsequent methods to delete student completely from DB
         *  
         *@param int $userID UserID of user
         *@return void
         */
        public function deleteStudent($userID) {
            $this->incrementStudentSeats($userID);
            $this->deleteUser($userID);
            $this->deleteStudentCourses($userID);
            $this->deleteStudentRegistration($userID);
        }

        public function removeUserAllPresenter($accessID) {
            $this->removeUserPresenter($accessID);
            $this->removeUserCoPresenter1($accessID);
            $this->removeUserCoPresenter2($accessID);
        }

        public function removeUserPresenter($accessID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':accessid';
            $arr[$i]['value'] = $accessID;
            $arr[$i]['data_type'] = PDO::PARAM_STR;

            $query = 'UPDATE FYS_Cocurricular_Events 
            SET Presenter = ""
            WHERE Presenter = :accessid';

            return $this->core->executeSQL($query, $arr);
        }

        public function removeUserCoPresenter1($accessID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':accessid';
            $arr[$i]['value'] = $accessID;
            $arr[$i]['data_type'] = PDO::PARAM_STR;

            $query = 'UPDATE FYS_Cocurricular_Events 
            SET CoPres1 = ""
            WHERE CoPres1 = :accessid';

             return $this->core->executeSQL($query, $arr);
        }

        public function removeUserCoPresenter2($accessID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':accessid';
            $arr[$i]['value'] = $accessID;
            $arr[$i]['data_type'] = PDO::PARAM_STR;

            $query = 'UPDATE FYS_Cocurricular_Events 
            SET CoPres2 = ""
            WHERE CoPres2 = :accessid';

             return $this->core->executeSQL($query, $arr);
        }

        public function getAccessID($userID){
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':userid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'SELECT AccessID 
            FROM FYS_User_Table 
            WHERE UserID = :userid';

            return $this->core->executeSQL($query, $arr);

        }
        
        /**
         *  deleteUser Removes user from DB including their user roles
         *  
         *@param int $userID UserID of user
         *@return null|int DB response of query
         */
        public function deleteUser($userID) {
            $userID = (int) $userID;
            if(empty($userID) || !is_int($userID)) {
                return null;
            }

            $accessIDArr = $this->getAccessID($userID);
            $accessID = $accessIDArr[0]->AccessID;

            if(empty($accessID)){
                return null;
            } else {

                $this->removeUserAllPresenter($accessID);
            
                $i = 0; $arr = null;
                $arr[$i]['parameter'] = ':userid';
                $arr[$i]['value'] = $userID;
                $arr[$i]['data_type'] = PDO::PARAM_INT;

                $query = 'DELETE FROM FYS_User_Table
                          WHERE UserID = :userid';
                          
                $this->deleteUserRoles($userID);
                return $this->core->executeSQL($query, $arr);   
            } 
        }
        
        /**
         *  addEvent Creates a new event for DB based on post form submitted values
         *  
         *@param string[] $eventInfo Post form request submitted values
         *@return int DB response of query
         */
        public function addEvent($eventInfo) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':name';
            $arr[$i]['value'] = $eventInfo['Name'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':type';
            $arr[$i]['value'] = $eventInfo['Type'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':date';
            $arr[$i]['value'] = $eventInfo['Date'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':start';
            $arr[$i]['value'] = $eventInfo['StartTime'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':end';
            $arr[$i]['value'] = $eventInfo['EndTime'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':location';
            $arr[$i]['value'] = $eventInfo['Location'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':seats';
            $arr[$i]['value'] = $eventInfo['Seats'];
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':description';
            $arr[$i]['value'] = $eventInfo['Description'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':link';
            if (empty($eventInfo['Link'])) {
                $arr[$i]['value'] = '';
            }
            else {
                $arr[$i]['value'] = $eventInfo['Link'];
            }
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':presenter';
            $arr[$i]['value'] = $eventInfo['Presenter'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':copres1';
            $arr[$i]['value'] = $eventInfo['CoPres1'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':copres2';
            $arr[$i]['value'] = $eventInfo['CoPres2'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':semester';
            $arr[$i]['value'] = $eventInfo['Semester'];
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':active';
            if (!array_key_exists('Active', $eventInfo)) {
                $arr[$i]['value'] = 0;
            }
            else {
                $arr[$i]['value'] = $eventInfo['Active'];
            }
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'INSERT INTO FYS_Cocurricular_Events
                      (Name, Type, Date, StartTime, EndTime, Location, Seats,
                      Description, Link, Presenter, CoPres1, CoPres2, active, semesterid)
                      VALUES (:name, :type, :date, :start, :end, :location, :seats,
                      :description, :link, :presenter, :copres1, :copres2, :active, :semester)';

            return $this->core->executeSQL($query, $arr, 'all', 1, false, true);
        }

        /**
         *  notifyAdminEvent Emails all admins regarding creation of a new event
         *  
         *@param string[] $eventInfo Post form request submitted values
         *@param int $eventID     EventID of new event
         *@return void
         */
        public function notifyAdminEvent($eventInfo, $eventID) {
            $query = 'SELECT FName, LName, AccessID, Email
                      FROM FYS_User_Table
                      INNER JOIN FYS_User_Roles_XREF rxref
                      USING (UserID)
                      WHERE rxref.User_Role_ID = 1 AND Email <> \'\';';

            $results = $this->core->executeSQL($query);

            $subject = 'FYS - New Event Added - ID #' . $eventID;
            
            $message = 'A new FYS event has been added.' . "<br /><br />\r\n\r\n" .
                       'Added By: ' . $eventInfo['Signal'] . "<br />\r\n" . 'Event ID: ' . $eventID . "<br />\r\n" .
                       'Name: ' . $eventInfo['Name'] . "<br />\r\n" . 'Type: ' . $eventInfo['Type'] . "<br />\r\n" .
                       'Date: ' . $eventInfo['Date'] . "<br />\r\n" . 'Start Time: ' . $eventInfo['StartTime'] . "\r\n" .
                       'End Time: ' . $eventInfo['EndTime'] . "<br />\r\n" . 'Location: ' . $eventInfo['Location'] . "<br />\r\n" .
                       'Seats: ' . $eventInfo['Seats'] . "<br /><br />\r\n" . 'Description: ' . $eventInfo['Description'] . "<br />\r\n" .
                       'Link: ' . $eventInfo['Link'] . "<br /><br />\r\n\r\n" . 'Presenter: ' . $eventInfo['Presenter'] . "<br />\r\n" .
                       'Co-Presenter 1: ' . $eventInfo['CoPres1'] . "<br />\r\n" . 'Co-Presenter 2: ' . $eventInfo['CoPres2'] .
                       "<br />\r\n" . 'Semester: ' . $eventInfo['Semester'] . "<br /><br />\r\n\r\n" . '<a href="' . $_SERVER['HTTP_HOST'] .
                       $this->settings['current_URL_path'] . '/admin/events/edit-event?EventID=' . $eventID . '">View Event</a>' ;

            $headers = 'FROM: psh_fys_no_reply@psu.edu' . "\r\n" .
                       'Reply-To: psh_fys_no_reply@psu.edu' . "\r\n" .
                       'X-Mailer: PHP/' . phpversion() . "\r\n" .
                       'MIME-Version: 1.0' . "\r\n" . 
                       'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";

            foreach ($results as $result) {
                mail($result->Email, $subject, $message, $headers);
            }
        }

        /**
         *  editEvent Modifies event based on post request submitted values
         *  
         *@param string[] $eventInfo Post request submitted values
         *@return bool
         */
        public function editEvent($eventInfo) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':name';
            $arr[$i]['value'] = $eventInfo['Name'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':type';
            $arr[$i]['value'] = $eventInfo['Type'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':date';
            $arr[$i]['value'] = $eventInfo['Date'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':start';
            $arr[$i]['value'] = $eventInfo['StartTime'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':end';
            $arr[$i]['value'] = $eventInfo['EndTime'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':location';
            $arr[$i]['value'] = $eventInfo['Location'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':seats';
            $arr[$i]['value'] = $eventInfo['Seats'];
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':description';
            $arr[$i]['value'] = $eventInfo['Description'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':link';            
            $arr[$i]['value'] = (empty($eventInfo['Link'])) ? '' : $eventInfo['Link']; 

            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':presenter';
            $arr[$i]['value'] = $eventInfo['Presenter'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':copres1';
            $arr[$i]['value'] = $eventInfo['CoPres1'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':copres2';
            $arr[$i]['value'] = $eventInfo['CoPres2'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':semester';
            $arr[$i]['value'] = $eventInfo['Semester'];
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':active';
            $arr[$i]['value'] = $eventInfo['Active'];
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':eventID';
            $arr[$i]['value'] = $eventInfo['EventID'];
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'UPDATE FYS_Cocurricular_Events
                      SET Name = :name, Type = :type, Date = :date, StartTime = :start, EndTime = :end,
                          Location = :location, Seats = :seats, Description = :description, Link = :link,
                          Presenter = :presenter, CoPres1 = :copres1, CoPres2 = :copres2, active = :active,
                          semesterid = :semester
                      WHERE EventID = :eventID';

            $results = $this->core->executeSQL($query, $arr, 'all', 1, false, true);

            if ($results != 0) {
                return true;
            }

            return false;
        }

        /**
         *  deleteAllEventRegistration Removes all event registration for an event
         *  
         *@param int $eventID EventID of event
         *@return int Response of DB query
         */
        public function deleteAllEventRegistration($eventID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':eventID';
            $arr[$i]['value'] = $eventID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'DELETE FROM FYS_Registration_Table
                      WHERE EventID = :eventID';

            return $this->core->executeSQL($query, $arr);
        }
        
        /**
         *  deleteEvent Calls to remove all event registration and deletes event from events table
         *  
         *@param int $eventID EventID of event
         *@return int Response of DB query
         */
        public function deleteEvent($eventID) {
            $this->deleteAllEventRegistration($eventID);
            
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':eventID';
            $arr[$i]['value'] = $eventID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'DELETE FROM FYS_Cocurricular_Events
                      WHERE EventID = :eventID';

            return $this->core->executeSQL($query, $arr);
        }

        /**
         *  getEventInfo Retrieves event information based on specified event
         *  
         *@param int $eventID EventID of event
         *@return null|object DB object containing event information (*)
         */
        public function getEventInfo($eventID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter']=':eventID';
            $arr[$i]['value']= $eventID;
            $arr[$i]['data_type']=PDO::PARAM_INT;
            
            $query = 'SELECT *
                      FROM FYS_Cocurricular_Events
                      WHERE EventID = :eventID';

            $eventInfo = $this->core->executeSQL($query, $arr);

            if (isset($eventInfo) && !empty($eventInfo)) {
                return $eventInfo;
            }

            return null;
        }

        /**
         *  getPreEventEmailRecipients Retrieves all pre event email recipients (admins and students registered for the event)
         *  
         *@param int $eventID EventID of event
         *@return object[] Empty array or DB object containing all matching email recipients
         */
        public function getPreEventEmailRecipients($eventID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter']=':eventID';
            $arr[$i]['value']= $eventID;
            $arr[$i]['data_type']=PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter']=':userID';
            $arr[$i]['value']= $this->userID;
            $arr[$i]['data_type']=PDO::PARAM_INT;

            $query = 'SELECT UserID, FName, LName, Email
                      FROM FYS_User_Table
                      INNER JOIN (
                        SELECT UserID
                        FROM FYS_Registration_Table
                        WHERE EventID = :eventID

                        UNION
                        SELECT UserID
                        FROM FYS_User_Table
                          INNER JOIN FYS_User_Roles_XREF rxref
                          USING (UserID)
                        WHERE rxref.User_Role_ID = 1

                        UNION SELECT :userID
                      ) uidt
                      USING (UserID)';

            $results = $this->core->executeSQL($query, $arr);

            if (isset($results) && !empty($results)) {
                return $results;
            }

            return array();
        }

        /**
         *  sendEmailToAll Sends an email to all users and students
         *  
         *@param string[] $emailInfo   Array containing email subject, message, replyTo
         *@param object[] $recipients Array of DB objects of email recipients
         *@return bool
         */
        public function sendEmailToAll($emailInfo, $recipients) {
            if (isset($emailInfo)) {
                $subject = strip_tags($emailInfo['Subject']);

                $message = strip_tags($emailInfo['Message']);

                $headers =  'FROM: psh_fys_no_reply@psu.edu' . "\r\n" .
                            'Reply-To: ' . strip_tags($emailInfo['Reply']) . "\r\n" .
                            'X-Mailer: PHP/' . phpversion() . "\r\n";

                foreach ($recipients as $recipient) {
                    mail($recipient->Email, $subject, $message, $headers);
                }
                return true;
            }

            return false;
        }
        
        /**
         *  sendPreEventEmails Sends pre event emails based on recipients and email info
         *  
         *@param string[] $emailInfo   Array containing email subject, message, replyTo
         *@param object[] $recipients Array of DB objects of email recipients
         *@return bool
         */
        public function sendPreEventEmails($emailInfo, $recipients) {
            $eventInfo = $this->getEventInfo($emailInfo['EventID']);

            if (isset($eventInfo)) {
                $subject = strip_tags($emailInfo['Subject']);

                $message = strip_tags($emailInfo['Message']);

                $headers =  'FROM: psh_fys_no_reply@psu.edu' . "\r\n" .
                            'Reply-To: ' . strip_tags($emailInfo['Reply']) . "\r\n" .
                            'X-Mailer: PHP/' . phpversion() . "\r\n";

                foreach ($recipients as $recipient) {
                    if ($emailInfo['Greeting'] == 'Yes') {
                        mail($recipient->Email, $subject, 'Hello ' . $recipient->FName . ",\r\n\r\n" . $message, $headers);
                    }
                    else {
                        mail($recipient->Email, $subject, $message, $headers);
                    }
                }
                return true;
            }

            return false;
        }

        /**
         *  updateAttendance Updates attendance for an event after it is completed, usually performed by presenter of the event
         *  
         *@param string[] $post Array of post event info and attendance values
         *@return bool
         */
        public function updateAttendance($post) {
            $i = 0; $arr = null;
            $arr[$i]['parameter']=':eventID';
            $arr[$i]['value']= $post['EventID'];
            $arr[$i]['data_type']=PDO::PARAM_INT;           
            
            $yesIDs = array();
            $noIDs = array();
            $resultsYes = -1;
            $resultsNo = -1;
            
            foreach ($post as $key => $value) {
                if (is_numeric($key)) {
                    if ($value == "Y") {
                        array_push($yesIDs, $key);
                    }
                    elseif ($value == "N") {
                        array_push($noIDs, $key);
                    }
                }
            }

            if (!empty($yesIDs)) {
                $query = 'UPDATE FYS_Registration_Table
                          SET IsAttended = \'Y\'
                          WHERE EventID = :eventID
                          AND RegistrationID IN (' . implode(',', $yesIDs) . ')';

                $resultsYes = $this->core->executeSQL($query, $arr);
            }

            if (!empty($noIDs)) {
                $query = 'UPDATE FYS_Registration_Table
                          SET IsAttended = \'N\'
                          WHERE EventID = :eventID
                          AND RegistrationID IN (' . implode(',', $noIDs) . ')';

                $resultsNo = $this->core->executeSQL($query, $arr);
            }

            if ($resultsYes > 0 || $resultsNo > 0) {
                return true;
            }

            return false;
        }

        /**
         *  getAttendanceCount Fetches total attendance count for an event- used to send out post event email to all students regarding their attendance for the event
         *  
         *@param int $eventID EventID of event
         *@return int Number of total attendance count for the event
         */
        public function getAttendanceCount($eventID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter']=':eventID';
            $arr[$i]['value']= $eventID;
            $arr[$i]['data_type']=PDO::PARAM_INT;

            $query = 'SELECT COUNT(IsAttended) AS Count
                      FROM FYS_Registration_Table
                      WHERE EventID = :eventID
                      AND IsAttended IN (\'Y\', \'N\')';

            $results = $this->core->executeSQL($query, $arr);

            if (count($results) > 0 && property_exists($results[0], 'Count')) {
                return $results[0]->Count;
            }

            return 0;
        }

        /**
         *  emailAttendance Emails attendance of student after event is completed- sent to each student who registered for an event
         *  
         *@param int $eventID EventID of event
         *@return bool
         */
        public function emailAttendance($eventID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter']=':eventID';
            $arr[$i]['value']= $eventID;
            $arr[$i]['data_type']=PDO::PARAM_INT;

            $query = 'SELECT FYS_Registration_Table.EventID, FYS_User_Table.Email, FYS_Cocurricular_Events.Date
                      , FYS_Cocurricular_Events.Name, FYS_Cocurricular_Events.StartTime, FYS_User_Table.FName
                      , FYS_User_Table.LName, FYS_Registration_Table.IsAttended
                      FROM FYS_Registration_Table
                      INNER JOIN FYS_User_Table
                        ON (FYS_Registration_Table.UserID = FYS_User_Table.UserID)
                      INNER JOIN FYS_Cocurricular_Events
                        ON (FYS_Registration_Table.EventID = FYS_Cocurricular_Events.EventID)
                      WHERE (FYS_Registration_Table.EventID = :eventID
                      AND FYS_Registration_Table.IsAttended IN (\'Y\', \'N\'));';

            $results = $this->core->executeSQL($query, $arr);

            if (is_array($results) && count($results > 0)) {
                require_once 'includes/phpmailer.php';

                foreach ($results as $result) {
                    $mail = new PHPMailer;
                    $mail->setFrom('psh_fys_no_reply@psu.edu');
                    $mail->isHTML(true);
                    $mail->addAddress($result->Email);

                    if ($result->IsAttended == 'Y') {
                        $mail->Subject = $results[0]->Name . ' - FYS cocurricular event attendance confirmed';

                        $mail->Body = 'Hello ' . $result->FName . ' ' . $result->LName . ',<br /><br />'
                        . 'Your attendance at the FYS event, <strong>' . $result->Name . '</strong> on ' . date_format(date_create($result->Date), 'm-d-Y')
                        . ' has been confirmed. Please complete the survey at: <a href="http://' . $_SERVER['HTTP_HOST']
                        . $this->settings['current_URL_path'] . '/register/survey">http://' . $_SERVER['HTTP_HOST']
                        . $this->settings['current_URL_path'] . '/register/survey</a><br /><br />'
                        . 'Contact the administrator at <a href="mailto:FYScocurriculars@psu.edu">FYScocurriculars@psu.edu</a> if you have questions.';

                        $mail->AltBody = 'Hello ' . $result->FName . ' ' . $result->LName . ",\r\n\r\n"
                        . 'Your attendance at the FYS event, ' . $result->Name . ' on ' . date_format(date_create($result->Date), 'm-d-Y')
                        . ' has been confirmed. Please complete the survey at: http://' . $_SERVER['HTTP_HOST']
                        . $this->settings['current_URL_path'] . '/register/survey">' . "\r\n\r\n"
                        . 'Contact the administrator at FYScocurriculars@psu.edu if you have questions.';
                    }
                    elseif ($result->IsAttended == 'N') {
                        $mail->Subject = $results[0]->Name . ' - Missed FYS cocurricular event';

                        $mail->Body = 'Hello ' . $result->FName . ' ' . $result->LName . ',<br /><br />'
                        . 'Our attendance records show that you did <strong>NOT</strong> attend FYS event, <strong>' . $result->Name . '</strong> on '
                        . date_format(date_create($result->Date), 'm-d-Y') . '.<br /><br />'
                        . 'For future events, as a courtesy to other students, please cancel if you cannot attend.<br /><br />'
                        . 'Contact the administrator at <a href="mailto:FYScocurriculars@psu.edu">FYScocurriculars@psu.edu</a> if you have questions.';

                        $mail->AltBody = 'Hello ' . $result->FName . ' ' . $result->LName . ",\r\n\r\n"
                        . 'Our attendance records show that you did NOT attend FYS event, ' . $result->Name . ' on '
                        . date_format(date_create($result->Date), 'm-d-Y') . ".\r\n\r\n"
                        . 'For future events, as a courtesy to other students, please cancel if you cannot attend.' . "\r\n\r\n"
                        . 'Contact the administrator at FYScocurriculars@psu.edu if you have questions.';
                    }

                    if (!$mail->send()) {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        /**
         *  getPreEventComments Fetches all pre event comments for an event
         *  
         *@param int $eventID EventID of event
         *@return bool|object DB object containing all PreEventComments
         */
        public function getPreEventComments($eventID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter']=':eventID';
            $arr[$i]['value']= $eventID;
            $arr[$i]['data_type']=PDO::PARAM_INT;

            $query = 'SELECT PreEventComments
                      FROM FYS_Registration_Table
                      WHERE EventID = :eventID 
                        AND PreEventComments IS NOT NULL 
                        AND PreEventComments <> \'\'';

            $results = $this->core->executeSQL($query, $arr);

            if (is_array($results) && count($results) > 0) {
                return $results;
            }

            return false;
        }

        /**
         *  getEventFeedback Fetches event feedback for a specified event
         *  
         *@param int $eventID EventID of event
         *@return int|object DB Object containing all feedback for an event
         */
        public function getEventFeedback($eventID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter']=':eventID';
            $arr[$i]['value']= $eventID;
            $arr[$i]['data_type']=PDO::PARAM_INT;

            #FIXME maybe. CF version links to CourseID. This has been removed due to current inability to test
            # data related the courses table. See excel_report.cfm for original query.
            # Also, consider that if a non super admin user views the Feedback report page and the survey information and
            # pre-event comments are somehow both null, blank rows will be produced.

            $query = 'SELECT reg.UserID, SurveyRating, SurveyParagraph, PreEventComments, FName, LName, AccessID
                      FROM FYS_Registration_Table reg
                      INNER JOIN FYS_User_Table usr
                      USING (UserID)
                      WHERE reg.EventID = :eventID 
                      ORDER BY usr.LName ASC, usr.FName ASC';

            $results = $this->core->executeSQL($query, $arr);

            if (is_array($results) && count($results) > 0) {
                return $results;
            }

            return 0;
        }
        
        /**
         *  redirectEmailAllPresenters Redirect for post email all presenters action by admin- used to perform DB query and redirect based on error or success
         *  
         *@param string[] $post Array of post request values
         *@return void
         */
        public function redirectEmailAllPresenters($post) {
            if (isset($post['Submit']) && in_array("", $post, true) == false) {
                $recipients = $this->getPresenters(false);
                if (!empty($recipients)) {
                    if ($this->sendEmailToAll($post, $recipients)) {
                        header('Location: ' . $this->settings['current_URL_path'] . '/admin/reports/confirm-email-all?'
                        . 'results=success');
                        exit;
                    }
                    
                    header('Location: ' . $this->settings['current_URL_path'] . '/admin/reports/confirm-email-all?'
                    . 'results=failure');
                    exit;
                }
                
                header('Location: ' . $this->settings['current_URL_path'] . '/admin/reports/confirm-email-all?'
                . 'results=empty');
                exit;
            }
            
            header('Location: ' . $this->settings['current_URL_path'] . '/admin/reports/confirm-email-all?'
            . 'results=error');
            exit;
        }
        
        /**
         *  redirectEmailAllStudents Redirect for post email all students action by admin- used to perform DB query and redirect based on error or success
         *  
         *@param string[] $post Array of post request values
         *@return void
         */
        public function redirectEmailAllStudents($post) {
            if (isset($post['Submit']) && in_array("", $post, true) == false) {
                $recipients = $this->getAllUsers();
                if (!empty($recipients)) {
                    if ($this->sendEmailToAll($post, $recipients)) {
                        header('Location: ' . $this->settings['current_URL_path'] . '/admin/reports/confirm-email-all?'
                        . 'results=success');
                        exit;
                    }
                    
                    header('Location: ' . $this->settings['current_URL_path'] . '/admin/reports/confirm-email-all?'
                    . 'results=failure');
                    exit;
                }
                
                header('Location: ' . $this->settings['current_URL_path'] . '/admin/reports/confirm-email-all?'
                . 'results=empty');
                exit;
            }
            
            header('Location: ' . $this->settings['current_URL_path'] . '/admin/reports/confirm-email-all?'
            . 'results=error');
            exit;
        }
        
        /**
         *  redirectPreEventEmail Redirect for post pre event email action by admin- used to perform DB query and redirect based on error or success
         *  
         *@param string[] $post Array of post request values
         *@return void
         */
        public function redirectPreEventEmail($post) {
            if (isset($post['Submit']) && in_array("", $post, true) == false) {
                $recipients = $this->getPreEventEmailRecipients($post['EventID']);
                if (!empty($recipients)) {
                    if ($this->sendPreEventEmails($post, $recipients)) {
                        header('Location: ' . $this->settings['current_URL_path'] . '/admin/events/confirm-pre-event-email?'
                        . 'results=success');
                        exit;
                    }
                    
                    header('Location: ' . $this->settings['current_URL_path'] . '/admin/events/confirm-pre-event-email?'
                    . 'results=failure');
                    exit;
                }
                
                header('Location: ' . $this->settings['current_URL_path'] . '/admin/events/confirm-pre-event-email?'
                . 'results=empty');
                exit;
            }
            
            header('Location: ' . $this->settings['current_URL_path'] . '/admin/events/confirm-pre-event-email?'
            . 'results=error');
            exit;
        }
        
        /**
         *  redirectDevelopAttendance Redirect for post develop attendance action by admin- used to perform DB query and redirect based on error or success
         *  
         *@param string[] $post Array of post request values
         *@return void
         */
        public function redirectDevelopAttendance($post) {
            if (isset($post['Submit']) && in_array("", $post, true) == false) {
                if(isset($post['ForceStudent']) && isset($post['EventID'])) {
                    foreach($post['ForceStudent'] as $uid) {
                        $this->forceStudentRegistration($uid, $post['EventID']);
                    }
                    header('Location: ' . $this->settings['current_URL_path'] . '/admin/events/develop-attendance-report?'
                    . 'results=added&EventID=' . $post['EventID']);
                    exit;
                }
                
                if ($this->updateAttendance($post)) {
                    header('Location: ' . $this->settings['current_URL_path'] . '/admin/events/confirm-develop-attendance-report?'
                    . 'results=success&EventID=' . $post['EventID']);
                    exit;
                }
                
                header('Location: ' . $this->settings['current_URL_path'] . '/admin/events/confirm-develop-attendance-report?'
                . 'results=nochange&EventID=' . $post['EventID']);
                exit;
            }

            header('Location: ' . $this->settings['current_URL_path'] . '/admin/events/confirm-develop-attendance-report?'
            . 'results=error&EventID=' . $post['EventID']);
            exit;
        }

        /**
         *  redirectEmailAttendance Redirect for post email attendance action by admin- used to perform DB query and redirect based on error or success
         *  
         *@param string[] $post Array of post request values
         *@return void
         */
        public function redirectEmailAttendance($post) {
            if (isset($post['Submit']) && isset($post['EventID'])) {
                if ($this->emailAttendance($post['EventID'])) {
                    header('Location: ' . $this->settings['current_URL_path'] . '/admin/events/confirm-email-attendance-report?'
                    . 'results=success&EventID=' . $post['EventID']);
                    exit;
                }

                header('Location: ' . $this->settings['current_URL_path'] . '/admin/events/confirm-email-attendance-report?'
                    . 'results=failure&EventID=' . $post['EventID']);
                    exit;
            }

            header('Location: ' . $this->settings['current_URL_path'] . '/admin/events/confirm-email-attendance-report?'
                    . 'results=error&EventID=' . $post['EventID']);
                    exit;
        }
        
        public function backupDB($fileSuffix='',$download=0) {
            $path = dirname(__FILE__) . "/admin/import/exports/";

            // defaults from settings
            $host     = $this->core->settings['dbHost'];
            $user     = $this->core->settings['dbUsername'];
            $password = $this->core->settings['dbPassword'];
            $database = $this->core->settings['dbName'];
            
            $dateString = date("Ymd_Gis");
            $path .= $dateString . "_" . $database . "_" . $fileSuffix .".sql";
            
            $file = exec('mysqldump --host=\''. $host .'\' --user=\''. $user .'\' --password=\''. $password .'\' '. $database .' --add-drop-table --single-transaction > '. $path);
            if($download && file_exists($path)) {
                header("Content-Description: File Transfer");
                header("Content-Disposition: attachment; filename=" . basename($path) . "");
                header("Content-Length: " . filesize($path));
                header("Content-Type: application/octet-stream;");
                readfile($path);
                exit;
            }
            
            return $file === 0;
        }
        
        /**
         *  base64_url_encode Encodes input string with base64_encode- used for passing IDs and other information quickly without executing DB query
         *  
         *@param string $input String to encode
         *@return string Encoded string
         */
        public function base64_url_encode($input)
        {
            return strtr(base64_encode($input), '+/=', '-_,');
        }
        
        /**
         *  base64_url_decode Decodes encoded input string with base64_decode- used for passing IDs and other information quickly without executing DB query
         *  
         *@param string $input String to encode
         *@return string Decoded string
         */
        public function base64_url_decode($input)
        {
            return base64_decode(strtr($input, '-_,', '+/='));
        }

        /**
         *  chromeLog Logger function to log PHP messages/errors to console
         *  
         *@param string $msg Log message
         *@return void
         */
        public function chromeLog($msg) {
            include("ChromePhp.php");
            ChromePhp::log($msg);
        }
    }

?>
