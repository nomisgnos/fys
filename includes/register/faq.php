                <ol style="padding-bottom:7.5px;" class="breadcrumb">
                  <li class="breadcrumb-item"><a style="text-decoration: none;" href="<?= $_settings['current_URL_path'];?>/register">FYS</a></li>
                  <li class="breadcrumb-item active">FAQ</li>
                </ol><!--.breadcrumbs"-->
                <span class="clearfloat"></span>
                <h2>FAQ</h2>
            </div><!--content-header-->
            
            <div class="body-content" id="body-content-padding">

                <p><strong>What if I register for an event and do not attend?</strong><br />
                You are expected to drop your registration one business day prior to the event. The report to your professor will include students who attended the event and students who did not attend and did not delete their registration. There will be no report of students who dropped their registration prior to the event.</p>

                <p><strong>Can I attend an event even if I am not registered for it?</strong><br />
                Yes, provided there is space.  Space is determined by the number of seats in the room. Students who have registered will have first priority.</p>

                <p><strong>How do I remove myself from the registration list if I find out I cannot attend an event?</strong><br />
                There is an option to drop the cocurricular on the "My Registered Events" page. This is available up to one business day before the event. If you find you cannot attend after that time, you cannot drop the event.</p>

                <p><strong>How many cocurriculars do I have to do? Which event types are mandatory?</strong><br />
                Six (6) out-of-class events are required. One of these is an advising event, one is a writing ethics event, and one is a Title IX. event, all three of which are mandatory. You must then register for and attend 3 "Other Events" to satisfy the FYS requirement.</p>

                <p><strong>Do I have to complete the pre-event questions and the post-event survey?</strong><br />
                There is no grade penalty if you do not, but please do so. If you provide information, questions, and evaluation to the presenters, they will consider and use that information (whenever possible) either in planning for the event or in revising the workshop for the next semester.</p>

                <p><strong>Does the presenter view my survey?</strong><br />
                The presenters are sent a list of the ratings and comments at the end of the semester. They use this information to plan for the next year. However, your name is removed from the list, so the presenters see only the ratings and comments without knowing who wrote them.</p>

                <a href="<?=$_settings['current_URL_path']; ?>/register" class="btn btn-default">Return to Events Listing</a>
                
            </div><!--.body-content-->
        </div><!--.column-1-->
    </div><!--.clearfix-->
</div><!--.wrapper-->