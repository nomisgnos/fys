
                <ol style="padding-bottom:7.5px;" class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= $_settings['current_URL_path'];?>">FYS</a></li>
                  <li class="breadcrumb-item active">Register</li>
                </ol>
                <span class="clearfloat"></span>
                <h1>Event Registration Page</h1>
            </div> <!-- #content_header -->

            <div class="body-content" id="body-content-padding">
                <div class="available">
                    <h2>Available Event Categories</h2>
                    <div class="list-group">
                      <a style="color: #337ab7;" href="#Advising" class="list-group-item list-group-item-action"><span class="glyphicon glyphicon-book" aria-hidden="true"></span>&nbsp;&nbsp;Advising (mandatory to attend one)</a>
                      <a style="color: #337ab7;" href="#TitleIX" class="list-group-item list-group-item-action"><span class="glyphicon glyphicon-education" aria-hidden="true"></span>&nbsp;&nbsp;Title IX (mandatory to attend one)</a>
                      <a style="color: #337ab7;" href="#WritingEthics" class="list-group-item list-group-item-action"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&nbsp;&nbsp;Writing Ethics (mandatory to attend one)</a>
                      <a style="color: #337ab7;" href="#OtherEvents" class="list-group-item list-group-item-action"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>&nbsp;&nbsp;Other Events</a>
                    </div>
                </div><!--.available-->
            
            <?php 
            if(!isset($userID)) {
                $ef->printAlertBox();
            } ?>
                <div class="infobox">
                    <p>For the First-Year Seminar classes, <strong>five</strong> out-of-class events are required. One of these is an <strong>advising event</strong>, one is a <strong>writing ethics</strong> event, and one is a <strong>Title IX</strong> event, all three of which are mandatory. You must choose 2 "Other Events" to satisfy the FYS requirement. Select which activity(s) you want to attend and then register using this page. Some activities have a limited number of students that can attend. You will be unable to register if less than 1 business day remains before the day of the event.</p>                    
                    
                    <ol>
                        <li><strong>Register</strong> (below) before the deadline given to you by your instructor <strong>but no less than 2 business days before</strong> the day of the event.</li><p></p>
                        <li>Print your <a href="<?= $_settings['current_URL_path'];?>/register/print-report">registration report</a> and be sure to record your choices on your calendar.</li><p></p>
                        <li><strong>Attend the events!</strong> Your attendance will be reported to your instructor. You will receive an email that confirms your attendance at each event. Keep this as a confirmation.</li><p></p>
                        <li>Complete the <strong>event survey</strong> that you will receive an email about after the event.</li>
                    </ol>
                    
                    <p><a href="<?= $_settings['current_URL_path'];?>/register/faq">Cocurricular FAQ</a></p>
                </div>
                
                <h2 id="Advising">Advising Events</h2>
                <div class="table-responsive">
                    <table style="display:none;" class="table table-hover dataRegisterEvents" >
                        <?php $ef->printEvents($ef->getEvents('Advising'), $userID); ?>
                    </table>
                </div>

                <h2 id="TitleIX">Title IX</h2>
                <div class="table-responsive">
                    <table style="display:none;" class="table table-hover dataRegisterEvents" >
                        <?php $ef->printEvents($ef->getEvents('Title IX'), $userID);  ?>
                    </table>
                </div>
                        
                <h2 id="WritingEthics">Writing Ethics</h2>
                <div class="table-responsive">
                    <table style="display:none;" class="table table-hover dataRegisterEvents" >
                        <?php $ef->printEvents($ef->getEvents('Writing Ethics'), $userID);  ?>
                    </table>
                </div>

                <h2 id="OtherEvents">Other Events</h2>
                <div class="table-responsive ">
                    <table style="display:none;" class="table table-hover dataRegisterEvents" >
                        <?php $ef->printEvents($ef->getOtherEvents(), $userID); ?>
                    </table>
                </div>  
            </div><!--body-content-->
        </div><!--.clearfix-->
    </div><!--column-1-->
</div><!--.wrapper-->

<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/dataTables.bootstrap.min.js"></script>
<script>
$('[data-toggle="tooltip"]').tooltip({'selector': '','placement': 'top','container':'body'}); 
    var table = $('.dataRegisterEvents').DataTable({
        deferRender: true,
        orderClasses: false,
        responsive: true,
        autoWidth: true,
        paging:   false,
        searching: true,
        columnDefs: [{ "orderable": false, "targets": 0 } ],
        order: [[ 2, "asc" ]]
    });
    $('.dataRegisterEvents').show();
    
    ajaxEventInfoModal();
</script>
