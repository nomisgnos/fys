                <ol style="padding-bottom:7.5px;" class="breadcrumb">
                  <li class="breadcrumb-item"><a style="text-decoration: none;" href="<?= $_settings['current_URL_path'];?>/register">FYS</a></li>
                  <li class="breadcrumb-item active">Event Registration</li>
                </ol><!--.breadcrumbs"-->
                <span class="clearfloat"></span>
                <h2>Event Registration</h2>
            </div> <!-- #content_header -->
            <div id="alert-area"></div>
            <div class="body-content" id="body-content-padding">

            <?php 

            if (isset($_GET['results'])) {
                switch ($_GET['results']) {
                    case 'deadline':
                        echo '<p>The registration deadline for this event has past. Try signing up for another event.</p>';
                        break;
                    case 'duplicate':
                        echo '<p>You are already registered for this event.</p>';
                        break;
                    case 'failure':
                        echo '<p>The registration request was rejected by the database. Please try again.';
                        break;
                    case 'full':
                        echo '<p>The seats for this event are full. Try signing up for another event.</p>';
                        break;
                    case 'success':
                        echo '<p>You have been successfully registered for this event.</p>';
                        break;
                    default:
                        echo '<p>Unable to register for class due to unknown error. Please try again.</p>';
                        break;
                }

                echo PHP_EOL . '<a class="btn btn-default" href="' . $_settings['current_URL_path'] . '/register">Go back to events listing</a>';
            }

            elseif (isset($_GET['EventID']) && isset($userID)) {
                $eventID = filter_var($_GET['EventID'], FILTER_VALIDATE_INT);
                
                // Make sure eventID is valid
                if(!$eventID) {
                    echo '<p>Invalid Event ID, please check and try again.</p>' . PHP_EOL 
                    . '<a class="btn btn-default" href="' . $_settings['current_URL_path'] . '/register">Go back to events listing</a>';;
                    return;
                }
                
                $eventInfo = $ef->getEventInfo($_GET['EventID']);
                
                // Make sure event is active
                if(!$ef->isEventActive($eventInfo[0])) {
                    echo '<p>Event could not be found!</p>' . PHP_EOL 
                    . '<a class="btn btn-default" href="' . $_settings['current_URL_path'] . '/register">Go back to events listing</a>';
                    return;
                }
                
                $ef->printRegisteredEvents($eventInfo);
                
                if (count($ef->checkIfRegistered($_GET['EventID'], $userID)) != 0) {
                    echo '<p>You are already registered for this event.</p>' . PHP_EOL
                    . '<a class="btn btn-default" href="' . $_settings['current_URL_path'] . '/register">Go back to events listing</a>';
                }elseif ($ef->isEventFull($eventInfo[0])) {
                    echo '<p>The seats for this event are full. Try signing up for another event.</p>' . PHP_EOL  .' <br />' . PHP_EOL
                    . '<a class="btn btn-default" href="' . $_settings['current_URL_path'] . '/register">Go back to events listing</a>';
                }
                elseif ($ef->isBeforeDeadline($eventInfo[0]->Date, 1)) {
                    if ($ef->isBeforeDeadline($eventInfo[0]->Date, 10)) {
                            echo '<form id="EventRegistrationComments" action="confirm-register?EventID=' . $_GET['EventID'] . '" method="post">'; ?>
                            <p><strong>Pre-Event Comments</strong></p>
                            <p>Please answer the following questions. This information will be sent to the event presenter who will be able to make 
                            the event more relevant to your interests and goals. You may type up to 1000 characters.</p>
                            <ol style="list-style-type: upper-alpha">
                              <li>Tell us what you expect to learn or experience at this event.</li>
                              <li>How do you expect these learning goals to assist in your academic success?</li>
                            </ol>
                            
                            <textarea class="form-control" rows="5" cols="80" maxlength="1000" name="Comments" placeholder="Enter your comments or questions here."></textarea>
                            <br /><br />
                            <input type="hidden" name="Redirect" value="redirectRegister" />
                            <input name="EventID" type="hidden" value="<?= $_GET['EventID'] ?>" />
                            <input class="btn btn-success" name="SubmitComments" type="submit" value="Register for Event"/>&nbsp;&nbsp;&nbsp;
                            <input class="btn btn-danger" type="button" value="Cancel Registration" onclick="window.location.href='<?=$_settings['current_URL_path'] . '/register';?>'"/>
                        <?php
                        }
                        else {
                            echo '<form id="EventRegistration" action="confirm-register?EventID=' . $_GET['EventID'] . '" method="post">'; ?>
                            <p>Confirm that you wish to register for the above event.</p>
                            <input name="Redirect" type="hidden" value="redirectRegister" />
                            <input name="EventID" type="hidden" value="<?= $_GET['EventID'] ?>" />
                            <input class="btn btn-success" name="Submit" type="submit" value="Register for Event"/>&nbsp;&nbsp;&nbsp;
                            <input class="btn btn-danger" type="button" value="Cancel Registration" onclick="window.location.href='<?=$_settings['current_URL_path'] . '/register';?>'"/>
                        <?php
                        }

                    echo '</form>';
                }
                else {
                    echo '<a class="btn btn-default" href="' . $_settings['current_URL_path'] . '/register">Go back to events listing</a>'; 
                }
            }
            else {
                if (!isset($userID)) {
                    $ef->printAlertBox();
                }
                echo '<a class="btn btn-default" href="' . $_settings['current_URL_path'] . '/register">Go back to events listing</a>';
            } ?>
            
            </div><!--.body-content-->
        </div><!--.column-1-->
    </div><!--.clearfix-->
</div><!--.wrapper-->
<script>
    $(document).ready(function(){
        $("#EventRegistrationComments").submit(function(event) {
            // textarea is empty or contains only white-space
            if ($.trim($("textarea[name=Comments]").val()) == '') {
                newAlert('danger', '<strong>REQUIRED FIELD:</strong> Please type your comments/questions question in the box provided. ');
                event.preventDefault();
            }
        });
    });
</script>