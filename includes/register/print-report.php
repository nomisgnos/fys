<?php

if (!isset($userID)) {
    $userID = "";
}
if (!isset($accessID)) {
    $accessID = "";
}

$userInfo = $obj->getUserInfo($accessID);
?>
<!DOCTYPE html>

<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="HandheldFriendly" content="true">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>FYS Cocurricular Event Registration Report</title>
  <style>
      @media print {    
        .no-print, .no-print *
        {
            display: none !important;
        }
      }
  </style>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100%7COpen+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,600,800&amp;subset=latin,latin" media="all"/>
</head>

<body style='font-family: "Open Sans","HelveticaNeue","Helvetica","Arial",sans-serif; width: 100%;'>

    <h2>Penn State Harrisburg -- FYS Cocurricular Event Registration Report</h2>

    <p>User Details:</p>

    <ul style="list-style-type: none">
        <?php if (isset($userInfo)) {
            echo '<li>' . $accessID . ' -- ' . $userInfo->LName . ', '. $userInfo->FName . '</li>';
            echo '<li>Email: ' . $userInfo->Email . "</li>";
        }
        else {
            echo '<li>' . $accessID .  '</li>';
            echo '<li>Email: No email on file</li>';
        }
        ?>
    </ul>

    <p>Events Registered For:</p>

    <?php $ef->printRegisteredEvents($ef->getRegisteredInfo($userID)); ?>
    <script>window.print();</script>

</body>

</html>

<?php exit; ?>
