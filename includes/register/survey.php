                <ol style="padding-bottom:7.5px;" class="breadcrumb">
                  <li class="breadcrumb-item"><a style="text-decoration: none;" href="<?= $_settings['current_URL_path'];?>/register">FYS</a></li>
                  <li class="breadcrumb-item active">Event Surveys</li>
                </ol><!--.breadcrumbs"-->
                <span class="clearfloat"></span>
                <?php 
                    if (isset($_POST['Submitted'])) {
                        echo '<h2>Survey responses submitted successfully!</h2>';
                    } else {
                        echo '<h2>Student Evaluation of Events</h2>';
                    }
                ?>
            </div> <!-- #content_header -->
            
            <div class="body-content" id="body-content-padding">
                <?php
                //survey initial page
                //if ($userIDisset && !isset($_POST['Submitted']) && !isset($_GET['RegistrationID'])) { 
                if (isset($userID) && !isset($_GET['RegistrationID'])) { ?>
                    <p>Events for which your attendance has been confirmed are listed below. Please allow 1 business day after the event for your attendance to be verified and the database to be updated.</p>
                    <p>To complete a survey, click on the <strong><em>Take this survey</em></strong> link below an event. Each survey will take approximately 5 minutes.</p>
                    <em style="color: rgba(255,0,0,.75); font-size: .95rem">If no events appear below then there are no surveys available for completion at this time; press <strong>Return to My Events</strong> to return to the My Registered Events page.</em>
                    <?php $ef->printRegisteredEvents($ef->getAvailableSurveys($userID), false, true);

                    echo '<a class="btn btn-default" href="' . $_settings['current_URL_path'] . '/register/drop">Return to My Events</a>';
                } 
                //elseif (isset($_GET['RegistrationID']) && !isset($_POST['Submitted'])) { //survey form page
                elseif (isset($userID) && isset($_GET['RegistrationID'])) {
                    $regID = (int) htmlspecialchars($_GET['RegistrationID'], ENT_QUOTES); //check sanitation here
                    //$eventExists = $ef->getEventInfo(ff); //check if event exists

                    // if the event is a number and not text, and exists
                    if (isset($regID)) {
                        if (!$ef->checkSurvey($regID)) {
                            $regEntry = $ef->matchRegisteredEvent($regID);
                            $ef->printRegisteredEvents($ef->getEventInfo($regEntry[0]->EventID)); ?>
                        
                            <p>Please provide the presenter with your opinion about the event. Your input is used to assess and improve the First-Year Seminar program. You may type up to 1000 characters.</p>
                            <form id="SurveyResults" action="" method="post">
                                <input type="hidden" name="RegistrationID" value="<?=$regID;?>" />
                                <input type="hidden" name="Redirect" value="redirectSurvey" />
                                <ol style="list-style-type: upper-alpha">
                                    <li>Did the event meet your learning goals? Explain.</li>
                                    <li>What ideas can you use in the next week(s)?</li>
                                </ol>
                                <textarea form="SurveyResults" name="SurveyParagraph" maxlength="1000" rows="4" cols="50"></textarea>
                                <p>Please rate this session according to its value to you.</p>
                                <div id="requiredText" class="redtext bold hidden">REQUIRED FIELD: Please rate this session.</div>
                                <ul style="list-style:none;">
                                    <li><input type="radio" name="SurveyRating" value="5" /> 5 - Extremely</li> 
                                    <li><input type="radio" name="SurveyRating" value="4" /> 4 - Very</li>
                                    <li><input type="radio" name="SurveyRating" value="3" /> 3 - Moderately</li>
                                    <li><input type="radio" name="SurveyRating" value="2" /> 2 - Somewhat</li>
                                    <li><input type="radio" name="SurveyRating" value="1" /> 1 - Not at all </li>
                                </ul>
                                <input type="submit" name="Submit" value="Submit Survey">&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="button" value="Back to Available Surveys" onclick="window.location.href='<?=$_settings['current_URL_path'] . '/register/survey';?>'" />
                            </form>
                        <?php 
                        } else { ?>
                            <p class="redtext bold">No survey available for the specified event. Please choose another event.</p>
                            <hr />
                            <p>Events for which your attendance has been confirmed are listed below. Please allow 1 business day after the event for your attendance to be verified and the database to be updated.</p>
                            <p>To complete a survey, click on the <strong><em>Take this survey</em></strong> link below an event. Each survey will take approximately 5 minutes.</p>
                            <em style="color: rgba(255,0,0,.75); font-size: .95rem">If no events appear below then there are no surveys available for completion at this time; press <strong>Return to My Events</strong> to return to the My Registered Events page.</em>
                            
                            <?php $ef->printRegisteredEvents($ef->getAvailableSurveys($userID), false, true);
                            echo '<a class="btn btn-default" href="' . $_settings['current_URL_path'] . '/register/drop" >Return to My Events</a>';
                        }
                    } else {
                        echo '<p></p>';
                        //redirect goes here for non-integer info being passed to EventID query
                    }
                }
                else {
                    $ef->printAlertBox();
                }?>
                    
            </div><!--.body-content-->
        </div><!--.column-1-->
    </div><!--.clearfix-->
</div><!--.wrapper-->
<script>
    $(document).ready(function(){
        $("#SurveyResults").submit(function(event) {
            if ($("input[name='SurveyRating']:checked").length == 0) {
                $('#requiredText').show();
                alert('REQUIRED FIELD: Please provide rating for this session.');
                event.preventDefault();
            }
        });
    });
</script>