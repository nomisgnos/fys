                <ol style="padding-bottom:7.5px;" class="breadcrumb">
                  <li class="breadcrumb-item"><a style="text-decoration: none;" href="<?= $_settings['current_URL_path'];?>/register">FYS</a></li>
                  <li class="breadcrumb-item active">Event Surveys</li>
                </ol><!--.breadcrumbs"-->
                <span class="clearfloat"></span>
            </div> <!-- #content_header -->

            <div class="body-content" id="body-content-padding">

                <h2>Student Evaluation of Events</h2>

                <?php
                if (isset($_GET['results'])) {
                    if ($_GET['results'] == 'success') {
                        echo '<p>Your survey feedback has been successfully processed. Thank you.</p>';
                    }
                    else {
                        echo '<p>There was a problem processing your survey feedback. Please try again.</p>';
                    }
                }
                // No else block is needed. If a user managed to manually come to this page without
                // result being passed in the URL, all we need is a link for them to return to surveys
                
                echo '<a class="btn btn-default" href="' . $_settings['current_URL_path'] . '/register/survey">Return to Event Surveys</a>';

                ?>

            </div><!--.body-content-->
        </div><!--.column-1-->
    </div><!--.clearfix-->
</div><!--.wrapper-->               

