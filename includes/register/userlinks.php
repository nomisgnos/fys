<div class="wrapper">
    <div class="clearfix">
        <div class="column-1">
            <div id="content-header">
            <?php if(isset($accessID)) : ?>
                
                <div style="padding:7.5px;" id="userlinks" class="no-print floatright">
                    <span>Logged in as &nbsp;<strong><?php echo $accessID; ?></strong></span>
                    <ul>
                        
                        <li><a href="<?= $_settings['current_URL_path'];?>/register/drop"><div class="fa fa-calendar"></div> Registered Events</a></li>
                        <?php 
                            // admin allowed
                            $allowed = array(1,2,3,4);
                            if ($obj->match_in_arrays($obj->getRoleID($userID), $allowed)) { ?>
                                <li><a href="<?= $_settings['current_URL_path'];?>/admin"><div class="fa fa-dashboard"></div> Admin View</a></li>
                        <?php } ?>
                        <li><a href="https://webaccess.psu.edu/cgi-bin/logout"><div class="glyphicon glyphicon-log-out"></div> Logout</a></li>
                    </ul>
                </div><!--.userlinks-->
                
            <?php endif; ?>