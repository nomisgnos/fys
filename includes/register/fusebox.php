<?php

    require_once('includes/event.php');
    $ef = new EventMethods($obj, $_settings, $userID);

    // check for redirects
    if (isset($_POST) && isset($_POST['Redirect'])) {
        $reflectedMethod = new ReflectionMethod('EventMethods', $_POST['Redirect']);
        $reflectedMethod->invoke($ef, $_POST);
    }

    if (checkCircuit($circuit, 'print-report') == false) {
        echo $buffer;
    }

    require_once('userlinks.php');

    switch ($circuit) {
        case '':
            include 'register.php';
            break;
        case checkCircuit($circuit, 'print-report'):
            include 'print-report.php';
            break;
        case checkCircuit($circuit, 'confirm-drop'):
            include 'confirm-drop.php';
            break;
        case checkCircuit($circuit, 'confirm-register'):
            include 'confirm-register.php';
            break;
        case checkCircuit($circuit, 'confirm-survey'):
            include 'confirm-survey.php';
            break;          
        case checkCircuit($circuit, 'drop'):
            include 'drop.php';
            break;
        case checkCircuit($circuit, 'faq'):
            include 'faq.php';
            break;          
        case checkCircuit($circuit, 'survey'):
            include 'survey.php';
            break;
        case checkCircuit($circuit, 'test'):
            include 'test.php';
            break;
        default:
            include 'register.php';
            break;
    }
?>