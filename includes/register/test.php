                <div class="breadcrumbs">
                    <a href="<?= $_settings['current_URL_path'];?>/register">FYS</a> > Drop Registered Event(s)
                </div>
                <span class="clearfloat"></span>
                <h2>My Registered Events:</h2>
            </div><!--content-header-->
            
            <div class="body-content" id="body-content-padding">
                    <p>You are registered for the following events. If you are no longer able to attend an event, you may drop it here.</p>
                    <p>**Note that you cannot drop your registration for an event that has already occurred or will occur within one business day of today's date.**</p>
                    <p>Select the event(s) you wish to drop then click "Drop Events."</p>
                        <form id="test" action="confirm-survey.php" method="post">
                            <div id="requiredText" style="color: red; font-weight: bold; display: none;">REQUIRED FIELD: Please enter your comments/questions in the box below.</div>
                        <?php 
                            $ef->printRegisteredEvents($ef->getRegisteredInfo($userID), true); // print events user is registered for
                        ?>
                            <input type="submit" name="Submit" value="Drop Events" />
                             
                        </form>
                        
                        <div class="no-print">
                            </br>
                            <a href="<?=$_settings['current_URL_path']; ?>/register/survey" style="color:red">[ Complete Event Surveys ]</a></br></br>
                            <a href="#" onclick="window.print();return false;" style="color:red">[ Print a Registration Report ]</a></br></br>
                            <a href="<?=$_settings['current_URL_path']; ?>/register" style="color:red;">[ Return to Registration ]</a>
                        </div>
            </div><!--.body-content-->
        </div><!--.column-1-->
    </div><!--.clearfix-->
</div><!--.wrapper-->
<script>
    $(document).ready(function() {
        alert($("input[name='SelectedEvent[]']").length);
        $("#test").submit(function(event) {
            alert("..");
            event.preventDefault();
        });
    });
</script>