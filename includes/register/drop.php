                <ol style="padding-bottom:7.5px;" class="breadcrumb">
                  <li class="breadcrumb-item"><a style="text-decoration: none;" href="<?= $_settings['current_URL_path'];?>/register">FYS</a></li>
                <?php 
                    $crumb = isset($_POST['Submit']) ? 'Drop Events' : 'Registered Events';
                ?>
                  <li class="breadcrumb-item active"><?=$crumb;?></li>
                </ol>
                
                <span class="clearfloat"></span>
                
                <?php 
                    if (isset($_POST['Submit'])) { 
                        echo '<h2>Drop Events</h2>';
                    } 
                    else {
                        echo '<h2>Registered Events</h2>';
                    }
                ?>

            </div><!--content-header-->
<div id="alert-area"></div>
            <div class="body-content" id="body-content-padding">
                <?php 
                if (isset($userID)) {
                    if (isset($_POST['SelectedEvent']) && isset($_POST['Submit'])) {
                        echo '<p class="redtext bold">You have not dropped any event(s) yet. Click "Confirm Drop" below to confirm your selection and drop the event(s).</p>';
                        $eventIDs = [];

                        foreach($_POST['SelectedEvent'] as $item) {
                            array_push($eventIDs, (int) $item);
                        }
                        
                        $safeIDs = implode(',', array_map("intval", $eventIDs)); 
                        $ef->printRegisteredEvents($ef->getEventInfo($safeIDs)); 
                        ?>

                        <p>If you drop an event by mistake you may simply re-register for it.</p>
                        <p><strong>Note:</strong> Event registrations close 1 business day before their scheduled date or when capacity runs out. Please be sure your selections are correct. In some cases you may not be able to re-register for an event you have dropped erroneously.</p>
                        <form id="ConfirmDrop" action="<?= $_settings['current_URL_path'];?>/register/confirm-drop" method="post">
                            <?php
                                    echo '<input type="hidden" name="DropIDs" value="'. $safeIDs .'" >';
                            ?>
                            <input name="Redirect" type="hidden" value="redirectDrop" />
                            <input class="btn btn-danger" type="submit" name="Submit" value="Confirm Drop">&nbsp;&nbsp;&nbsp;
                            <input class="btn btn-success" type="button" value="Cancel Drop" onclick="window.location.href='<?=$_settings['current_URL_path'] . '/register/drop';?>'"/>
                        </form>
                    <?php
                    }
                    else { ?>
                        <p>You are registered for the following events. If you are no longer able to attend an event, you may drop it here.</p>
                        <p>**Note that you cannot drop your registration for an event that has already occurred or will occur within one business day of today's date.**</p>
                        <p class="dropText">Select the event(s) you wish to drop then click "Drop Events."</p>

                        <form id="DropRegisteredEvent" action="" method="post">
                            <?php 
                            if (isset($_POST['Submit'])) {
                                echo '<div id="errorText">Please select events to drop.</div>';
                            }
                            
                            echo '<div id="requiredText" class="redtext bold hidden">REQUIRED FIELD: Please select at least one event to drop.</div>';
                            $ef->printRegisteredEvents($ef->getRegisteredInfo($userID), true); ?>

                            <div class="no-print">
                                <input class="btn btn-danger" type="submit" name="Submit" id="Submit" value="Drop Events">
                            </div>
                        </form>
                        
                        <div class="no-print">
                            </br>
                            <a class="btn btn-primary" href="<?=$_settings['current_URL_path']; ?>/register/survey" class="redtext">Complete Event Surveys</a></br></br>
                            <a class="btn btn-primary" href="<?=$_settings['current_URL_path']; ?>/register/print-report" class="redtext">Print a Registration Report</a></br></br>
                            <a class="btn btn-default" href="<?=$_settings['current_URL_path']; ?>/register" class="redtext">Return to Events Listing</a>
                        </div>
                <?php
                    }
                }
                else {
                    $ef->printAlertBox();
                } ?>
            </div><!--.body-content-->
        </div><!--.column-1-->
    </div><!--.clearfix-->
</div><!--.wrapper-->
<script>
    $(document).ready(function() {
        if ($("input[name='SelectedEvent[]']").length == 0) {
                $('#Submit').hide();
        }
        else {
            $("#DropRegisteredEvent").submit(function(event) {
                if ($("input[name='SelectedEvent[]']").length > 0 && $("input[name='SelectedEvent[]']:checked").length == 0) {
                    newAlert('danger', '<strong>REQUIRED FIELD:</strong> Please select at least one event to drop.');
                    event.preventDefault();
                }
            });
        }
    });
</script>