                <ol style="padding-bottom:7.5px;" class="breadcrumb">
                  <li class="breadcrumb-item"><a style="text-decoration: none;" href="<?= $_settings['current_URL_path'];?>/register">FYS</a></li>
                  <li class="breadcrumb-item active">Drop Results</li>
                </ol><!--.breadcrumbs"-->
                <span class="clearfloat"></span>
                <h2>Drop Results</h2>
            </div> <!-- #content_header -->
                <div class="body-content" id="body-content-padding">
                    <?php
                    if ($_GET['results']) {
                        switch ($_GET['results']) {
                            case 'failure':
                                echo '<p>The drop request was rejected by the database. Please try again.';
                                break;
                            case 'invalid':
                                echo '<p>The drop request contained invalid events. Please try again.</p>';
                                break;
                            case 'success':
                                if ($_GET['EventIDs']) {
                                    echo '<p>You have been successfully removed from the following event(s).</p>';
                                    $ef->printRegisteredEvents($ef->getEventInfo($_GET['EventIDs']));   
                                }
                                else {
                                    echo '<p>You have been successfully removed from the event(s).</p>';
                                }
                                break;
                            default:
                                echo '<p>Unable to drop class(es) due to unknown error. Please try again.</p>';
                                break;
                        }
                    } 
                    elseif (!isset($userID)) {
                        $ef->printAlertBox();
                    } 
                    else {
                        echo '<p>No events were selected to be dropped!</p>';
                    }
                    echo '<p><a class="btn btn-danger" href="' . $_settings['current_URL_path'] . '/register/drop" >Drop More Events</a></p>';
                    echo '<p><a class="btn btn-default" href="' . $_settings['current_URL_path'] . '/register" >Return to Events Listing</a></p>';
                    ?>
                </div><!--.body-content-->
        </div><!--.column-1-->
    </div><!--.clearfix-->
</div><!--.wrapper-->