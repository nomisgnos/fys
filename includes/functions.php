<?php
//require_once('includes/libraries/core.php');
//require_once('includes/settings.php');
///////////////////////////////Table Queries///////////////////////////
//$obj = new Core($_settings); 
/*
**GetTable
*================================================
*Pass: Cocurricular Event Type
*Return: Select * ... Where Type = $type 
*/
function psu_GetTable($type){
    
    $i=0;$arr=null;
    $arr[$i]['parameter']=':Type';
    $arr[$i]['value']=$type;
    $arr[$i]['data_type']= PDO::PARAM_STR;
    
    $query = 'SELECT * 
            FROM FYS_Cocurricular_Events 
            WHERE Type = :Type';
    $getData = $obj->executeSQL($query, $arr);
    return $getData;
}

/*
**GetUID
*================================================
*Pass: Remote User
*Return: Select UserID ... Where AccessID = $remote_user
*/
function psu_GetUID($remote_user){ //Do Not know where $remote_user is coming from, WebAccess?
    
    $i=0;$arr=null;
    $arr[$i]['parameter']=':remote_user';
    $arr[$i]['value']=$remote_user;
    $arr[$i]['data_type']=PDO::PARAM_INT;
    
    $query = 'SELECT UserID
            FROM FYS_User_Table
            WHERE AccessID = :remote_user';
    $getData = $obj->executeSQL($query, $arr);
    return $getData;
}

/*
**GetUserRole
*================================================
*Pass: Remote User
*Return: Select UserRole ... Where AccessID = $remote_user
*/
function psu_GetUserRole($remote_user){ //Do Not know where $remote_user is coming from, WebAccess?
    
    $i=0;$arr=null;
    $arr[$i]['parameter']=':remote_user';
    $arr[$i]['value']=$remote_user;
    $arr[$i]['data_type']=PDO::PARAM_INT;
    
    $query = 'SELECT UserRole
            FROM FYS_User_Table 
            WHERE AccessID = :remote_user';
    $getData = $obj->executeSQL($query, $arr);
    return $getData;
}

/*
**GetRegisteredEvents
*================================================
*Pass: Remote User
*Return: Select RegistrationID ... Where UserID = psu_GetUID($remote_user_)
*/
function psu_GetRegisteredEvents($remote_user){//Do Not know where $remote_user is coming from, WebAccess?
    
    $i=0;$arr=null;
    $arr[$i]['parameter']=':remote_user';
    $arr[$i]['value']=$remote_user;
    $arr[$i]['data_type']=PDO::PARAM_INT;
    
    $query = 'SELECT RegistrationID
            FROM FYS_Registration_Table 
            WHERE UserID = '.psu_GetUID($remote_user);
    $getData = $obj->executeSQL($query, $arr);
    return $getData;
}
?>
