<?php
/**
 *Methods for FYS Export
 *
 *Contains all methods used by Import tool to execute DB queries
 *
 */
 
/**
 *Methods for FYS Export
 *
 *Contains all methods used by Import tool to execute DB queries
 *
 *@package FYS
 */
Class ExportMethods {
    
    /**
     *@var object $core Core class object
     */
    private $core;
    
    /**
     *__construct Constructor for ExportMethods
     *
     *@param AdminMethods $adm Class object of AdminMethods
     *@return void
     */
    public function __construct($core) {
        $this->core = $core;
    }
    
    /**
     *getSurveyResponses Fetches all survey responses and informatino related to event and user
     *
     *@return object[] Array of DB objects
     */
    public function getSurveyResponses() {
        $query = "SELECT reg.EventID, reg.UserID, reg.PreEventComments, reg.IsAttended, reg.SurveyRating, reg.SurveyParagraph, crs.Name, crs.Number, crs.Section, usr.FName, usr.LName, usr.AccessID, evnt.Name AS EventName, evnt.EventID, evnt.Date AS EventDate
                    FROM FYS_Registration_Table as reg, FYS_User_Table as usr, FYS_Course_Table as crs, FYS_Cocurricular_Events as evnt, FYS_User_Courses_XREF as usrcrs
                    WHERE reg.UserID = usr.UserID 
                        AND usrcrs.UserID = usr.UserID
                        AND usrcrs.CourseID = crs.CourseID
                        AND evnt.EventID = reg.EventID
                    ORDER BY EventName, LName, FName";
        
        $results = $this->core->executeSQL($query);
        return $results;
    }
    
    /**
     *getAdminCC Concatnates admin emails in a CC format string
     *
     *@return string Admin emails separated by semicolon
     */
    public function getAdminCC() {
        $query = "SELECT REPLACE(group_concat(email, ';'),',','') as cc   
                    FROM FYS_User_Table as usr, FYS_User_Roles_XREF as uref
                    WHERE User_Role_ID = 1 AND usr.UserID = uref.UserID";
        $results = $this->core->executeSQL($query);
        
        return ($results[0]->cc);
    }
    
    /**
     *getSemesterReport Fetches DB info about semester reports
     *
     *@return object[] DB 
     */
    public function getSemesterReport() {
        $query = 'SELECT
                    event.Name as EvName,
                    event.Date,
                    event.EventID,
                    event.EndTime,
                    event.StartTime,
                    event.CoPres1,
                    event.CoPres2,
                    reg.UserID,
                    reg.SurveyRating,
                    reg.SurveyParagraph,
                    reg.PreEventComments,
                    reg.IsAttended,
                    event.Presenter
                FROM
                    FYS_Registration_Table reg
                    INNER JOIN FYS_Cocurricular_Events event
                        ON reg.EventID = event.EventID
                    INNER JOIN FYS_User_Table usr
                        ON (usr.UserID = reg.UserID)
                ORDER BY EventID';
                
        $results = $this->core->executeSQL($query);
        return $results;
    }
    
    /**
     *sendSemesterReport Sends semester report to presenters
     *
     *@param object $info      DB object with report information
     *@param string $tableData Table data to print out in email
     *@param string $send      Flag to indicate send report is confirmed
     *@return void
     */
    public function sendSemesterReport($info, $tableData, $send) {
        $date = date('m-d-Y');
        $message = "<h3>End of Semester Report :" . $info->EvName . " (" . $date . ")</h3>
                    <strong>Time:</strong> " . $info->StartTime . " - " . $info->EndTime . "<br />
                    <strong>Presenter:</strong> " . $info->Presenter;
        
        $message .= "<table>
                    <tr>
                        <td>Present</td>
                        <td>Pre-Event Comments</td>
                        <td>Rating</td>
                        <td>Post Event Comments</td>
                    </tr>" . $tableData .
                "</table>";

        if(!empty($send)) {
            $details = $info->EvName . " (" . $date . ") [#" . $info->EventID . "]";
            $subject = "FYS End of Semester Report - " . $details;
            $email = $info->Presenter . "@psu.edu";
            
            echo $info->Presenter . " - " . $details . "<br>";
            
            $headers = 'FROM: psh_fys_no_reply@psu.edu' . "\r\n" .
                       'Reply-To: psh_fys_no_reply@psu.edu' . "\r\n" .
                       'X-Mailer: PHP/' . phpversion() . "\r\n" .
                       'MIME-Version: 1.0' . "\r\n" . 
                       'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
                       
            mail($email, $subject, $message, $headers);
        } else {
            echo $message;
        }
    }
    
    public function separateResultsByID($data) {
        $currentID = "";
        $collection = [];
        $arr = [];
        
        foreach($data as $row) {
            if($row->EventID != $currentID) {
                if($currentID != "") {
                    array_push($collection, $arr);
                }
                
                $currentID = $row->EventID;
                $arr = [];
            }
            
            array_push($arr, $row);
        }
        
        return $collection;
    }
}
?>