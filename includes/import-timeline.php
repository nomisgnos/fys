<?php
/**
 *Methods for FYS Import Timeline tool
 *
 *Contains all methods used by Import Timeline tool to execute DB queries
 *
 */
 
/**
 *Methods for FYS Import Timeline tool
 *
 *Contains all methods used by Import Timeline tool to execute DB queries
 *
 *@package FYS
 */
Class ImportTimeline {
    
    /**
     *__construct Constructor for ImportTimeline
     *
     *@param AdminMethods $adm Class object of AdminMethods
     *@return void
     */
    public function __construct($adm) {
        $this->adm = $adm;
    }
    
    public function getUniqueImportDates() {
        $query = "SELECT DISTINCT importedOn FROM FYS_User_Table WHERE importedOn != '' ORDER BY importedOn DESC";
        $response = $this->adm->core->executeSQL($query);
        
        return $response;
    }
    
    public function getAllData() {
        $query = "SELECT usr.UserID, usr.FName, usr.LName, usr.AccessID, usr.Email, usr.importedOn, rref.User_Role_ID, crs.CourseID, crs.ScheduleNumber FROM FYS_User_Table as usr INNER JOIN FYS_User_Roles_XREF as rref USING(UserID) LEFT JOIN FYS_User_Courses_XREF as cref USING(UserID) LEFT JOIN FYS_Course_Table as crs ON InstructorID=AccessID OR cref.CourseID = crs.CourseID WHERE usr.importedOn != '' AND User_Role_ID IN (2,5)";
        $response = $this->adm->core->executeSQL($query);
        
        return $response;
    }
}
?>