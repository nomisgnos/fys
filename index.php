<?php
    require_once("includes/core.php");
    require_once("includes/settings.php");
    
    function checkCircuit($circuit, $redirect) {
        return (strpos(strtolower($circuit), $redirect) !== false);
    }

    $obj = new Core($_settings);
    if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost') {
        $accessID = 'sys106';
    } else {
        $accessID = $obj->getAccessID();
    }
    
    $userID = $obj->getUserID($accessID);
    $roleID = $obj->getRoleID($userID);
    
    $head_title = '';
    $bread = '';
    $circuit = (!empty($_REQUEST['circuit'])) ? $_REQUEST['circuit'] : '';
    
    if (checkCircuit($circuit, 'print-report')) {
        include 'includes/register/fusebox.php';
    }

    switch ($circuit) {
        case '':
        case checkCircuit( $circuit, 'register' ):
            $head_title .= 'FYS Event Registration';
            break;
        case checkCircuit( $circuit, 'admin' ):
            $head_title .= 'FYS Administration';
            break;
        default:
            $head_title .= '404 - Page not found';
            break;
    }

    ob_start();
    require_once("includes/templates/header.php");
    $buffer = ob_get_contents();
    ob_clean();
    $buffer = str_replace("%TITLE%", $head_title, $buffer);
    
    switch ($circuit) {
        case '':
            if(isset($roleID) && $obj->canUserAccess(array(1,2,3,4), $roleID)) {
                $head_title = 'FYS Administration';
                include 'includes/admin/fusebox.php';
            } else {
                include 'includes/register/fusebox.php';
            }
            break;
        case checkCircuit($circuit, 'register'): //register is our index
            include 'includes/register/fusebox.php';
            break;
        case checkCircuit($circuit, 'admin'):
            include 'includes/admin/fusebox.php';
            break;
        default:
            echo $buffer;
            include 'includes/templates/404.php';
            break;
    }

    require_once("includes/templates/footer.php");
    ob_end_flush();
?> 