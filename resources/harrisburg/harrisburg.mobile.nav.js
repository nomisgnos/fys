// toggle the primary navigation visibility when using the "Menu" button on mobile
  $( document ).ready(function() {
	 // grab the "title" of the informaion menu; Assuming it is "Information for:"
  var infoHeader = $('.header-information-nav h4.nav-label').text();
  // get the navigation items from the information nav
  var infoMenu = $('.header-information-nav ul').clone();
  // wrap the header in a list item
  var newListItem = '<li class="info-control"><a href="#">'+ infoHeader +'</a></li>';
  // grab the call to action menu, clone it, do some tweaks, and add the information menu list item to this newly cloned list.

  var menuClones = $('ul#cta-menu').clone().attr('id', 'mobile--information-menus').prepend(newListItem);
  // attach the mobile menus to the appropriate markup structure
  $('#mobile-menus').append(menuClones);
  // attach the sub-menu for the information menu "dropdown"
  $('#mobile--information-menus li.info-control').append(infoMenu);
  // alter the classes on those list items since we've added a top level item
  $('#mobile--information-menus li').removeClass('first');
  $('#mobile--information-menus li:first-child').addClass('first');
  // add click functionality to open the submenu
  $('#mobile--information-menus li.info-control > a').on('click', function(){
	$(this).parent('li.info-control').toggleClass('active');
	$('#mobile--information-menus #information-menu').toggle().toggleClass('information-menu--open');
	if ($('#header-menu .nav--primary-nav').attr('style')) {
	  $('#header-menu .nav--primary-nav').removeAttr('style');
	}
  });

  // PSUM-600: Add another copy of the CTA menu in the footer for mobile.
  $('ul#cta-menu').clone().attr('id', 'cta-menu-footer-mobile').appendTo('#footer');
  // PSUM-624 because of the positioning issue using absolute, the quickest solution is to
  // add another copy and toggle visibility via relative positioning.
  $('ul#cta-menu-footer-mobile').clone().attr('id', 'cta-menu-footer-main').insertAfter('.footer-campus-data .site-address');
  $('ul#cta-menu-footer-main').removeClass('clearfix');

  // PSUM-600: Add a mobile only "Top" button in the logo footer area.
  $('<a>')
	.attr({
	  href: '#skip-link',
	  id: 'sitewide-back-to-top'
	})
	.text('Top')
	.appendTo('div.footer-logo');

  // toggle the primary navigation visibility when using the "Menu" button on mobile
  $('.nav-toggle-menu').on('click', function(e) {
	e.preventDefault();
	$('#header-menu .nav--primary-nav').toggle();
	if ($('#mobile--information-menus #information-menu').hasClass('information-menu--open')) {
	  $('#mobile--information-menus #information-menu').toggle().toggleClass('information-menu--open');
	  $('#mobile--information-menus li.info-control').toggleClass('active');
	}
  });

  // toggle the primary navigation visibility when using the "Menu" button on mobile
  $('.nav-toggle-search').on('click', function(e) {
	e.preventDefault();
	$('#navbar form.search-form').toggle();
	if ($('#navbar form.search-form').css('display') != 'none') {
	  $('#navbar form.search-form input.form-text').focus();
	}
  });
  });