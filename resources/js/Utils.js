function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}

function redirectToLastPage(waitTime) {
    setTimeout('location.replace(document.referrer);', waitTime);
}

function redirectTo(site, waitTime) {
    setTimeout('location.replace("' + site + '");', waitTime);
}

function newAlert (type, message) {
  $("#alert-area").append($("<div class='alert center-block alert-" + type + " fade in'> " +  message + " <button type='button' class='close' data-dismiss='alert'>  &times;</button></div>"));

  $(".alert").delay(2000).fadeOut("slow", function () { $(this).remove(); });
}

function ajaxEventInfoModal() {
    $(document).on("click", ".info-modal", function(e) {
        $(this).tooltip('hide');
        var displayTitle = $(this).html();
        var description = $(this).attr('data-original-title');
        var presenter = $(this).attr('data-presenter');
        var msg = description + '<br><br><strong>Presenter: </strong>' + presenter;
        
        var box = bootbox.alert({
            title: displayTitle,
            message: msg,
            backdrop: true,
            buttons: {
                ok: {
                    label: 'Close',
                    className: 'btn-danger',
                    callback: function(){}
                }
            },
        });

        // Center the modal
        box.css({
          'top': '50%',
          'margin-top': function () {
            return -(box.height() / 2);
          }
        });
    });
}